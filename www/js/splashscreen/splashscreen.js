var splashscreen = function() {
    var progresHandler = {

        proValObj   : null,
        SQLLObj     : null,
        ip          : null,
        gloabalToken: null,
        initialize: function() {
            document.addEventListener('deviceready', function () {
                progresHandler.proValObj = progressValue();
                progresHandler.ip = new ip();
                progresHandler.ip = progresHandler.ip.remoteIpAddress();
                progresHandler.SQLLObj = new SQLiteLogin();
                progresHandler.checkTokenAvailable();
            }, false);
        },

        checkTokenAvailable: function() {
            var tokenInformations = progresHandler.SQLLObj.pullToken();
            tokenInformations.done(function(informations) {
                var accessToken = '';
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    accessToken = information.token;
                }
                if (accessToken.trim() === '') {
                  console.log('there is no accessToken');
                    $("progress").attr("value", 100);
                    $('.container').empty();
                    $('.container').load('./views/login.html');
                    return;
                }
                progresHandler.gloabalToken = accessToken;

                var values = {
                    "accessToken": accessToken
                };
                $.ajax({
                    url: progresHandler.ip+'?r=check-token/check',
                    method: "POST",
                    dataType: 'json',
                    data: values,

                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR.status + '\n' + jqXHR.responseText);
                    },

                    success: function(data) {
                        // console.log(data);
                        if (data.success === true) {
                            $("progress").attr("value", 100);
                            //  redirecting user to dashbord-layout.html
                            window.location.href = "./views/reach.html";
                            // progresHandler.todayPlanning(accessToken);
                            var setFlag = new forgroundEventHandler();
                            setFlag.appStartFlagCheck();
                            document.addEventListener("resume", setFlag.appStartFlagCheck, false);
                        } else {
                            $("progress").attr("value", 100);
                            $('.container').empty();
                            $('.container').load('././views/login.html');
                        }

                    }
                }).fail(function(error) {
                    console.error(error);
                    navigator.notification.alert(
                        'Unable to Connect with the Server.' +
                        ' Check out internet connection and try again',
                        progresHandler.checkTokenAvailable,
                        'Connection error',
                        'Try again'
                    );
                    $("progress").attr("value", 0);
                });

                console.log(accessToken);
            }).fail(function(error) {
                console.error(error);
            });
        },
        todayPlanning: function() {
            alert(progresHandler.gloabalToken);
            var values = {
                    "accessToken": progresHandler.gloabalToken
                };
                $.ajax({
                    url: progresHandler.ip+'?r=current-date-planning/today-planning',
                    method: "POST",
                    dataType: 'json',
                    data: values,

                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR.status + '\n' + jqXHR.responseText);
                    },

                    success: function(data) {
                        console.log(data);
                    }
                }).fail(function(error) {
                    console.error(error);
                    navigator.notification.alert(
                        'Unable to Connect with the Server.' +
                        ' Check out internet connection and try again',
                        progresHandler.todayPlanning,
                        'Connection error',
                        'Try again'
                    );
                    $("progress").attr("value", 0);
                });
        }

    }

    return progresHandler.initialize();
}
