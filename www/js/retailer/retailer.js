var retailer = {
  SQLLObj: null,
  ip     : null,
  ctr    : null,
  locSet : null,
  cam    : null,

  initialize: function() {
    screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
    retailer.ip      = new ip();
    retailer.ip      = retailer.ip.remoteIpAddress();
    retailer.ctr     = new controller();
    retailer.SQLLObj = new SQLiteLogin();
    retailer.locSet  = new locationSet();
    retailer.cam     = new camera();

    $('#google-map').hide();
    retailer.bindEvents();
  },

  bindEvents: function() {
    $(document).on('click', '#camera', function(e) {
      e.preventDefault();
      retailer.cam.openDefaultCamera();
    });
    $(document).on('click','#gallary', function(e) {
      e.preventDefault();
      retailer.cam.openFilePicker();
    });
    $(document).on('click', '#btnRetInfoInsert', retailer.pushRetToRemote);
    $(document).on('click', '#mapBtn', retailer.loadMpa);
    $(document).on('click', '#gpsBtn', retailer.setLocation);
  },
  setLocation: function() {
    retailer.locSet.setPosition();
  },
  //  Displaying google map in screen
  loadMpa: function() {
    // alert('hi anil');
    $('#google-map').show();
    // $('#google-map').removeClass('noDisplay');
    //   $('#google-map').addClass('google-map');
  },
  addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },

  pushRetToRemote: function(e) {
    e.preventDefault();

    //  geting data from page
    var retName         = $('#retName').val();
    var retOwner        = $('#retOwner').val();
    var retPostalCode = $('#postalCode').val();
    var retLat             = $('#latOfLoc').attr('lat');
    var retLng            = $('#lngOfLoc').attr('lng');
    var retCountry      = $('#retCountry').val();
    var retCity            = $('#retCity').val();
    var retDistrict       = $('#retDistrict').val();
    var retProvince     = $('#retProvince').val();
    var retPhone         = $('#retPhone').val();
    var retMobile        = $('#retMobile').val();
    var retEmail          = $('#retEmail').val();
    var retFacebook    = $('#retFacebook').val();
    var retViber           = $('#retViber').val();

    //  checking for input field is empty or not
    if (retName.trim() === '') {
      navigator.notification.alert(
                    'please enter business name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (retOwner.trim() === '') {
      navigator.notification.alert(
                    'please enter business owner name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if(retLat.trim() === '' || retLng.trim() === '') {
      navigator.notification.alert(
                    'please select retailer location.',
                    null,
                    'Error',
                    'OK'
                );
                return;
    }
    if (retPostalCode.trim() === '') {
      navigator.notification.alert(
                    'please enter postal code.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (retCountry.trim() === '') {
      navigator.notification.alert(
                    'please enter country name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (retCity.trim() === '') {
      navigator.notification.alert(
                    'please enter city name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (retDistrict.trim() === '') {
      navigator.notification.alert(
                    'please enter district name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (retMobile.trim() === '') {
      navigator.notification.alert(
                    'please enter mobile number.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    // checking Email field is empty or not
    if(!(retEmail.trim() === '')) {
      var checkEmail = retailer.isEmail();
        if(checkEmail === 0) {
            navigator.notification.alert(
                            'please enter valid email.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
    }
    retailer.addLoading();
    $('.loadingText').text('sending data...');

    var tokenInformations = retailer.SQLLObj.pullToken();
    tokenInformations.done(function(informations) {
      var accessToken = '';
      for (var idx in informations) {
          var information = informations[idx];
          //alert(information.name);
          accessToken = information.token;
      }
      if (accessToken.trim() === '') {
        console.log('there is no accessToken');
          window.location.href = "../index.html";
          return;
      }
      var imageData          = $('#photo').attr('src');
      var options            = new FileUploadOptions();
      options.fileKey        = "file";
      options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
      options.mimeType       = "image/jpeg";
      console.log(options.fileName);
      // Creating new object to stor field data
      var params           = new Object();
      params.accessToken   = accessToken;
      params.retName       = retName;
      params.retOwner      = retOwner;
      params.retPostalCode = retPostalCode;
      params.retLat        = retLat;
      params.retLng        = retLng;
      params.retCountry    = retCountry;
      params.retCity       = retCity;
      params.retDistrict   = retDistrict;
      params.retProvince   = retProvince;
      params.retPhone      = retPhone;
      params.retMobile     = retMobile;
      params.retEmail      = retEmail;
      params.retFacebook   = retFacebook;
      params.retViber      = retViber;
      // stroing params object to options
      options.params         = params;
      options.chunkedMode    = false;

      var ft = new FileTransfer();
      ft.upload(imageData, retailer.ip+'?r=customer-information-pusher/retailer', function(result){
        console.log('data is: ',result);
        retailer.removeLoading();
        var response = $.parseJSON(result.response);
        if(response.status === 1) {
          navigator.notification.alert(
                    response.msgBody,
                    null,
                    response.msgTitle,
                    'OK'
                );
                window.location.href = './retailerInsertForm.html';
        }
        if(response.status === 0) {
          navigator.notification.alert(
                    response.msgBody,
                    null,
                    response.msgTitle,
                    'OK'
                );
        }
        if(response.status === 2) {
          navigator.notification.alert(
                    response.msgBody,
                    null,
                    response.msgTitle,
                    'OK'
                );
                window.location.href = "../index.html";
        }
      }, function(error){
        console.error(error);
        distributor.removeLoading();
        navigator.notification.alert(
          'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
          null,
          'ERROR',
          'OK'
        );
      }, options);

    }).fail(function(error) {
        console.error(error);
        retailer.removeLoading();
        navigator.notification.alert(
          'Error occured',
          null,
          'ERROR',
          'OK'
        );
    });
  },

  isEmail: function() {
    var email = $('#retEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  }


}
retailer.initialize();
