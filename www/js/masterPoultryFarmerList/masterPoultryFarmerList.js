var masterPoultryFarmerList = {
    SQLLObj  : null,
    ip       : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    compData : null,
    distCount: null,
    retCount : null,
    compCount: null,
    cam      : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        masterPoultryFarmerList.ip      = new ip();
        masterPoultryFarmerList.ip      = masterPoultryFarmerList.ip.remoteIpAddress();
        masterPoultryFarmerList.ctr     = new controller();
        masterPoultryFarmerList.SQLLObj = new SQLiteLogin();
        masterPoultryFarmerList.locSet  = new locationSet();
        masterPoultryFarmerList.addLoading();
        masterPoultryFarmerList.loadDist();

        masterPoultryFarmerList.distCount = 0;
        masterPoultryFarmerList.retCount  = 0;
        masterPoultryFarmerList.compCount = 0;
        //  Creating object of camera class
        masterPoultryFarmerList.cam       = new camera();
        masterPoultryFarmerList.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#camera', function(e) {
            e.preventDefault();
            masterPoultryFarmerList.cam.openDefaultCamera();
        });
        $(document).on('click','#gallary', function(e) {
            e.preventDefault();
            masterPoultryFarmerList.cam.openFilePicker();
        });
        $(document).on('click', '#mapBtn', masterPoultryFarmerList.loadMpa);
        $(document).on('click', '#gpsBtn', masterPoultryFarmerList.setLocation);
        $(document).on('click', '.dist-add', masterPoultryFarmerList.addDist);
        $(document).on('click', '.dist-minus', masterPoultryFarmerList.remove);
        $(document).on('click', '.ret-add', masterPoultryFarmerList.addRet);
        $(document).on('click', '.ret-minus', masterPoultryFarmerList.remove);
        $(document).on('click', '.feed-add' , masterPoultryFarmerList.addFeedCompany);
        $(document).on('click', '.feed-minus', masterPoultryFarmerList.remove);
        $(document).on('click', '.send-Master-Poultry', masterPoultryFarmerList.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('input', '#feedUse', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('compId',$(this).attr('comp_id'));
             }
           });
        });
    },
    setLocation: function() {
        masterPoultryFarmerList.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = masterPoultryFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterPoultryFarmerList.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterPoultryFarmerList.distData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  masterPoultryFarmerList.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = masterPoultryFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterPoultryFarmerList.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterPoultryFarmerList.retData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#retList').append(templete);
                  masterPoultryFarmerList.loadFeedComp();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all feed company data from server
    loadFeedComp: function() {
      var tokenInformations = masterPoultryFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterPoultryFarmerList.ip + '?r=order-handler/comp-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterPoultryFarmerList.compData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option comp_id="'+this.id+'" value="'+this.compName+'">';
                  });
                  // templete += '</ul>';
                  $('#compList').append(templete);
                  masterPoultryFarmerList.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  masterPoultryFarmerList.loadFeedComp,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+masterPoultryFarmerList.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+masterPoultryFarmerList.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+masterPoultryFarmerList.distCount;
        var templeteOption = '';
                  
        $.each(masterPoultryFarmerList.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        masterPoultryFarmerList.distCount = masterPoultryFarmerList.distCount + 1;
    },
    remove: function() {
        $(this).parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+masterPoultryFarmerList.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+masterPoultryFarmerList.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+masterPoultryFarmerList.retCount;
        var templeteOption = '';
                  
        $.each(masterPoultryFarmerList.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        masterPoultryFarmerList.retCount = masterPoultryFarmerList.retCount + 1;
    },
    addFeedCompany: function() {
        var templete = '';
        templete += '<div class="feedData row">'+
                        '<div class="col-xs-9" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="feedUse" compId="" list="compList'+masterPoultryFarmerList.compCount+'" placeholder="feed use(company)">'+
                            '<datalist id="compList'+masterPoultryFarmerList.compCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 feed-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus feed-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:5px;"></button>'+
                    '</div>';
        $('.feedList').append(templete);
        var target = '#compList'+masterPoultryFarmerList.compCount;
        var templeteOption = '';
                  
        $.each(masterPoultryFarmerList.compData, function() {
            templeteOption += '<option comp_id="'+this.id+'" value="'+this.compName+'">';
        });
        $(target).append(templeteOption);
        masterPoultryFarmerList.compCount = masterPoultryFarmerList.compCount + 1;
    },
    isEmail: function() {
    var email = $('#farEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#farName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farbird').val().trim() === '') {
            navigator.notification.alert(
                            'farmer bird number area can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if(!($('input[type="radio"][name="farBirdType"]:checked').val())) {
            navigator.notification.alert(
                            'select bird type.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var compStatus = 0;
        $.each($('.feedData'), function() {
            if(($(this).find('#feedUse').val().trim() ==='')) {
                $(this).find('#feedUse').attr('compId','');
            } else {
                if($(this).find('#feedUse').attr('compId').trim() === '') {
                    compStatus = 1;
                }
            }
        });
        if(compStatus === 1) {
            navigator.notification.alert(
                            'feed(company) information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        //  Checking email address field is empty or not
        if(!($('#farEmail').val().trim() === '')) {
            var checkEmail = masterPoultryFarmerList.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
                return;
            }
        }
        masterPoultryFarmerList.addLoading();
        $('.loadingText').text('Sending......');
        masterPoultryFarmerList.sendRemote();
    },
    sendRemote: function() {
        var poultryFarInfo = [];
        poultryFarDetail = {
            name      : $('#farName').val(),
            number    : $('#farNumber').val(),
            lat       : $('#latOfLoc').attr('lat'),
            lng       : $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address   : $('#farAddress').val(),
            bird      : $('#farbird').val(),
            birdType  : $('input[type="radio"][name="farBirdType"]:checked').val(),
            doctor    : $('#farDoctor').val(),
            email     : $('#farEmail').val(),
            viber     : $('#farViber').val(),
            facebook  : $('#farFacebook').val()
        }
        poultryFarInfo.push(poultryFarDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        poultryFarInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        poultryFarInfo.push(retList);
        var feedList = [];
        $.each($('.feedData'), function() {
            if(!($(this).find('#feedUse').attr('compId').trim() === '')) {
                var data = {
                feed: $(this).find('#feedUse').attr('compId')
                }
                feedList.push(data);
            }
        });
        poultryFarInfo.push(feedList);
        console.log(poultryFarInfo);
        // sending data to server using ajax
        var tokenInformations = masterPoultryFarmerList.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('#photo').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1);
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to stor field data
            var params             = new Object();
            params.accessToken     = accessToken;
            params.poultryFarInfo  = poultryFarInfo;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;

            var ft = new FileTransfer();
            ft.upload(imageData, masterPoultryFarmerList.ip + '?r=master-poultry-farmer/handler', function(result){
                console.log('data is: ',result);
                masterPoultryFarmerList.removeLoading();
                var response = $.parseJSON(result.response);
                if(response.status === 1) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                        window.location.href = './masterPoultryFarmerList.html';
                }
                if(response.status === 0) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                }
                if(response.status === 2) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                        window.location.href = "../index.html";
                }
            }, function(error){
                console.error(error);
                masterPoultryFarmerList.removeLoading();
                navigator.notification.alert(
                    'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                    null,
                    'ERROR',
                    'OK'
                    );
             }, options);
        }).fail(function(error) {
            console.error(error);
            masterPoultryFarmerList.removeLoading();
            navigator.notification.alert(
                                    'Error occured',
                                    null,
                                    'ERROR',
                                    'OK'
                                );
        });
    }
}

masterPoultryFarmerList.initialize();