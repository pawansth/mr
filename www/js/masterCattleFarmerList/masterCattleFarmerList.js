var masterCattleFarmerList = {
    SQLLObj  : null,
    ip       : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    compData : null,
    distCount: null,
    retCount : null,
    compCount: null,
    cam      : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        masterCattleFarmerList.ip      = new ip();
        masterCattleFarmerList.ip      = masterCattleFarmerList.ip.remoteIpAddress();
        masterCattleFarmerList.ctr     = new controller();
        masterCattleFarmerList.SQLLObj = new SQLiteLogin();
        masterCattleFarmerList.locSet  = new locationSet();
        masterCattleFarmerList.addLoading();
        masterCattleFarmerList.loadDist();

        masterCattleFarmerList.distCount = 0;
        masterCattleFarmerList.retCount  = 0;
        masterCattleFarmerList.compCount = 0;
        masterCattleFarmerList.cam       = new camera();
        masterCattleFarmerList.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#camera', function(e) {
            e.preventDefault();
            masterCattleFarmerList.cam.openDefaultCamera();
        });
        $(document).on('click','#gallary', function(e) {
            e.preventDefault();
            masterCattleFarmerList.cam.openFilePicker();
        });
        $(document).on('click', '#mapBtn', masterCattleFarmerList.loadMpa);
        $(document).on('click', '#gpsBtn', masterCattleFarmerList.setLocation);
        $(document).on('click', '.dist-add', masterCattleFarmerList.addDist);
        $(document).on('click', '.dist-minus', masterCattleFarmerList.remove);
        $(document).on('click', '.ret-add', masterCattleFarmerList.addRet);
        $(document).on('click', '.ret-minus', masterCattleFarmerList.remove);
        $(document).on('click', '.feed-add' , masterCattleFarmerList.addFeedCompany);
        $(document).on('click', '.feed-minus', masterCattleFarmerList.remove);
        $(document).on('click', '.send-Master-Cattle', masterCattleFarmerList.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('input', '#feedUse', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('compId',$(this).attr('comp_id'));
             }
           });
        });
    },
    setLocation: function() {
        masterCattleFarmerList.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="file:///android_asset/www/img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = masterCattleFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterCattleFarmerList.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterCattleFarmerList.distData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  masterCattleFarmerList.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = masterCattleFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterCattleFarmerList.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterCattleFarmerList.retData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#retList').append(templete);
                  masterCattleFarmerList.loadFeedComp();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all feed company data from server
    loadFeedComp: function() {
      var tokenInformations = masterCattleFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterCattleFarmerList.ip + '?r=order-handler/comp-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterCattleFarmerList.compData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option comp_id="'+this.id+'" value="'+this.compName+'">';
                  });
                  // templete += '</ul>';
                  $('#compList').append(templete);
                  masterCattleFarmerList.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+masterCattleFarmerList.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+masterCattleFarmerList.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+masterCattleFarmerList.distCount;
        var templeteOption = '';
                  
        $.each(masterCattleFarmerList.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        masterCattleFarmerList.distCount = masterCattleFarmerList.distCount + 1;
    },
    remove: function() {
        $(this).parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+masterCattleFarmerList.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+masterCattleFarmerList.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+masterCattleFarmerList.retCount;
        var templeteOption = '';
                  
        $.each(masterCattleFarmerList.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        masterCattleFarmerList.retCount = masterCattleFarmerList.retCount + 1;
    },
    addFeedCompany: function() {
        var templete = '';
        templete += '<div class="feedData row">'+
                        '<div class="col-xs-9" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="feedUse" compId="" list="compList'+masterCattleFarmerList.compCount+'" placeholder="feed use(company)">'+
                            '<datalist id="compList'+masterCattleFarmerList.compCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 feed-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus feed-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:5px;"></button>'+
                    '</div>';
        $('.feedList').append(templete);
        var target = '#compList'+masterCattleFarmerList.compCount;
        var templeteOption = '';
                  
        $.each(masterCattleFarmerList.compData, function() {
            templeteOption += '<option comp_id="'+this.id+'" value="'+this.compName+'">';
        });
        $(target).append(templeteOption);
        masterCattleFarmerList.compCount = masterCattleFarmerList.compCount + 1;
    },
    isEmail: function() {
    var email = $('#farEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#farName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farCattle').val().trim() === '') {
            navigator.notification.alert(
                            'farmer cattle number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var compStatus = 0;
        $.each($('.feedData'), function() {
            if(($(this).find('#feedUse').val().trim() ==='')) {
                $(this).find('#feedUse').attr('compId','');
            } else {
                if($(this).find('#feedUse').attr('compId').trim() === '') {
                    compStatus = 1;
                }
            }
        });
        if(compStatus === 1) {
            navigator.notification.alert(
                            'feed(company) information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if(!($('#farEmail').val().trim() === '')) {
            var checkEmail = masterCattleFarmerList.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
            return;
            }
        }
        masterCattleFarmerList.addLoading();
        $('.loadingText').text('Sending......');
        masterCattleFarmerList.sendRemote();
    },
    sendRemote: function() {
        var cattleFarInfo = [];
        cattleFarDetail = {
            name      : $('#farName').val(),
            number    : $('#farNumber').val(),
            lat       : $('#latOfLoc').attr('lat'),
            lng       : $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address   : $('#farAddress').val(),
            cattle    : $('#farCattle').val(),
            doctor    : $('#farDoctor').val(),
            email     : $('#farEmail').val(),
            viber     : $('#farViber').val(),
            facebook  : $('#farFacebook').val()
        }
        cattleFarInfo.push(cattleFarDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        cattleFarInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        cattleFarInfo.push(retList);
        var feedList = [];
        $.each($('.feedData'), function() {
            if(!($(this).find('#feedUse').attr('compId').trim() === '')) {
                var data = {
                feed: $(this).find('#feedUse').attr('compId')
                }
                feedList.push(data);
            }
        });
        cattleFarInfo.push(feedList);
        console.log(cattleFarInfo);
        // sending data to server using ajax
        var tokenInformations = masterCattleFarmerList.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            console.log(accessToken);

            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('#photo').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1);
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to stor field data
            var params             = new Object();
            params.accessToken     = accessToken;
            params.cattleFarInfo   = cattleFarInfo;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;

            var ft = new FileTransfer();
            ft.upload(imageData, masterCattleFarmerList.ip + '?r=master-cattle-farmer-list/handler', function(result){
                console.log('data is: ',result);
                masterCattleFarmerList.removeLoading();
                var response = $.parseJSON(result.response);
                if(response.status === 1) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                window.location.href = './masterCattleFarmerList.html';
                }
                if(response.status === 0) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                }
                if(response.status === 2) {
                    navigator.notification.alert(
                        response.msgBody,
                        null,
                        response.msgTitle,
                        'OK'
                    );
                    window.location.href = "../index.html";
                }
            }, function(error){
                console.error(error);
                masterCattleFarmerList.removeLoading();
                navigator.notification.alert(
                        'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                        null,
                        'ERROR',
                        'OK'
                    );
            }, options);
        }).fail(function(error) {
            console.error(error);
            masterCattleFarmerList.removeLoading();
            navigator.notification.alert(
                                    'Error occured',
                                    null,
                                    'ERROR',
                                    'OK'
                                );
        });
    }
}

masterCattleFarmerList.initialize();