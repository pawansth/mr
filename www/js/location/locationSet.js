var locationSet = function() {
    var setLocation = {
        ip: null,
        initialize: function() {
            setLocation.ip = new ip();
            setLocation.ip = setLocation.ip.remoteIpAddress();
        },
        setPosition: function() {
            //checking if GPS is enabled or not
            cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                console.log("Location is " + (enabled ? "enabled" : "disabled"));
                if (enabled) {
                    console.log('GPS is ON');
                    SpinnerDialog.show("Featching Location", "Feaching your current location....", true);
                    //As GPS is enabled featching user current location
                    navigator.geolocation.getCurrentPosition(setLocation.onSuccess, setLocation.onError, {
                        maximumAge: 60000,
                        timeout: 15000,
                        enableHighAccuracy: true
                    });
                } else {
                    console.log('GPS is OFF');
                    setLocation.dilogForGPS();
                    // cordova.plugins.diagnostic.switchToLocationSettings();
                }
            }, function(error) {
                navigator.notification.alert(
                            'Faild to featch location. Please try again',
                            setLocation.setPosition,
                            'warning',
                            'Try Again'
                        );
            });
        },
        onSuccess: function(position) {
            // console.log('inside onStartSuccess function');

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;
            $.ajax({
                url: 'https://maps.googleapis.com/maps/api/geocode/json?address='+latitude+','+longitude+'&',
                method: "POST",
                dataType: 'json',
                // data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    console.log(data.results[0].address_components);
                    var postal_code = '';
                      var num = ['0','1','2','3','4','5','6','7','8','9'];
                      var i = 0;
                      // console.log(results[0]);
                      $.each(data.results[0].address_components,function() {
                      var comValue = 0;
                      var str = data.results[0].address_components[i].long_name;
                      for (var j = 0; j < str.length; j++) {
                        var c = str.charAt(j);
                        if(jQuery.inArray( c, num) === -1) {
                        comValue = 1;
                        }
                      }
                      if(comValue === 0) {
                        postal_code = str;
                      }
                      i = i + 1;
                    });
                    $('#latOfLoc').attr('lat',latitude);
                    $('#latOfLoc').text('Latitude: '+latitude);
                    $('#lngOfLoc').attr('lng',longitude);
                    $('#lngOfLoc').text('Longitude: '+longitude);
                    $('#postalCode').val(postal_code);
                    SpinnerDialog.hide();
                }
        }).fail(function(error) {
            console.error(error);
            SpinnerDialog.hide();
            navigator.notification.alert(
                ' Failed to load postal code.',
                null,
                'Error',
                'OK'
            );
        });
            // navigator.notification.alert(
            //                 'latitude: '+latitude+' \n longitude: '+longitude,
            //                 null,
            //                 'warning',
            //                 'OK'
            //             );

            

        },

        onError: function(error) {
            console.error(error);
            SpinnerDialog.hide();
            navigator.notification.alert(
                            'Faild to featch location. Please try again',
                            null,
                            'warning',
                            'OK'
                        );
        },
        dilogForGPS: function() {
            //As GPS is not enabled poped up a dilog that say user to enable GPS
            //  and lead them to GPS setting location
            console.log('inside dilogForGps method');
            cordova.dialogGPS("Your GPS is Disabled, this app needs to be enable to works.", //message
                "Use GPS, with wifi or 3G.", //description
                function(buttonIndex) { //callback
                    switch (buttonIndex) {
                        case 0:
                            break; //cancel
                        case 1:
                            break; //neutro option
                        case 2:
                            break; //user go to configuration
                    }
                },
                "Please Turn on GPS", //title
                ["Cancel", "Go"]); //buttons
        }
    }
    setLocation.initialize();
    return setLocation;
}