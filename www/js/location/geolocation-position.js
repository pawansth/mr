var geolocationPosition = function() {

    var geoPosition = {
        self             : null,
        position         : null,
        bgLocationTracker: null,
        SSDHObj          : null,
        SLBFFG           : null,
        ip               : null,
        SQLROObj         : null,
        initialize: function() {
            self          = this;
            geoPosition.ip      = new ip();
            geoPosition.ip      = geoPosition.ip.remoteIpAddress();

            // creating SQLiteLocBtnFlagForGeoloc object
            geoPosition.SLBF = new SQLiteLocBtnFlag();

            //creating SQliteBridge object
            geoPosition.SSDHObj  = new SQLiteStartDateHandler();
            geoPosition.SQLROObj = new SQLiteReachOrigin();
            // console.log(this.SSDHObj);

            //creating object of bgGeolocation class
            geoPosition.bgLocationTracker = new bgGeolocation();
            console.log(geoPosition.bgLocationTracker); //printing bgLoationTracker in console log

        },

        getStartPosition: function() {
            //checking if GPS is enabled or not
            cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                console.log("Location is " + (enabled ? "enabled" : "disabled"));
                if (enabled) {
                    console.log('GPS is ON');
                    SpinnerDialog.show("Featching Location", "Feaching your current location....", true);
                    //As GPS is enabled featching user current location
                    navigator.geolocation.getCurrentPosition(geoPosition.onStartSuccess, geoPosition.onStartError, {
                        maximumAge: 60000,
                        timeout: 15000,
                        enableHighAccuracy: true
                    });
                } else {
                    console.log('GPS is OFF');
                    geoPosition.dilogForGPS();
                    // cordova.plugins.diagnostic.switchToLocationSettings();
                }
            }, function(error) {
                console.error("The following error occurred: " + error);
            });
        },
        onStartSuccess: function(position) {
            // console.log('inside onStartSuccess function');

            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;

            //  creating object of SQLiteLogin class
            var SQLLObj = new SQLiteLogin();

            //  pulling accessToken value from local database
            var tokenInformations = SQLLObj.pullToken();
            //  Feaching tooken value from tokenInformations variable that store promise value
            tokenInformations.done(function(informations) {
                var accessToken = '';
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    accessToken = information.token;
                }
                //  if accessToken is empty the redirect user to login page
                if (accessToken.trim() === '') {
                    window.location.href = "../index.html";
                    return;
                }

                //  List that store latitude and longitude values to send in ajax call
                var values = {
                    accessToken: accessToken,
                    latitude: latitude,
                    longitude: longitude
                };
                var startTime = geoPosition.gateTime(); //get local time of start button click
                var result = geoPosition.SSDHObj.pushStartDate(startTime);
                result.done(function() {
                    $.ajax({
                        url: geoPosition.ip+'?r=location/start-location',
                        method: "POST",
                        dataType: 'json',
                        data: values,
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                            geoPosition.SSDHObj.deleteStartDate();
                            SpinnerDialog.hide();
                            navigator.notification.alert(
                                'error occured, please try again',
                                false,
                                'ERROR',
                                'OK'
                            );
                        },

                        success: function(data) {
                            console.log(data);
                            if (data === 2) {
                              SpinnerDialog.hide();
                              window.location.href = "../index.html";
                              return;
                            }
                            SpinnerDialog.hide(); //hiding spinner dilog
                            $(document).find('#btn-stop').removeClass('active');
                            $(document).find('#btn-start').addClass('active');
                            $(document).find('#btn-start').text('Started Day');

                            //starting location track process in background
                            geoPosition.bgLocationTracker.startInBackground();
                            var result = geoPosition.SLBF.updateLocBtnFlag(1, 0);
                            result.done(function() {
                                var dbOrigin = [];
                                var origin = geoPosition.SQLROObj.getOrigin();
                                origin.done(function(informations) {
                                for (var idx in informations) {
                                    var information = informations[idx];
                                    //alert(information.name);
                                    var originLoc = {
                                        latitude : information.latitude,
                                        longitude: information.longitude
                                    };
                                    dbOrigin.push(originLoc);
                                }
                                if(dbOrigin.length < 1) {
                                    var originRecord = geoPosition.SQLROObj.setOrigin(latitude, longitude);
                                    originRecord.done(function() {
                                        console.log('successfully recorded user start origin location in local database');
                                    });
                                } else {
                                    var originReco = geoPosition.SQLROObj.updateOrigin(latitude, longitude);
                                    originReco.done(function() {
                                        console.log('successfully update user start origin location in local database');
                                    });
                                }
                                });
                                console.log('start log button flag successfully update');
                            }).fail(function(error) {
                                console.error(error);
                            });

                        }
                    });
                }).fail(function(error) {
                    console.warn(error);
                    console.log('unable to insert time in SQLite database');
                    SpinnerDialog.hide();
                    navigator.notification.alert(
                        'error occured, please try again',
                        false,
                        'ERROR',
                        'OK'
                    );
                });

            }).fail(function(error) {
                console.error(error);
            });

        },

        onStartError: function(error) {
            console.error(error);
            SpinnerDialog.hide();
        },

        //function that called while user touch stop button
        getStopPosition: function() {
            var informations = geoPosition.SSDHObj.pullStartDate();
            console.log(informations);
            informations.done(function(informations) {
                var recordedStartTime;
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    recordedStartTime = information.date_time;
                }
                var stopTime = geoPosition.gateTime(); //get local time of stop button click

                //subtract from stopTime to startTime to get his working hours
                var difference = stopTime - recordedStartTime;
                var workedHours = parseInt((difference) / (1000 * 60 * 60));
                if (workedHours >= 0) {
                    console.log('work hours finished');
                    geoPosition.getStopPositionAfter();
                } else {
                    console.log('you working hour is not finished');
                    navigator.notification.alert(
                            'You working hour is not finished.....',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
                }
            }).fail(function(error) {
                console.warn(error);
            });
        },

        //when working hours is finished this function is called
        getStopPositionAfter: function() {

            cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                console.log("Location is " + (enabled ? "enabled" : "disabled"));
                if (enabled) {
                    SpinnerDialog.show("Featching Location", "Feaching your current location....", true);
                    navigator.geolocation.getCurrentPosition(geoPosition.onStopSuccess, geoPosition.onStopError, {
                        maximumAge: 60000,
                        timeout: 15000,
                        enableHighAccuracy: true
                    });
                } else {
                    geoPosition.dilogForGPS();
                }
            }, function(error) {
                console.error("The following error occurred: " + error);
            });
        },

        //when mr stoped position is successfully captured onStopSuccess function is called
        onStopSuccess: function(position) {

            //  creating object of SQLiteLogin class
            var SQLLObj = new SQLiteLogin();
            console.log('position of stop', position);
            //  pulling accessToken value from local database
            var tokenInformations = SQLLObj.pullToken();
            //  Feaching tooken value from tokenInformations variable that store promise value
            tokenInformations.done(function(informations) {
                var accessToken = '';
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    accessToken = information.token;
                }
                //  if accessToken is empty the redirect user to login page
                if (accessToken.trim() === '') {
                    window.location.href = "../index.html";
                    return;
                }
                //  Origin data from database
                // reach.SQLROObj.setOrigin('27.694884','84.4241729');
                var dbOrigin = [];
                var origin = geoPosition.SQLROObj.getOrigin();
                origin.done(function(informations) {
                    for (var idx in informations) {
                        var information = informations[idx];
                        //alert(information.name);
                        var originLoc = {
                            latitude : information.latitude,
                            longitude: information.longitude
                        };
                        dbOrigin.push(originLoc);
                    }
                    var time = position.timestamp;
                    var destinyLoc = {
                        latitude : position.coords.latitude,
                        longitude: position.coords.longitude
                    };
                    // alert(new Date(time));
                    var values = {
                    accessToken: accessToken,
                    time       : time,
                    originLoc  : dbOrigin,
                    destinyLoc : destinyLoc
                    };
                    console.log('values: ',values);
                    $.ajax({
                        url: geoPosition.ip+'?r=location/stop-location',
                        method: "POST",
                        dataType: 'json',
                        data: values,

                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                            SpinnerDialog.hide();
                            navigator.notification.alert(
                                'error occured, please try again',
                                false,
                                'ERROR',
                                'OK'
                            );
                        },

                        success: function(data) {
                            // console.log(data);
                            if (data === 2) {
                                SpinnerDialog.hide();
                                window.location.href = "../index.html";
                                return;
                            }
                            if(data === 1) {
                                SpinnerDialog.hide();
                                $(document).find('#btn-start').removeClass('active');
                                $(document).find('#btn-stop').addClass('active');
                                $(document).find('#btn-start').text('Start Day');
                                geoPosition.SQLROObj.deleteOrigin();
                                geoPosition.SSDHObj.deleteStartDate();
                                var result = geoPosition.SLBF.updateLocBtnFlag(0, 1);
                                result.done(function() {
                                    console.log('stop button log is successfully recorded');
                                }).fail(function(error) {
                                    console.error(error);
                                });
                                //stoping background process of location tracking
                                geoPosition.bgLocationTracker.stopInBackground();
                            } else if(data === 0) {
                                navigator.notification.alert(
                                    'Operation faild. Please try again.',
                                    null,
                                    'Faild',
                                    'OK'
                                );
                            } 
                        }
                    }).fail(function(error) {
                        console.error(error);
                        navigator.notification.alert(
                            'There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                    });
                    }).fail(function(error) {
                    console.error(error);
                });
            }).fail(function(error) {
                console.error(error);
            });

        },

        //when mr stoped position is not captured onStopError function is called
        onStopError: function(error) {
            SpinnerDialog.hide();
            cordova.dialogGPS("Your GPS is Disabled, this app needs to be enable to works.", //message
                "Use GPS, with wifi or 3G.", //description
                function(buttonIndex) { //callback
                    switch (buttonIndex) {
                        case 0:
                            break; //cancel
                        case 1:
                            break; //neutro option
                        case 2:
                            break; //user go to configuration
                    }
                },
                "Please Turn on GPS", //title
                ["Cancel", "Later", "Go"]); //buttons
        },

        gateTime: function() {
            var now = new Date();
            var currentTime = now.getTime();
            return currentTime;
        },

        dilogForGPS: function() {
            //As GPS is not enabled poped up a dilog that say user to enable GPS
            //  and lead them to GPS setting location
            console.log('inside dilogForGps method');
            cordova.dialogGPS("Your GPS is Disabled, this app needs to be enable to works.", //message
                "Use GPS, with wifi or 3G.", //description
                function(buttonIndex) { //callback
                    switch (buttonIndex) {
                        case 0:
                            break; //cancel
                        case 1:
                            break; //neutro option
                        case 2:
                            break; //user go to configuration
                    }
                },
                "Please Turn on GPS", //title
                ["Cancel", "Go"]); //buttons
        }
        //        onSuccess: function(position) {
        //            alert('Latitude: '          + position.coords.latitude          + '\n' +
        //              'Longitude: '         + position.coords.longitude         + '\n' +
        //              'Altitude: '          + position.coords.altitude          + '\n' +
        //              'Accuracy: '          + position.coords.accuracy          + '\n' +
        //              'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
        //              'Heading: '           + position.coords.heading           + '\n' +
        //              'Speed: '             + position.coords.speed             + '\n' +
        //              'Timestamp: '         + position.timestamp                + '\n');
        //        },
        //        onError: function(error) {
        //            alert('code: '    + error.code    + '\n' +
        //              'message: ' + error.message + '\n');
        //        }
    }
    geoPosition.initialize();
    return geoPosition;
}
