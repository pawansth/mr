var SQLiteReachOrigin = function() {
    // getting sqlite connection
  var db = new SQLiteConnection();

  var service = {};
  service.setOrigin = function(latitude, longitude) {
      var deferred = $.Deferred();
      db.transaction(function(tx) {
        var query = 'INSERT INTO mr_origin (latitude, longitude) VALUES (?,?)';
          tx.executeSql(query, [latitude, longitude], function(tx, res) {
              deferred.resolve(res);
          }, function(e) {
              deferred.reject(e);
          });
      });
      return deferred.promise();
  },
  service.getOrigin = function() {
      var deferred = $.Deferred();
      db.transaction(function(tx) {
          tx.executeSql('SELECT * FROM mr_origin', [], function(tx, res) {
              var informations = [];
              console.log(res.rows.length);
              for (var i = 0; i < res.rows.length; i++) {
                  var information = {
                      latitude: res.rows.item(i).latitude,
                      longitude: res.rows.item(i).longitude
                  };
                  //alert('name'+res.rows.item(i).name);
                  informations.push(information);
              }
              deferred.resolve(informations);

          }, function(e) {
              deferred.reject(e);
          });
      });
      return deferred.promise();
  },
  service.updateOrigin = function(latitude, longitude) {
      var deferred = $.Deferred();
    // console.log(db);
    db.transaction(function(tx) {
      var query = 'UPDATE mr_origin SET latitude = ?, longitude = ? where id = ?';
        tx.executeSql(query, [latitude, longitude,1], function(tx, res) {
          console.log('updated sucessfully');
            deferred.resolve();
        }, function(e) {
            deferred.reject(e);
        });
    });
    return deferred.promise();
  },
  service.deleteOrigin = function() {
      var deferred = $.Deferred();
    // console.log(db);
    db.transaction(function(tx) {
      var query = 'DELETE FROM mr_origin';
        tx.executeSql(query, [], function(tx, res) {
          console.log('delete successfully sucessfully');
            deferred.resolve();
        }, function(e) {
            deferred.reject(e);
        });
    });
    return deferred.promise();
  }
  return service;
}