var SQLiteLogin = function() {
  // getting sqlite connection
  var db = new SQLiteConnection();

  var service = {};

  service.pushToken = function(accessToken) {
      var deferred  = $.Deferred();
      db.transaction(function(tx) {
        var query = 'INSERT INTO login_token (token) VALUES (?)';
          tx.executeSql(query, [accessToken], function(tx, res) {
              deferred.resolve(res);
          }, function(e) {
              deferred.reject(e);
          });
      });
      return deferred.promise();
  },

  service.pullToken = function() {
          //alert('inside pulldata function');
          var deferred = $.Deferred();
          db.transaction(function(tx) {
            var query = 'SELECT * FROM login_token';
              tx.executeSql(query, [], function(tx, res) {
                  var informations = [];
                  console.log('data length is '+res.rows.length);
                  for (var i = 0; i < res.rows.length; i++) {
                      var information = {
                          token: res.rows.item(i).token
                      };

                      informations.push(information);
                  }
                  deferred.resolve(informations);

              }, function(e) {
                  deferred.reject(e);
              });
          });
          return deferred.promise();
      }

  return service;

}
