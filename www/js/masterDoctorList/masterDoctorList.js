var masterDoctorList = {
    SQLLObj  : null,
    ip       : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    distCount: null,
    retCount : null,
    cam      : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        masterDoctorList.ip      = new ip();
        masterDoctorList.ip      = masterDoctorList.ip.remoteIpAddress();
        masterDoctorList.ctr     = new controller();
        masterDoctorList.SQLLObj = new SQLiteLogin();
        masterDoctorList.locSet  = new locationSet();

        masterDoctorList.addLoading();
        masterDoctorList.loadDist();

        masterDoctorList.distCount = 0;
        masterDoctorList.retCount  = 0;
        masterDoctorList.cam       = new camera();
        masterDoctorList.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#camera', function(e) {
            e.preventDefault();
            masterDoctorList.cam.openDefaultCamera();
        });
        $(document).on('click','#gallary', function(e) {
            e.preventDefault();
            masterDoctorList.cam.openFilePicker();
        });
        $(document).on('click', '#mapBtn', masterDoctorList.loadMpa);
        $(document).on('click', '#gpsBtn', masterDoctorList.setLocation);
        $(document).on('click', '.dist-add', masterDoctorList.addDist);
        $(document).on('click', '.dist-minus', masterDoctorList.remove);
        $(document).on('click', '.ret-add', masterDoctorList.addRet);
        $(document).on('click', '.ret-minus', masterDoctorList.remove);
        // $(document).on('click', '.prodFocus-add' , masterTechnicanList.addProdFocus);
        // $(document).on('click', '.prodFocus-minus', masterTechnicanList.remove);
        $(document).on('click', '.send-Master-Doctor', masterDoctorList.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
    },
    setLocation: function() {
        masterDoctorList.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = masterDoctorList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterDoctorList.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterDoctorList.distData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  masterDoctorList.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = masterDoctorList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterDoctorList.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterDoctorList.retData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#retList').append(templete);
                  masterDoctorList.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+masterDoctorList.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+masterDoctorList.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+masterDoctorList.distCount;
        var templeteOption = '';
                  
        $.each(masterDoctorList.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        masterDoctorList.distCount = masterDoctorList.distCount + 1;
    },
    remove: function() {
        $(this).parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+masterDoctorList.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+masterDoctorList.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+masterDoctorList.retCount;
        var templeteOption = '';
                  
        $.each(masterDoctorList.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        masterDoctorList.retCount = masterDoctorList.retCount + 1;
    },
    isEmail: function() {
    var email = $('#docEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#docName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#docNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#docAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#docWorkingClinic').val().trim() === '') {
            navigator.notification.alert(
                            'working clinic can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        // Checking email field is empty or not
        if(!($('#docEmail').val().trim() === '')) {
            var checkEmail = masterDoctorList.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
                        return;
            }
        }
        masterDoctorList.addLoading();
        $('.loadingText').text('Sending......');
        masterDoctorList.sendRemote();
    },
    sendRemote: function() {
        var docInfo = [];
        docDetail = {
            name: $('#docName').val(),
            number: $('#docNumber').val(),
            lat: $('#latOfLoc').attr('lat'),
            lng: $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address: $('#docAddress').val(),
            workingClinic: $('#docWorkingClinic').val(),
            email: $('#docEmail').val(),
            viber: $('#docViber').val(),
            facebook: $('#docFacebook').val()
        }
        docInfo.push(docDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        docInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        docInfo.push(retList);
        console.log(docInfo);
        // sending data to server using ajax
        var tokenInformations = masterDoctorList.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('#photo').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1);
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to stor field data
            var params             = new Object();
            params.accessToken     = accessToken;
            params.docInfo         = docInfo;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;

            var ft = new FileTransfer();
            ft.upload(imageData, masterDoctorList.ip + '?r=master-doctor-list/handler', function(result){
                console.log('data is: ',result);
                masterDoctorList.removeLoading();
                var response = $.parseJSON(result.response);
                if(response.status === 1) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                window.location.href = '../views/masterDoctorList.html';
                }
                if(response.status === 0) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                }
                if(response.status === 2) {
                    navigator.notification.alert(
                        response.msgBody,
                        null,
                        response.msgTitle,
                        'OK'
                    );
                    window.location.href = "../index.html";
                }
            }, function(error){
                console.error(error);
                masterDoctorList.removeLoading();
                navigator.notification.alert(
                        'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                        null,
                        'ERROR',
                        'OK'
                    );
            }, options);
        }).fail(function(error) {
            console.error(error);
            masterDoctorList.removeLoading();
            navigator.notification.alert(
                                    'Error occured',
                                    null,
                                    'ERROR',
                                    'OK'
                                );
        });
    }
}

masterDoctorList.initialize();