var distributorStock = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    prodData: null,

    initialize: function() {
        screen.orientation.unlock();
        // Lock orientation to portrait mode
        screen.orientation.lock('portrait');
        distributorStock.ip      = new ip();
        distributorStock.ip      = distributorStock.ip.remoteIpAddress();
        distributorStock.SQLLObj = new SQLiteLogin();
        distributorStock.ctr     = new controller();

        distributorStock.addLoading();
        distributorStock.loadDist();
        distributorStock.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#evaluationDate', distributorStock.showDatePicker);
        $(document).on('click', '.addProd', distributorStock.addProdTemplete);
        $(document).on('click', '.minusProd', distributorStock.removeProdTemplete);
        $(document).on('click', '.btn-dist-stock', distributorStock.chick);
        $(document).on('focusout', '#distNameSearch', function() {
            if($('#distNameSearch').val().trim() === '') {
                $('#distNameSearch').attr('distId', '');
            }
        });
        $(document).on('focusout', '#productName', function() {
            if($('#productName').val().trim() === '') {
                $('#productName').attr('prodId', '');
                $('#productName').attr('baseUnitId', '');
                $('#productName').attr('price', '');
            }
            distributorStock.calculateAmount($(this).parent().parent());
        });
        $(document).on('focusout', '#productUnit', function() {
            if($('#productUnit').val().trim() === '') {
                $('#productUnit').attr('prodUnitId', '');
            }
            distributorStock.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('focusout', '#productQty', function() {
            distributorStock.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#productName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var parent = $(this).parent().parent();
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodId',$(this).attr('prod_id'));
               $(ob).attr('baseUnitId',$(this).attr('base_unit_id'));
               $(ob).attr('price',$(this).attr('base_price'));
               distributorStock.addLoading();
               $('.loadingText').text('Loading units...');
               distributorStock.searchCrosProdUnit(parent);
             }
           });
        });
        $(document).on('input', '#productUnit', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodUnitId',$(this).attr('unit_id'));
               return;
             }
           });
        });
    },
    showDatePicker: function() {
      var options = {
          date: new Date(),
          minDate: Date.parse(new Date()),
          mode: 'date',
          windowTitle: 'evaluation Date'
        };

        datePicker.show(options, function(d){
          var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
          $('#evaluationDate').val(datestring);
          $('#evaluationDate').attr('disabled',true);
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loadDist: function() {
      var tokenInformations = distributorStock.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: distributorStock.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  distributorStock.loadProd();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  distributorStock.loadDist,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    loadProd: function() {
      var tokenInformations = distributorStock.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: distributorStock.ip + '?r=order-handler/prod-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  distributorStock.prodData = data;
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                  });
                  // templete += '</ul>';
                  $('#prodList').append(templete);
                  distributorStock.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  distributorStock.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    /* searching corresponding product unit */
    searchCrosProdUnit: function(currentObj) {
      crossProductId = currentObj.find('#productName').attr('prodId');
      //  Pulling access token form sqlite database
      var tokenInformations = distributorStock.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken,
              crossProductId: crossProductId
          };

          $.ajax({
              url: distributorStock.ip + '?r=order-handler/search-cross-unit',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  console.log(data);
                  
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }

                  var templete = '';
                  
                  $.each(data, function() {
                      templete += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                  });
                  console.log(currentObj.find('#unitList'));
                  currentObj.find('#unitList').append(templete);
                  distributorStock.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  distributorStock.searchCrosProdUnit,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addProdTemplete: function() {
        $(this).parent().parent().css('margin-bottom','5px');
        var templete = '';
        templete += '<div class="planProduct">'+
                        '<div class="">'+
                            '<input type="text" class="form-control" id="productName" prodId="" baseUnitId="" price="" list="prodList" placeholder="product">'+
                            '<datalist id="prodList"></datalist>'+
                        '</div>'+
                        '<div class="row add-top-margin">'+
                            '<div class="col-xs-4 nopadding-right">'+
                                '<input type="text" class="form-control" id="productUnit" prodUnitId="" list="unitList" placeholder="unit">'+
                                '<datalist id="unitList"></datalist>'+
                            '</div>'+
                            '<div class="col-xs-2 nopadding-left nopadding-right">'+
                                '<input type="number" class="form-control" id="productQty" placeholder="qty">'+
                            '</div>'+
                            '<div class="col-xs-3 nopadding-left">'+
                                '<input type="text" class="form-control" id="productAmount" placeholder="NPR">'+
                            '</div>'+
                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                            '<button type="button" class="btn btn-warning floating-minus minusProd glyphicon glyphicon-remove col-xs-2"></button>'+
                        '</div>'+
                    '</div>';
        $('.planProductList').append(templete);
    },
    removeProdTemplete: function() {
        $(this).parent().parent().remove();
    },
    calculateAmount: function(parentObj) {
        if(!(parentObj.find('#productName').val().trim() === '' || parentObj.find('#productName').attr('prodId').trim() === '')) {
            if(!(parentObj.find('#productUnit').val().trim() === '' || parentObj.find('#productUnit').attr('prodUnitId').trim() === '')) {
                if(!(parentObj.find('#productQty').val().trim() === '')) {
                    var tokenInformations = distributorStock.SQLLObj.pullToken();
                    tokenInformations.done(function(informations) {
                        var accessToken = '';
                        for (var idx in informations) {
                            var information = informations[idx];
                            //alert(information.name);
                            accessToken = information.token;
                        }
                        if (accessToken.trim() === '') {
                            console.log('there is no accessToken');
                            window.location.href = "../index.html";
                            return;
                        }

                        // console.log(accessToken);

                        var values = {
                            accessToken: accessToken,
                            productId  : parentObj.find('#productName').attr('prodId'),
                            uid        : parentObj.find('#productUnit').attr('prodUnitId'),
                            base_uid   : parentObj.find('#productName').attr('baseUnitId'),
                            qtyToChange: parentObj.find('#productQty').val()
                        };
                        distributorStock.addLoading();
                        $('.loadingText').text('calcuation amount');
                    $.ajax({
                        url: distributorStock.ip + '?r=major-dist-plan/unit-to-price',
                        method: "POST",
                        dataType: 'json',
                        data: values,

                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                            navigator.notification.alert(
                            'There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                        },

                        success: function(data) {
                            distributorStock.removeLoading();
                            if (data === 2) {
                                window.location.href = "../index.html";
                            }
                            parentObj.find('#productAmount').val(data);
                        }
                    }).fail(function(error) {
                        console.error(error);
                        distributorStock.removeLoading();
                        navigator.notification.alert(
                            ' There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                    });
                    }).fail(function(error) {
                        console.error(error);
                    });
                }
            }
        }
        return;
    },
    chick: function() {
        if($('#distNameSearch').val().trim() === '') {
            navigator.notification.alert(
                'Distributor name can not be empty',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        if($('#distNameSearch').attr('distId').trim() === '') {
            navigator.notification.alert(
                'Distributor name is not properly slected form list.'+
                ' Please select distributor name form list.',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        if($('#evaluationDate').val().trim() === '') {
            navigator.notification.alert(
                'Evaluation Date is empty.'+
                ' Please select Evaluation Date.',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        var count = 1;
        var eachStatus = false;
        $.each($('.planProduct'), function() {
            if($(this).find('#productName').val().trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productName').attr('prodId').trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' should be select form list of give product.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').val().trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').attr('prodUnitId').trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' should be select form list of given unit list.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if ($(this).find('#productBatch').val().trim() === '') {
                navigator.notification.alert(
                    'Product batch in row ' + count +
                    ' can not be empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productQty').val().trim() === '') {
                navigator.notification.alert(
                    'Product quantity in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            count = count + 1;
        });
        if(eachStatus === true) {
            eachStatus = false;
            return;
        }
        distributorStock.addLoading();
        $('.loadingText').text('Submiting data')
        distributorStock.submitStock();
    },
    submitStock: function() {
        var productValues  = [];
        var distId         = $('#distNameSearch').attr('distId');
        var evaluationDate = new Date($('#evaluationDate').val()).getTime() / 1000;
        var description    = $('#desc').val();

        var distributorInfo = {
            distId        : distId,
            evaluationDate: evaluationDate,
            description   : description
        };
        var productId    = '';
        var productQty   = '';
        var productUnit  = '';
        var productAmount = '';

        $.each($('.planProduct'), function() {
            productId     = $(this).find('#productName').attr('prodId');
            productQty    = $(this).find('#productQty').val();
            productUnit   = $(this).find('#productUnit').attr('prodUnitId');
            productAmount = $(this).find('#productAmount').val();
            productBatch  = $(this).find('#productBatch').val();
            var data = {
                productId    : productId,
                productQty   : productQty,
                productUnit  : productUnit,
                productBatch : productBatch,
                productAmount: productAmount
            };
            // pushing json formate data to array
            productValues.push(data);
        });
        console.log(productValues);
        // sending data to server using ajax
        var tokenInformations = distributorStock.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken    : accessToken,
                distributorInfo: distributorInfo,
                productValues  : productValues
            };

            $.ajax({
                url: distributorStock.ip + '?r=dist-stock/handler',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    distributorStock.removeLoading();
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    // console.log(data);
                    if (data === 1) {
                        distributorStock.removeLoading();
                        navigator.notification.alert(
                            'Successfull',
                            null,
                            'Success',
                            'OK'
                        );
                        window.location.href = "./distributorStock.html";
                    } else if (data === 0) {
                        distributorStock.removeLoading();
                        navigator.notification.alert(
                            'Operation faild',
                            null,
                            'Error',
                            'OK'
                        );
                    } else if (data === 2) {
                        window.location.href = "../index.html";
                    }

                }
            }).fail(function(error) {
                console.error(error);
                distributorStock.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });

        }).fail(function(error) {
            distributorStock.removeLoading();
            navigator.notification.alert(
                'Error occured.',
                null,
                'ERROR',
                'OK'
            );
            console.error(error);
        });
    }
}

distributorStock.initialize();