var Login = function() {
    var loginHandler = {
      SLObj: null,
      ip: null,
      bckBtn: 0,
      bckHou: null,
      bckMin: null,
      bckSec: null,
      initialize: function() {
        document.addEventListener('deviceready', function () {
            loginHandler.ip        = new ip();
            loginHandler.ip        = loginHandler.ip.remoteIpAddress();
            loginHandler.SLObj = new SQLiteLogin();
            document.addEventListener("backbutton", function (e) {
                e.preventDefault();
                if(loginHandler.bckBtn === 1) {
                    var date = new Date();
                    var hou = date.getHours();
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    if(hou === loginHandler.bckHou && min === loginHandler.bckMin && ((sec-loginHandler.bckSec)<2)) {
                        navigator.app.exitApp();
                    } else {
                        loginHandler.bckHou = 0;
                        loginHandler.bckMin = 0;
                        loginHandler.bckSec = 0;
                        loginHandler.bckBtn = 0;
                    }
                } else {
                    var date = new Date();
                    loginHandler.bckHou = date.getHours();
                    loginHandler.bckMin = date.getMinutes();
                    loginHandler.bckSec = date.getSeconds();
                    loginHandler.bckBtn = 1;
                }
            }, false );
        }, false);
      },

        loginProcessHandler: function() {
            var userName = $('#txtUserName').val();
            var password = $('#txtPassword').val();

            //  removing error message div while user click on username textbox
            $('#txtUserName').focus(function() {
                $('#loginError').removeClass('jumbotron');
                $('#loginError').html('');
            });

            //  removing error message div while user click on password box
            $('#txtPassword').focus(function() {
                $('#loginError').removeClass('jumbotron');
                $('#loginError').html('');
            });

            if (userName.trim() === '') {
                $('#txtUserNameError').text('* user name field can not be empty...');
                return;
            } else {
                $('#txtUserNameError').text('');
            }
            if (password.length < 3) {
                $('#txtPasswordError').text('* password must have at list 8 character...');
                return;
            } else {
                $('#txtPasswordError').text('');
            }
            SpinnerDialog.show("Login", "Loging to your account...", true);
            this.remoteCheck(userName, password);
        },

        //  check user name and password on remote server
        remoteCheck: function(userName, password) {
            var values = {
                userName: userName,
                password: password
            };

            $.ajax({
                url: loginHandler.ip+'?r=login/login',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    console.log(data);

                    //  checking if user validation is successfull in server or not
                    if (data.success === true) {
                      var result = loginHandler.SLObj.pushToken(data.accessToken);
                          //  redirecting user to dashbord-layout
                          var setFlag = new forgroundEventHandler();
                          setFlag.appStartFlagCheck();
                          document.addEventListener("resume", setFlag.appStartFlagCheck, false);
                          window.location.href = "./views/reach.html";
                          SpinnerDialog.hide();
                          // $('.container').empty();
                          // $('.container').load('././views/dashbord-layout.html');
                    } else {
                      SpinnerDialog.hide();
                      $('#txtUserName').val('');
                      $('#txtPassword').val('');
                      $('#loginError').addClass('jumbotron');
                      $('#loginError').html('user name or password is wrong');
                    }

                }
            }).fail(function(error) {
                console.error(error);
            });
        }
    }
    loginHandler.initialize();
    return loginHandler;
}
