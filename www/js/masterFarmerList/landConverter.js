var landConverter = function() {
    var laCon = {
        metterSqValue: null,
        initialize: function(value, currType, toType) {
            console.log('value: ' + value + '\n' + 'currType: ' + currType + '\n' + 'toType: ' + toType);
            laCon.metterSqValue = laCon.convertToMetterSq(value, currType);
            console.log(laCon.metterSqValue);
            return laCon.convertToRequired(laCon.metterSqValue, toType);
        },
        convertToMetterSq: function(value, currType) {
            switch (currType) {
                case '1':
                    return value*508.72;
                case '2':
                    return value*31.79;
                case '3':
                    return value*7.95;
                case '4':
                    return value*6772.63;
                case '5':
                    return value*338.63;
                case '6':
                    return value*16.93;
                case '7':
                  return value*1.99;
                case '8':
                return value*1;
            default:
                console.log('non of above option is matched');
            }
        },

        convertToRequired:function(value, toType) {
            switch (toType) {
                case '1':
                    return value/508.72;
                case '2':
                    return value/31.79;
                case '3':
                    return value/7.95;
                case '4':
                    return value/6772.63;
                case '5':
                    return value/338.63;
                case '6':
                    return value/16.93;
                case '7':
                    return value/1.99;
                case '8':
                    return value/1;
                default:
                    console.log('non of above option is matched');
            }
        }

    }
    return laCon;
}