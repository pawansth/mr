function initMap() {
      var infoWindow = new google.maps.InfoWindow();
      var latlngbounds = new google.maps.LatLngBounds();

      $(document).on('click', '.selectPlace', function() {
        $('#postalCode').val($('#postalCod').attr('postalCod'));
        $('#latOfLoc').attr('lat',$('#lat').attr('lat'));
        $('#latOfLoc').text('Latitude: '+$('#lat').attr('lat'));
        $('#lngOfLoc').attr('lng',$('#lng').attr('lng'));
        $('#lngOfLoc').text('Longitude: '+$('#lng').attr('lng'));
        $('#google-map').hide();
      });

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: 27.6948708, lng: 84.4246932}
        });
        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function() {
          geocodeAddress(geocoder, map);
        });

            // var map = new google.maps.Map(document.getElementById("map"), mapOptions);
            google.maps.event.addListener(map, 'click', function (e) {
                var latlng = new google.maps.LatLng(e.latLng.lat(), e.latLng.lng());
                var geocoder = geocoder = new google.maps.Geocoder();
                geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                      console.log(results);
                      var postal_code = '';
                      var num = ['0','1','2','3','4','5','6','7','8','9'];
                      var i = 0;
                      // console.log(results[0]);
                      $.each(results[0].address_components,function() {
                      var comValue = 0;
                      var str = results[0].address_components[i].long_name;
                      for (var j = 0; j < str.length; j++) {
                        var c = str.charAt(j);
                        if(jQuery.inArray( c, num) === -1) {
                        comValue = 1;
                        }
                      }
                      if(comValue === 0) {
                        postal_code = str;
                      }
                      i = i + 1;
                      });
                        if (results[1]) {
                          templete = '';
                          templete += '<span id="postalCod" class="locationDetail" postalCod="'+postal_code+'">Code:&nbsp;'+postal_code+'</span>'+
                                              '&nbsp;&nbsp;&nbsp;<span id="lat" class="locationDetail" lat="'+results[0].geometry.location.lat()+'">Latitude:&nbsp;'+results[0].geometry.location.lat()+'</span>'+
                                              '<br/><span id="lng" class="locationDetail" lng="'+results[0].geometry.location.lng()+'">longitude:&nbsp;'+results[0].geometry.location.lng()+'</span>'+
                                              '<button type="button" class="selectPlace btn btn-block btn-primary">Ok</button>';
                          // $('#orderList').after(templete);
                          // parentTr.find('.productName').popover({
                          $('#floating-panel').popover({
                              html: true,
                              placement: 'bottom',
                              // title: 'Location Detail',
                              content: function() {
                                  var temp = templete;
                                  return temp;
                              }
                          }).popover('show').blur(function() {
                              $(this).popover('hide');
                          });
                            // alert("Location: " + results[1].formatted_address + "\r\nLatitude: " + e.latLng.lat() + "\r\nLongitude: " + e.latLng.lng());
                        }
                    }
                });
            });
      }

      function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function(results, status) {
          if (status === 'OK') {
            // console.log(results[0].address_components[5].long_name);
            // alert(results[0].geometry.location.lat()+'\n'+results[0].geometry.location.lng()+'\n');
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
              map: resultsMap,
              position: results[0].geometry.location
            });
            var postal_code = '';
            var num = ['0','1','2','3','4','5','6','7','8','9'];
            var i = 0;
            console.log(results[0]);
            $.each(results[0].address_components,function() {
              // jQuery.inArray( "Pete", arr)
              var comValue = 0;
              var str = results[0].address_components[i].long_name;
              for (var j = 0; j < str.length; j++) {
                var c = str.charAt(j);
                if(jQuery.inArray( c, num) === -1) {
                  comValue = 1;
                  }
                }
              if(comValue === 0) {
                postal_code = str;
              }
              i = i + 1;
            });
            templete = '';
            templete += '<span id="postalCod" class="locationDetail" postalCod="'+postal_code+'">Code:&nbsp;'+postal_code+'</span>'+
                                '&nbsp;&nbsp;&nbsp;<span id="lat" class="locationDetail" lat="'+results[0].geometry.location.lat()+'">Latitude:&nbsp;'+results[0].geometry.location.lat()+'</span>'+
                                '<br/><span id="lng" class="locationDetail" lng="'+results[0].geometry.location.lng()+'">longitude:&nbsp;'+results[0].geometry.location.lng()+'</span>'+
                                '<button type="button" class="selectPlace btn btn-block btn-primary">Ok</button>';
            // $('#orderList').after(templete);
            // parentTr.find('.productName').popover({
            $('#floating-panel').popover({
                html: true,
                placement: 'bottom',
                // title: 'Location Detail',
                content: function() {
                    var temp = templete;
                    return temp;
                }
            }).popover('show').blur(function() {
                $(this).popover('hide');
            });
          } else {
            navigator.notification.alert(
                            'Geocode was not successful for the following reason: ' + status,
                            null,
                            'warning',
                            'OK'
                        );
          }
        });
      }


// (function (global) {
//     "use strict";

//     function onDeviceReady () {
//         document.addEventListener("online", onOnline, false);
//         document.addEventListener("resume", onResume, false);
//         loadMapsApi();
//     }

//     function onOnline () {
//         loadMapsApi();
//     }

//     function onResume () {
//         loadMapsApi();
//     }

//     function loadMapsApi () {
//         if(navigator.connection.type === Connection.NONE || google.maps) {
//             return;
//         }
//         $.getScript('https://maps.googleapis.com/maps/api/js?key=API_KEY&sensor=true&callback=onMapsApiLoaded');
//     }

//     global.onMapsApiLoaded = function () {
//         // Maps API loaded and ready to be used.
//         var map = new google.maps.Map(document.getElementById("map"), {});
//     };

//     document.addEventListener("deviceready", onDeviceReady, false);
// })(window);