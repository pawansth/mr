var updateCattle = {
    SQLLObj  : null,
    ip       : null,
    ipObj    : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    compData : null,
    distCount: null,
    retCount : null,
    compCount: null,
    camUpdate: null,
    globID : null,
    editIndex: 0,
    globalAccessToken: null,
    imgPath: null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateCattle.ipObj     = new ip();
        updateCattle.ip        = updateCattle.ipObj.remoteIpAddress();
        updateCattle.imgPath   = updateCattle.ipObj.remoteImgIp();
        updateCattle.ctr       = new controller();
        updateCattle.SQLLObj   = new SQLiteLogin();
        updateCattle.locSet    = new locationSet();
        updateCattle.camUpdate = new cameraForUpdate();
        updateCattle.addLoading();
        updateCattle.loadDist();

        updateCattle.distCount = 0;
        updateCattle.retCount  = 0;
        updateCattle.compCount = 0;
        var passedData      = window.localStorage.getItem('dataToPass');
        var dataPassed      = JSON.parse(passedData);
        updateCattle.globID    = dataPassed.cattleId;
        updateCattle.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#mapBtn', function() {
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.loadMpa();
            }
        });
        $(document).on('click', '#gpsBtn', function() {
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.setLocation();
            }
        });
        $(document).on('click', '.dist-add', function() {
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.addDist();
            }
        });
        $(document).on('click', '.dist-minus', function() {
            var obj = $(this);
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.remove(obj);
            }
        });
        $(document).on('click', '.ret-add', function() {
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.addRet();
            }
        });
        $(document).on('click', '.ret-minus', function() {
            var obj = $(this);
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.remove(obj);
            }
        });
        $(document).on('click', '.feed-add' , function() {
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.addFeedCompany();
            }
        });
        $(document).on('click', '.feed-minus', function() {
            var obj = $(this);
            if(!(updateCattle.editIndex === 0)) {
                updateCattle.remove(obj);
            }
        });
        $(document).on('click', '.send-Master-Cattle', updateCattle.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('input', '#feedUse', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('compId',$(this).attr('comp_id'));
             }
           });
        });
        $(document).on('click', '#btn-edit', function() {
            $('.title').text('Edit Cattle Farmer');
            updateCattle.edit();
        });
        updateCattle.noEdit();
    },
    setLocation: function() {
        updateCattle.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = updateCattle.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateCattle.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: updateCattle.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateCattle.distData = data;
                  updateCattle.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateCattle.loadDist,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
          var values = {
              accessToken: updateCattle.globalAccessToken
          };
          var path = updateCattle.ip + '?r=order-handler/ret-list';
          var result = updateCattle.ajaxCall(path, values);
              result.then(function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateCattle.retData = data;
                  updateCattle.loadFeedComp();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateCattle.loadRet,
                  'Error',
                  'Try Again'
              );
          });
    },
    // method that load all feed company data from server
    loadFeedComp: function() {
          var values = {
              accessToken: updateCattle.globalAccessToken
          };
          var path = updateCattle.ip + '?r=order-handler/comp-list';
          var result = updateCattle.ajaxCall(path, values);
              result.then(function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateCattle.compData = data;
                  updateCattle.loadCattle();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateCattle.loadFeedComp,
                  'Error',
                  'Try Again'
              );
          });
    },
    loadCattle: function() {
        var values = {
            accessToken: updateCattle.globalAccessToken,
            id         : updateCattle.globID
        };
        var path = updateCattle.ip + '?r=update-cattle/cattle-info';
        var result = updateCattle.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = '../index.html';
            }
            console.log(data);
            updateCattle.removeLoading();
            $('#photo').attr('src',updateCattle.imgPath+data[0].image_url);
            $('#farName').val(data[0].name);
            $('#farNumber').val(data[0].number);
            $('#latOfLoc').attr('lat',data[0].latitude);
            $('#latOfLoc').text(data[0].latitude);
            $('#lngOfLoc').attr('lng',data[0].longitude);
            $('#lngOfLoc').text(data[0].longitude);
            $('#postalCode').val(data[0].postal_code);
            $('#farAddress').val(data[0].address);
            $('#farCattle').val(data[0].cattle_number);
            $('#farDoctor').val(data[0].doctor_prefer);
            $('#farEmail').val(data[0].email);
            $('#farViber').val(data[0].viber);
            $('#farFacebook').val(data[0].facebook);
            // code to display dist data
            var distToDisplay = [];
            for(var i = 0; i<updateCattle.distData.length; i++) {
                for(var j = 0; j<data[2].length; j++) {
                    if(updateCattle.distData[i].id === data[2][j].dist_id) {
                        var presentData = {
                            rowId  : data[2][j].id,
                            dist_id: data[2][j].dist_id,
                            name: updateCattle.distData[i].dist_name
                        };
                        distToDisplay.push(presentData);
                    }
                }
            }
            $('.distList').empty();
            for(var i = 0; i<distToDisplay.length; i++) {
                var templete = '<div class="distData row">'+
                                    '<div class="col-xs-8" style="padding-top:10px;">'+
                                        '<input type="text" class="form-control" id="distNameSearch" value="'+distToDisplay[i].name+'" distId="'+distToDisplay[i].dist_id+'" rowId="'+distToDisplay[i].rowId+'" list="distList'+i+'" disabled placeholder="distributor name">'+
                                        '<datalist id="distList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus dist-add col-xs-2" style="margin-top:10px;"></button>'+
					            '</div>';
                $('.distList').append(templete);
                var target = '#distList'+i;
                var templeteOption = '';
                  
                $.each(updateCattle.distData, function() {
                    templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateCattle.distCount = distToDisplay.length;
            // code to display ret data
            var retToDisplay = [];
            for(var i = 0; i<updateCattle.retData.length; i++) {
                for(var j = 0; j<data[1].length; j++) {
                    if(updateCattle.retData[i].id === data[1][j].ret_id) {
                        var presentData = {
                            rowId  : data[1][j].id,
                            ret_id: data[1][j].ret_id,
                            name: updateCattle.retData[i].retail_name
                        };
                        retToDisplay.push(presentData);
                    }
                }
            }
            $('.retList').empty();
            for(var i = 0; i<retToDisplay.length; i++) {
                var templete = '<div class="retData row">'+
					                '<div class="col-xs-8" style="padding-top:10px;">'+
                                        '<input type="text" class="form-control" value="'+retToDisplay[i].name+'" retId="'+retToDisplay[i].ret_id+'" rowId="'+retToDisplay[i].rowId+'" id="retNameSearch" list="retList'+i+'" disabled placeholder="retailer name">'+
                                        '<datalist id="retList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:10px;"></button>'+
					            '</div>';
                $('.retList').append(templete);
                var target = '#retList'+i;
                var templeteOption = '';
                  
                $.each(updateCattle.retData, function() {
                    templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateCattle.retCount = retToDisplay.length;
            // Code for feed company crop
            var feedToDisplay = [];
            for(var i = 0; i<updateCattle.compData.length; i++) {
                for(var j = 0; j<data[3].length; j++) {
                    if(updateCattle.compData[i].id === data[3][j].feed_company) {
                        var presentData = {
                            rowId  : data[3][j].id,
                            feed_id: data[3][j].feed_company,
                            name: updateCattle.compData[i].compName
                        };
                        feedToDisplay.push(presentData);
                    }
                }
            }
            $('.feedList').empty();
            for(var i = 0; i<feedToDisplay.length; i++) {
                var templete = '<div class="feedData row">'+
                                    '<div class="col-xs-9" style="padding-top:10px;">'+
                                        '<input type="text" class="form-control" id="feedUse" compId="'+feedToDisplay[i].feed_id+'" value="'+feedToDisplay[i].name+'" rowId="'+feedToDisplay[i].rowId+'" disabled list="compList'+i+'" placeholder="feed use(company)">'+
                                        '<datalist id="compList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning col-xs-1 feed-add floating-add glyphicon glyphicon-plus" style="margin-top:10px;"></button>'+
                                '</div>';
                $('.feedList').append(templete);
            }
            updateCattle.compCount = feedToDisplay.length;
        }).catch(function(error) {
            updateCattle.removeLoading();
            console.error(error);
            navigator.notification.alert(
                ' There is error in network',
                updateCattle.loadFar,
                'Error',
                'Try Again'
            );
        });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:10px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+updateCattle.distCount+'" rowId="0" placeholder="distributor name">'+
                            '<datalist id="distList'+updateCattle.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:10px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:10px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+updateCattle.distCount;
        var templeteOption = '';
                  
        $.each(updateCattle.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        updateCattle.distCount = updateCattle.distCount + 1;
    },
    remove: function(obj) {
        obj.parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:10px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+updateCattle.retCount+'" rowId="0" placeholder="retailer name">'+
                        '<datalist id="retList'+updateCattle.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:10px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:10px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+updateCattle.retCount;
        var templeteOption = '';
                  
        $.each(updateCattle.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        updateCattle.retCount = updateCattle.retCount + 1;
    },
    addFeedCompany: function() {
        var templete = '';
        templete += '<div class="feedData row">'+
                        '<div class="col-xs-9" style="padding-top:10px;">'+
                            '<input type="text" class="form-control" id="feedUse" compId="" list="compList'+updateCattle.compCount+'" rowId="0" placeholder="feed use(company)">'+
                            '<datalist id="compList'+updateCattle.compCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 feed-add floating-add glyphicon glyphicon-plus" style="margin-top:10px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus feed-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:10px;"></button>'+
                    '</div>';
        $('.feedList').append(templete);
        var target = '#compList'+updateCattle.compCount;
        var templeteOption = '';
                  
        $.each(updateCattle.compData, function() {
            templeteOption += '<option comp_id="'+this.id+'" value="'+this.compName+'">';
        });
        $(target).append(templeteOption);
        updateCattle.compCount = updateCattle.compCount + 1;
    },
    isEmail: function() {
    var email = $('#farEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#farName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farCattle').val().trim() === '') {
            navigator.notification.alert(
                            'farmer cattle number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var compStatus = 0;
        $.each($('.feedData'), function() {
            if(($(this).find('#feedUse').val().trim() ==='')) {
                $(this).find('#feedUse').attr('compId','');
            } else {
                if($(this).find('#feedUse').attr('compId').trim() === '') {
                    compStatus = 1;
                }
            }
        });
        if(compStatus === 1) {
            navigator.notification.alert(
                            'feed(company) information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if(!($('#farEmail').val().trim() === '')) {
            var checkEmail = updateCattle.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
            return;
            }
        }
        updateCattle.addLoading();
        $('.loadingText').text('Sending......');
        updateCattle.sendRemote();
    },
    sendRemote: function() {
        var cattleFarInfo = [];
        cattleFarDetail = {
            name         : $('#farName').val(),
            number       : $('#farNumber').val(),
            lat          : $('#latOfLoc').attr('lat'),
            lng          : $('#lngOfLoc').attr('lng'),
            postalCode   : $('#postalCode').val(),
            address      : $('#farAddress').val(),
            cattle_number: $('#farCattle').val(),
            doctor_prefer: $('#farDoctor').val(),
            email        : $('#farEmail').val(),
            viber        : $('#farViber').val(),
            facebook     : $('#farFacebook').val()
        }
        cattleFarInfo.push(cattleFarDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                    id: $(this).find('#distNameSearch').attr('rowId'),
                    dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        cattleFarInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                    id: $(this).find('#retNameSearch').attr('rowId'),
                    ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        cattleFarInfo.push(retList);
        var feedList = [];
        $.each($('.feedData'), function() {
            if(!($(this).find('#feedUse').attr('compId').trim() === '')) {
                var data = {
                    id: $(this).find('#feedUse').attr('rowId'),
                    feed_company: $(this).find('#feedUse').attr('compId')
                }
                feedList.push(data);
            }
        });
        cattleFarInfo.push(feedList);
        var values = {
            id: updateCattle.globID,
            accessToken: updateCattle.globalAccessToken,
            cattleFarInfo: cattleFarInfo
        };
        var path = updateCattle.ip + '?r=update-cattle/make-update';
        var result = updateCattle.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                updateCattle.removeLoading();
                window.location.href = '../index.html';
            } else if(data.status === 0) {
                updateCattle.removeLoading();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    data.msgTitle,
                    'OK'
                );
            } else if(data.status === 1) {
                updateCattle.loadCattle();
                updateCattle.remove();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    'data.msgTitle',
                    'OK'
                );
                $('.title').text('Farmer Detail');
                updateCattle.noEdit();
            }
        }).catch(function(error) {
            console.error(error);
            updateCattle.removeLoading();
            navigator.notification.alert(
                    'Network error occured',
                    null,
                    'ERROR',
                    'OK'
                );
        });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    noEdit: function() {
        $('#farName').attr('disabled', true);
        $('#farNumber').attr('disabled', true);
        $('#latOfLoc').attr('disabled', true);
        $('#latOfLoc').attr('disabled', true);
        $('#lngOfLoc').attr('disabled', true);
        $('#lngOfLoc').attr('disabled', true);
        $('#postalCode').attr('disabled', true);
        $('#farAddress').attr('disabled', true);
        $('#farCattle').attr('disabled', true);
        $('#farDoctor').attr('disabled', true);
        $('#farEmail').attr('disabled', true);
        $('#farViber').attr('disabled', true);
        $('#farFacebook').attr('disabled', true);
        $('.send-Master-Cattle').attr('disabled', true);
    },
    edit: function() {
        updateCattle.editIndex = 1;
        $('#farName').attr('disabled', false);
        $('#farNumber').attr('disabled', false);
        $('#latOfLoc').attr('disabled', false);
        $('#latOfLoc').attr('disabled', false);
        $('#lngOfLoc').attr('disabled', false);
        $('#lngOfLoc').attr('disabled', false);
        $('#postalCode').attr('disabled', false);
        $('#farAddress').attr('disabled', false);
        $('#farCattle').attr('disabled', false);
        $('#farDoctor').attr('disabled', false);
        $('#farEmail').attr('disabled', false);
        $('#farViber').attr('disabled', false);
        $('#farFacebook').attr('disabled', false);
        $('.send-Master-Cattle').attr('disabled', false);
        $.each($('.distData'), function() {
            $(this).find('#distNameSearch').attr('disabled', false);
        });
        $.each($('.retData'), function() {
            $(this).find('#retNameSearch').attr('disabled', false);
        });
        $.each($('.feedData'), function() {
            $(this).find('#feedUse').attr('disabled', false);
        });
        $('.div-image-updater').load('./imageUpdater.html', function() {
            $('.div-prev-img').hide();
            $(document).on('click', '.div-def-img', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1',function() {
            updateCattle.camUpdate.openDefaultCamera();
        });
        $(document).on('click', '.div-img-change-opt-2', function() {
            updateCattle.camUpdate.openFilePicker();
        });
        $(document).on('click', '.div-change-image-container', function() {
            $('.div-change-image-container').css({left: '-100%'});
        });
        $(document).on('click', '.btn-done-upload', function() {
            updateCattle.addLoading();
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('.img-prev').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to store field data
            var params         = new Object();
            params.accessToken = updateCattle.globalAccessToken;
            params.id          = updateCattle.globID;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;
            // Creating object for file transfer class
            var ft = new FileTransfer();
            ft.upload(imageData, updateCattle.ip + '?r=update-cattle/photo-update', function(result){
              console.log('data is: ',result);
              updateCattle.removeLoading();
              var response = $.parseJSON(result.response);
              if(response.status === 1) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      $('#photo').attr('src',$('.img-prev').attr('src'));
                      $('.img-prev').hide();
              }
              if(response.status === 0) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                    $('.img-prev').hide();
              }
              if(response.status === 2) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      window.location.href = "../index.html";
              }
            }, function(error){
              console.error(error);
              updateCattle.removeLoading();
              $('.img-prev').hide();
              navigator.notification.alert(
                'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                null,
                'ERROR',
                'OK'
              );
            }, options);
        });
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
        });
    }
}
updateCattle.initialize();