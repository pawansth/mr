var updateFar = {
    SQLLObj  : null,
    ip       : null,
    ipObj    : null,
    ctr      : null,
    locSet   : null,
    lanConv  : null,
    currType : null,
    distData : null,
    retData  : null,
    distCount: null,
    retCount : null,
    globID : null,
    camUpdate: null,
    editIndex: 0,
    globalAccessToken: null,
    imgPath: null,

    initialize: function() {
        $('#google-map').hide();
        document.addEventListener('deviceready', function () {
            screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
            updateFar.ipObj     = new ip();
            updateFar.ip        = updateFar.ipObj.remoteIpAddress();
            updateFar.imgPath   = updateFar.ipObj.remoteImgIp();
            updateFar.ctr       = new controller();
            updateFar.SQLLObj   = new SQLiteLogin();
            updateFar.locSet    = new locationSet();
            updateFar.camUpdate = new cameraForUpdate();
        
            updateFar.addLoading();
            updateFar.loadDist();
        
            updateFar.distCount = 0;
            updateFar.retCount  = 0;
            updateFar.currType  = '1';
            updateFar.lanConv   = new landConverter();
            var passedData      = window.localStorage.getItem('dataToPass');
            var dataPassed      = JSON.parse(passedData);
            updateFar.globID    = dataPassed.farId;
            updateFar.bindEvents();
        },false);
    },
    bindEvents: function() {
        $(document).on('click', '#mapBtn', function() {
            if(!(updateFar.editIndex === 0)) {
                updateFar.loadMpa();
            }
        });
        $(document).on('click', '#gpsBtn', function() {
            if(!(updateFar.editIndex === 0)) {
                updateFar.setLocation();
            }
        });
        $(document).on('change', '#landMeasure', updateFar.areaConverter);
        $(document).on('click', '.dist-add', function() {
            if(!(updateFar.editIndex === 0)) {
                updateFar.addDist();
            }
        });
        $(document).on('click', '.dist-minus', function() {
            var obj = $(this);
            if(!(updateFar.editIndex === 0)) {
                updateFar.remove(obj);
            }
        });
        $(document).on('click', '.ret-add', function() {
            if(!(updateFar.editIndex === 0)) {
                updateFar.addRet();
            }
        });
        $(document).on('click', '.ret-minus', function() {
            var obj = $(this);
            if(!(updateFar.editIndex === 0)) {
                updateFar.remove(obj);
            }
        });
        $(document).on('click', '.crop-add' , function() {
            if(!(updateFar.editIndex === 0)) {
                updateFar.addCrop();
            }
        });
        $(document).on('click', '.crop-minus', function() {
            var obj = $(this);
            if(!(updateFar.editIndex === 0)) {
                updateFar.remove(obj);
            }
        });
        $(document).on('click', '.send-Master-farmer', updateFar.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('click', '#btn-edit', function() {
            $('.title').text('Farmer Edit');
            updateFar.edit();
        });
        updateFar.noEdit();
    },
    setLocation: function() {
    updateFar.locSet.setPosition();
  },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    //  Function that convert land area to different area
    areaConverter: function() {
        var value = updateFar.lanConv.initialize($('#farArea').val(),updateFar.currType,$('#landMeasure').val());
        updateFar.currType = $('#landMeasure').val();
        $('#farArea').val(value);
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = updateFar.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateFar.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: updateFar.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateFar.distData = data;
                //   var templete = '';
                //   // templete += '<ul>';

                //   // display response  data in list
                //   $.each(data, function() {
                //     // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                //       templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                //   });
                //   // templete += '</ul>';
                //   $('#distList').append(templete);
                  updateFar.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
          var values = {
              accessToken: updateFar.globalAccessToken
          };
          var path = updateFar.ip + '?r=order-handler/ret-list';
          var result = updateFar.ajaxCall(path, values);


              result.then(function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateFar.retData = data;
                //   var templete = '';
                //   // templete += '<ul>';

                //   // display response  data in list
                //   $.each(data, function() {
                //     // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                //       templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                //   });
                //   // templete += '</ul>';
                //   $('#retList').append(templete);
                  updateFar.loadFar();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });
    },
    loadFar: function() {
        var values = {
            accessToken: updateFar.globalAccessToken,
            id         : updateFar.globID
        };
        var path = updateFar.ip + '?r=update-farmer/farmer-info';
        var result = updateFar.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = '../index.html';
            }
            console.log(data);
            updateFar.removeLoading();
            $('#photo').attr('src',updateFar.imgPath+data[0].image_url);
            $('#farName').val(data[0].name);
            $('#farNumber').val(data[0].phone_no);
            $('#latOfLoc').attr('lat',data[0].latitude);
            $('#latOfLoc').text(data[0].latitude);
            $('#lngOfLoc').attr('lng',data[0].longitude);
            $('#lngOfLoc').text(data[0].longitude);
            $('#postalCode').val(data[0].postal_code);
            $('#farAddress').val(data[0].address);
            $('#farEmail').val(data[0].email);
            $('#farViber').val(data[0].viber);
            $('#farFacebook').val(data[0].facebook);
            var landType      = data[0].area_type;
            var area          = data[0].area;
            var convertedArea = updateFar.lanConv.initialize(area, '8', landType.toString());
            $('#farArea').val(convertedArea);
            $('#landMeasure').val(landType.toString());
            // code to display dist data
            var distToDisplay = [];
            for(var i = 0; i<updateFar.distData.length; i++) {
                for(var j = 0; j<data[2].length; j++) {
                    if(updateFar.distData[i].id === data[2][j].dist_id) {
                        var presentData = {
                            rowId  : data[2][j].id,
                            dist_id: data[2][j].dist_id,
                            name: updateFar.distData[i].dist_name
                        };
                        distToDisplay.push(presentData);
                    }
                }
            }
            $('.distList').empty();
            for(var i = 0; i<distToDisplay.length; i++) {
                var templete = '<div class="distData row">'+
                                    '<div class="col-xs-8" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" id="distNameSearch" value="'+distToDisplay[i].name+'" distId="'+distToDisplay[i].dist_id+'" rowId="'+distToDisplay[i].rowId+'" list="distList'+i+'" disabled placeholder="distributor name">'+
                                        '<datalist id="distList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus dist-add col-xs-2" style="margin-top:5px;"></button>'+
					            '</div>';
                $('.distList').append(templete);
                var target = '#distList'+i;
                var templeteOption = '';
                  
                $.each(updateFar.distData, function() {
                    templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateFar.distCount = distToDisplay.length;
            // code to display ret data
            var retToDisplay = [];
            for(var i = 0; i<updateFar.retData.length; i++) {
                for(var j = 0; j<data[1].length; j++) {
                    if(updateFar.retData[i].id === data[1][j].ret_id) {
                        var presentData = {
                            rowId  : data[1][j].id,
                            ret_id: data[1][j].ret_id,
                            name: updateFar.retData[i].retail_name
                        };
                        retToDisplay.push(presentData);
                    }
                }
            }
            $('.retList').empty();
            for(var i = 0; i<retToDisplay.length; i++) {
                var templete = '<div class="retData row">'+
					                '<div class="col-xs-8" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" value="'+retToDisplay[i].name+'" retId="'+retToDisplay[i].ret_id+'" rowId="'+retToDisplay[i].rowId+'" id="retNameSearch" list="retList'+i+'" disabled placeholder="retailer name">'+
                                        '<datalist id="retList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
					            '</div>';
                $('.retList').append(templete);
                var target = '#retList'+i;
                var templeteOption = '';
                  
                $.each(updateFar.retData, function() {
                    templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateFar.retCount = retToDisplay.length;
            // Code for cultivated crop
            $('.cropList').empty();
            for(var i = 0; i<data[3].length; i++) {
                var templete = '<div class="cropData row">'+
                                    '<div class="col-xs-9" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" id="cultCrop" value="'+data[3][i].crop+'" rowId="'+data[3][i].id+'" disabled placeholder="cultivated crop">'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning col-xs-1 crop-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                                '</div>';
                $('.cropList').append(templete);
            }
        }).catch(function(error) {
            updateFar.removeLoading();
            console.error(error);
            navigator.notification.alert(
                ' There is error in network',
                updateFar.loadFar,
                'Error',
                'Try Again'
            );
        });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+updateFar.distCount+'" rowId="0" placeholder="distributor name">'+
                            '<datalist id="distList'+updateFar.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+updateFar.distCount;
        var templeteOption = '';
                  
        $.each(updateFar.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        updateFar.distCount = updateFar.distCount + 1;
    },
    remove: function(obj) {
        obj.parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+updateFar.retCount+'" rowId="0" placeholder="retailer name">'+
                        '<datalist id="retList'+updateFar.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+updateFar.retCount;
        var templeteOption = '';
                  
        $.each(updateFar.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        updateFar.retCount = updateFar.retCount + 1;
    },
    addCrop: function() {
        var templete = '';
        templete += '<div class="cropData row">'+
                        '<div class="col-xs-9" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="cultCrop" rowId="0" placeholder="cultivated crop">'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 crop-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus crop-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:5px;"></button>'+
                    '</div>';
        $('.cropList').append(templete);
    },
    isEmail: function() {
    var email = $('#farEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#farName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farArea').val().trim() === '') {
            navigator.notification.alert(
                            'area can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        // Checking if email address is empty or not
        if(!($('#farEmail').val().trim() === '')) {
            var checkEmail = updateFar.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
            return;
            }
        }
        updateFar.addLoading();
        $('.loadingText').text('Updating......');
        updateFar.sendRemote();
    },
    sendRemote: function() {
        var farmerInfo = [];
        farDetail = {
            name: $('#farName').val(),
            number: $('#farNumber').val(),
            lat: $('#latOfLoc').attr('lat'),
            lng: $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address: $('#farAddress').val(),
            email: $('#farEmail').val(),
            viber: $('#farViber').val(),
            facebook: $('#farFacebook').val(),
            area: $('#farArea').val(),
            measureType: $('#landMeasure').val()
        }
        farmerInfo.push(farDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                    id: $(this).find('#distNameSearch').attr('rowId'),
                    dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        farmerInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                    id: $(this).find('#retNameSearch').attr('rowId'),
                    ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        farmerInfo.push(retList);
        var cropList = [];
        $.each($('.cropData'), function() {
            if(!($(this).find('#cultCrop').val().trim() === '')) {
                var data = {
                    id: $(this).find('#cultCrop').attr('rowId'),
                    crop: $(this).find('#cultCrop').val()
                }
                cropList.push(data);
            }
        });
        farmerInfo.push(cropList);
        console.log(farmerInfo);
        var values = {
            accessToken: updateFar.globalAccessToken,
            id         : updateFar.globID,
            farmerInfo : farmerInfo
        }
        var path = updateFar.ip+'?r=update-farmer/make-update';
        var result = updateFar.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                updateFar.removeLoading();
                window.location.href = '../index.html';
            } else if(data.status === 0) {
                updateFar.removeLoading();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    data.msgTitle,
                    'OK'
                );
            } else if(data.status === 1) {
                updateFar.loadFar();
                updateFar.remove();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    'data.msgTitle',
                    'OK'
                );
                $('.title').text('Farmer Detail');
                updateFar.noEdit();
            }
        }).catch(function(error) {
            updateFar.removeLoading();
            console.error(error);
            navigator.notification.alert(
                ' There is error in network',
                null,
                'Error',
                'Try Again'
            );
        });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    noEdit: function() {
        $('#farName').attr('disabled', true);
        $('#farNumber').attr('disabled', true);
        $('#postalCode').attr('disabled', true);
        $('#farAddress').attr('disabled', true);
        $('#farEmail').attr('disabled', true);
        $('#farViber').attr('disabled', true);
        $('#farFacebook').attr('disabled', true);
        $('#farArea').attr('disabled', true);
        $('#landMeasure').attr('disabled', true);
        $('.send-Master-farmer').attr('disabled', true);
    },
    edit: function() {
        updateFar.editIndex = 1;
        $('#farName').attr('disabled', false);
        $('#farNumber').attr('disabled', false);
        $('#postalCode').attr('disabled', false);
        $('#farAddress').attr('disabled', false);
        $('#farEmail').attr('disabled', false);
        $('#farViber').attr('disabled', false);
        $('#farFacebook').attr('disabled', false);
        $('#farArea').attr('disabled', false);
        $('#landMeasure').attr('disabled', false);
        $('.send-Master-farmer').attr('disabled', false);
        $.each($('.distData'), function() {
            $(this).find('#distNameSearch').attr('disabled', false);
        });
        $.each($('.retData'), function() {
            $(this).find('#retNameSearch').attr('disabled', false);
        });
        $.each($('.cropData'), function() {
            $(this).find('#cultCrop').attr('disabled', false);
        });
        $('.div-image-updater').load('./imageUpdater.html', function() {
            $('.div-prev-img').hide();
            $(document).on('click', '.div-def-img', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1',function() {
            updateFar.camUpdate.openDefaultCamera();
        });
        $(document).on('click', '.div-img-change-opt-2', function() {
            updateFar.camUpdate.openFilePicker();
        });
        $(document).on('click', '.div-change-image-container', function() {
            $('.div-change-image-container').css({left: '-100%'});
        });
        $(document).on('click', '.btn-done-upload', function() {
            updateFar.addLoading();
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('.img-prev').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to store field data
            var params         = new Object();
            params.accessToken = updateFar.globalAccessToken;
            params.id          = updateFar.globID;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;
            // Creating object for file transfer class
            var ft = new FileTransfer();
            ft.upload(imageData, updateFar.ip + '?r=update-farmer/photo-update', function(result){
              console.log('data is: ',result);
              updateFar.removeLoading();
              var response = $.parseJSON(result.response);
              if(response.status === 1) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      $('#photo').attr('src',$('.img-prev').attr('src'));
                      $('.img-prev').hide();
              }
              if(response.status === 0) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                    $('.img-prev').hide();
              }
              if(response.status === 2) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      window.location.href = "../index.html";
              }
            }, function(error){
              console.error(error);
              updateFar.removeLoading();
              $('.img-prev').hide();
              navigator.notification.alert(
                'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                null,
                'ERROR',
                'OK'
              );
            }, options);
        });
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
        });
    }
}
updateFar.initialize();