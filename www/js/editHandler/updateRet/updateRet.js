var updateRet = {
    SQLLObj: null,
    ipObj  : null,
    ip     : null,
    imgPath: null,
    ctr    : null,
    locSet : null,
    cam    : null,
    camUpdate: null,
    globID : null,
    editIndex: 0,
    globalAccessToken: null,
    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateRet.ipObj     = new ip();
        updateRet.ip        = updateRet.ipObj.remoteIpAddress();
        updateRet.imgPath   = updateRet.ipObj.remoteImgIp();
        updateRet.ctr       = new controller();
        updateRet.SQLLObj   = new SQLiteLogin();
        updateRet.locSet    = new locationSet();
        updateRet.cam       = new camera();
        updateRet.camUpdate = new cameraForUpdate();
        $('#google-map').hide();
        var passedData = window.localStorage.getItem('dataToPass');
        var dataPassed = JSON.parse(passedData);
        updateRet.globID = dataPassed.retId;
        updateRet.loadRet(updateRet.globID);
        updateRet.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#mapBtn', function() {
            if(!(updateRet.editIndex === 0)) {
                updateRet.loadMpa();
            }
        });
        $(document).on('click', '#gpsBtn', function() {
            if(!(updateRet.editIndex === 0)) {
                updateRet.setLocation();
            }
        });
        $(document).on('click', '#btn-edit', updateRet.edit);
        $(document).on('click', '#btnRetInfoInsert', updateRet.check);
    },
    // method that show loading module
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    // method that remove loading module
    removeLoading: function() {
        $('.loading').remove();
    },
    setLocation: function() {
        updateRet.locSet.setPosition();
    },
    loadMpa: function() {
        $('#google-map').show();
    },
    loadRet: function(retId) {
        updateRet.addLoading();
        var tokenInformations = updateRet.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
                updateRet.globalAccessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken: accessToken,
                id         : retId
            };
            var path = updateRet.ip + '?r=ret-update/retailer-info';
            var result = updateRet.ajaxCall(path, values);

            result.then(function(data) {
                updateRet.removeLoading();
                console.log(data);
                if (data.status === 2) {
                    window.location.href = "../index.html";
                }
                $('#photo').attr('src', updateRet.imgPath+data.image_url);
                $('#retName').val(data.retail_name);
                $('#retOwner').val(data.retail_owner);
                $('#latOfLoc').attr('lat',data.latitude);
                $('#lngOfLoc').attr('lng',data.longitude);
                $('#latOfLoc').text(data.latitude);
                $('#lngOfLoc').text(data.longitude);
                $('#postalCode').val(data.retail_postalcode);
                $('#retCountry').val(data.retail_country);
                $('#retCity').val(data.retail_city);
                $('#retDistrict').val(data.retail_district);
                $('#retProvince').val(data.retail_province);
                $('#retPhone').val(data.retail_phone);
                $('#retMobile').val(data.retail_mobile);
                $('#retEmail').val(data.retail_email);
                $('#retFacebook').val(data.retail_facebook);
                $('#retViber').val(data.retail_viber);
            }).catch(function(error) {
                updateRet.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    updateRet.loadRet,
                    'Error',
                    'TRY AGAIN'
                );
            });
            updateRet.noEdit();
        }).fail(function(error) {
            updateRet.removeLoading();
            console.error(error);
        });
    },
    noEdit: function() {
        $('#retName').attr('disabled', true);
        $('#retOwner').attr('disabled', true);
        $('#postalCode').attr('disabled', true);
        $('#retCountry').attr('disabled', true);
        $('#retCity').attr('disabled', true);
        $('#retDistrict').attr('disabled', true);
        $('#retProvince').attr('disabled', true);
        $('#retPhone').attr('disabled', true);
        $('#retMobile').attr('disabled', true);
        $('#retEmail').attr('disabled', true);
        $('#retFacebook').attr('disabled', true);
        $('#retViber').attr('disabled', true);
        $('#btnDistInfoInsert').attr('disabled', true);
    },
    edit: function() {
        updateRet.editIndex = 1;
        $('#retName').attr('disabled', false);
        $('#retOwner').attr('disabled', false);
        $('#postalCode').attr('disabled', false);
        $('#retCountry').attr('disabled', false);
        $('#retCity').attr('disabled', false);
        $('#retDistrict').attr('disabled', false);
        $('#retProvince').attr('disabled', false);
        $('#retPhone').attr('disabled', false);
        $('#retMobile').attr('disabled', false);
        $('#retEmail').attr('disabled', false);
        $('#retFacebook').attr('disabled', false);
        $('#retViber').attr('disabled', false);
        $('#btnDistInfoInsert').attr('disabled', false);
        $('.title').text('Edit Retailer');
        $('.div-image-updater').load('./imageUpdater.html', function() {
            $('.div-prev-img').hide();
            $(document).on('click', '.div-def-img', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1',function() {
            updateRet.camUpdate.openDefaultCamera();
        });
        $(document).on('click', '.div-img-change-opt-2', function() {
            updateRet.camUpdate.openFilePicker();
        });
        $(document).on('click', '.div-change-image-container', function() {
            $('.div-change-image-container').css({left: '-100%'});
        });
        $(document).on('click', '.btn-done-upload', function() {
            updateRet.addLoading();
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('.img-prev').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to store field data
            var params         = new Object();
            params.accessToken = updateRet.globalAccessToken;
            params.id          = updateRet.globID;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;
            // Creating object for file transfer class
            var ft = new FileTransfer();
            ft.upload(imageData, updateRet.ip + '?r=ret-update/photo-update', function(result){
              console.log('data is: ',result);
              updateRet.removeLoading();
              var response = $.parseJSON(result.response);
              if(response.status === 1) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      $('#photo').attr('src',$('.img-prev').attr('src'));
                      $('.img-prev').hide();
              }
              if(response.status === 0) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                    $('.img-prev').hide();
              }
              if(response.status === 2) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      window.location.href = "../index.html";
              }
            }, function(error){
              console.error(error);
              updateRet.removeLoading();
              $('.img-prev').hide();
              navigator.notification.alert(
                'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                null,
                'ERROR',
                'OK'
              );
            }, options);
        });
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
        });
    },
    check: function() {
        var retName         = $('#retName').val();
        var retOwner        = $('#retOwner').val();
        var retPostalCode = $('#postalCode').val();
        var retLat             = $('#latOfLoc').attr('lat');
        var retLng            = $('#lngOfLoc').attr('lng');
        var retCountry      = $('#retCountry').val();
        var retCity            = $('#retCity').val();
        var retDistrict       = $('#retDistrict').val();
        var retProvince     = $('#retProvince').val();
        var retPhone         = $('#retPhone').val();
        var retMobile        = $('#retMobile').val();
        var retEmail          = $('#retEmail').val();
        var retFacebook    = $('#retFacebook').val();
        var retViber           = $('#retViber').val();

        //  checking for input field is empty or not
        if (retName.trim() === '') {
          navigator.notification.alert(
                        'please enter business name.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        if (retOwner.trim() === '') {
          navigator.notification.alert(
                        'please enter business owner name.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        if(retLat.trim() === '' || retLng.trim() === '') {
          navigator.notification.alert(
                        'please select retailer location.',
                        null,
                        'Error',
                        'OK'
                    );
                    return;
        }
        if (retPostalCode.trim() === '') {
          navigator.notification.alert(
                        'please enter postal code.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        if (retCountry.trim() === '') {
          navigator.notification.alert(
                        'please enter country name.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        if (retCity.trim() === '') {
          navigator.notification.alert(
                        'please enter city name.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        if (retDistrict.trim() === '') {
          navigator.notification.alert(
                        'please enter district name.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        if (retMobile.trim() === '') {
          navigator.notification.alert(
                        'please enter mobile number.',
                        null,
                        'Error',
                        'OK'
                    );
          return;
        }
        // checking Email field is empty or not
        if(!(retEmail.trim() === '')) {
          var checkEmail = updateRet.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
                            return;
            }
        }
        updateRet.updateToServer();
    },
    updateToServer: function() {
        updateRet.addLoading();
        var values = {
            accessToken       : updateRet.globalAccessToken,
            id                : updateRet.globID,
            retail_name       : $('#retName').val(),
            retail_owner      : $('#retOwner').val(),
            latitude          : $('#latOfLoc').attr('lat'),
            longitude         : $('#lngOfLoc').attr('lng'),
            retail_postalcode : $('#postalCode').val(),
            retail_country    : $('#retCountry').val(),
            retail_city       : $('#retCity').val(),
            retail_district   : $('#retDistrict').val(),
            retail_province   : $('#retProvince').val(),
            retail_phone      : $('#retPhone').val(),
            retail_mobile     : $('#retMobile').val(),
            retail_email      : $('#retEmail').val(),
            retail_facebook   : $('#retFacebook').val(),
            retail_viber      : $('#retViber').val()
        };
        var path = updateRet.ip + '?r=ret-update/make-update';
        var result = updateRet.ajaxCall(path, values);
        result.then(function(data) {
            updateRet.removeLoading();
            console.log(data);
            if (data.status === 2) {
                window.location.href = "../index.html";
            }
            if(data.status === 0) {
                navigator.notification.alert(
                    data.msg,
                    null,
                    data.title,
                    'OK'
                );
            }
            updateRet.editIndex = 0;
            $('#photo').attr('src', updateRet.imgPath+data.image_url);
            $('#retName').val(data.retail_name);
            $('#retOwner').val(data.retail_owner);
            $('#latOfLoc').attr('lat',data.latitude);
            $('#lngOfLoc').attr('lng',data.longitude);
            $('#latOfLoc').text(data.latitude);
            $('#lngOfLoc').text(data.longitude);
            $('#postalCode').val(data.retail_postalcode);
            $('#retCountry').val(data.retail_country);
            $('#retCity').val(data.retail_city);
            $('#retDistrict').val('retail_district');
            $('#retProvince').val('retail_province');
            $('#retPhone').val(data.retail_phone);
            $('#retMobile').val(data.retail_mobile);
            $('#retEmail').val(data.retail_email);
            $('#retFacebook').val(data.retail_facebook);
            $('#retViber').val(data.retail_viber);
            $('.title').text('Retailer Detail');
        }).catch(function(error) {
            updateRet.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                null,
                'Error',
                'OK'
            );
        });
        updateRet.noEdit();
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    isEmail: function() {
    var email = $('#retEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  }
}
updateRet.initialize();