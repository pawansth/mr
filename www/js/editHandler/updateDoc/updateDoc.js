var updateDoc = {
    SQLLObj  : null,
    ip       : null,
    ipObj    : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    distCount: null,
    retCount : null,
    globID   : null,
    camUpdate: null,
    editIndex: 0,
    globalAccessToken: null,
    imgPath  : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateDoc.ipObj     = new ip();
        updateDoc.ip        = updateDoc.ipObj.remoteIpAddress();
        updateDoc.imgPath   = updateDoc.ipObj.remoteImgIp();
        updateDoc.ctr       = new controller();
        updateDoc.SQLLObj   = new SQLiteLogin();
        updateDoc.locSet    = new locationSet();
        updateDoc.camUpdate = new cameraForUpdate();

        updateDoc.addLoading();
        updateDoc.loadDist();

        updateDoc.distCount = 0;
        updateDoc.retCount  = 0;
        var passedData      = window.localStorage.getItem('dataToPass');
        var dataPassed      = JSON.parse(passedData);
        updateDoc.globID    = dataPassed.docId;
        updateDoc.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#mapBtn', function() {
            if(!(updateDoc.editIndex === 0)) {
                updateDoc.loadMpa();
            }
        });
        $(document).on('click', '#gpsBtn', function() {
            if(!(updateDoc.editIndex === 0)) {
                updateDoc.setLocation();
            }
        });
        $(document).on('click', '.dist-add', function() {
            if(!(updateDoc.editIndex === 0)) {
                updateDoc.addDist();
            }
        });
        $(document).on('click', '.dist-minus', function() {
            var obj = $(this);
            if(!(updateDoc.editIndex === 0)) {
                updateDoc.remove(obj);
            }
        });
        $(document).on('click', '.ret-add', function() {
            if(!(updateDoc.editIndex === 0)) {
                updateDoc.addRet();
            }
        });
        $(document).on('click', '.ret-minus', function() {
            var obj = $(this);
            if(!(updateDoc.editIndex === 0)) {
                updateDoc.remove(obj);
            }
        });
        $(document).on('click', '.send-Master-Doctor', updateDoc.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('click', '#btn-edit', function() {
            $('.title').text('Edit Doctor');
            updateDoc.edit();
        });
        updateDoc.noEdit();
    },
    setLocation: function() {
        updateDoc.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = updateDoc.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateDoc.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: updateDoc.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateDoc.distData = data;
                  updateDoc.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateDoc.loadDist,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
          var values = {
              accessToken: updateDoc.globalAccessToken
          };
          var path = updateDoc.ip + '?r=order-handler/ret-list';
          var result = updateDoc.ajaxCall(path, values);
          result.then(function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateDoc.retData = data;
                  updateDoc.loadDoc();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  'There is error in network',
                  updateDoc.loadRet,
                  'Error',
                  'Try Again'
              );
          });
    },
    loadDoc: function() {
        var values = {
            accessToken: updateDoc.globalAccessToken,
            id         : updateDoc.globID
        };
        var path = updateDoc.ip + '?r=update-doctor/doctor-info';
        var result = updateDoc.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = '../index.html';
            }
            console.log(data);
            updateDoc.removeLoading();
            $('#photo').attr('src',updateDoc.imgPath+data[0].image_url);
            $('#docName').val(data[0].name);
            $('#docNumber').val(data[0].number);
            $('#latOfLoc').attr('lat',data[0].latitude);
            $('#latOfLoc').text(data[0].latitude);
            $('#lngOfLoc').attr('lng',data[0].longitude);
            $('#lngOfLoc').text(data[0].longitude);
            $('#postalCode').val(data[0].postal_code);
            $('#docAddress').val(data[0].address);
            $('#docWorkingClinic').val(data[0].working_clinic);
            $('#docEmail').val(data[0].email);
            $('#docViber').val(data[0].viber);
            $('#docFacebook').val(data[0].facebook);
            // code to display dist data
            var distToDisplay = [];
            for(var i = 0; i<updateDoc.distData.length; i++) {
                for(var j = 0; j<data[2].length; j++) {
                    if(updateDoc.distData[i].id === data[2][j].dist_id) {
                        var presentData = {
                            rowId  : data[2][j].id,
                            dist_id: data[2][j].dist_id,
                            name: updateDoc.distData[i].dist_name
                        };
                        distToDisplay.push(presentData);
                    }
                }
            }
            $('.distList').empty();
            for(var i = 0; i<distToDisplay.length; i++) {
                var templete = '<div class="distData row">'+
                                    '<div class="col-xs-8" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" id="distNameSearch" value="'+distToDisplay[i].name+'" distId="'+distToDisplay[i].dist_id+'" rowId="'+distToDisplay[i].rowId+'" list="distList'+i+'" disabled placeholder="distributor name">'+
                                        '<datalist id="distList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus dist-add col-xs-2" style="margin-top:5px;"></button>'+
					            '</div>';
                $('.distList').append(templete);
                var target = '#distList'+i;
                var templeteOption = '';
                  
                $.each(updateDoc.distData, function() {
                    templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateDoc.distCount = distToDisplay.length;
            // code to display ret data
            var retToDisplay = [];
            for(var i = 0; i<updateDoc.retData.length; i++) {
                for(var j = 0; j<data[1].length; j++) {
                    if(updateDoc.retData[i].id === data[1][j].ret_id) {
                        var presentData = {
                            rowId  : data[1][j].id,
                            ret_id: data[1][j].ret_id,
                            name: updateDoc.retData[i].retail_name
                        };
                        retToDisplay.push(presentData);
                    }
                }
            }
            $('.retList').empty();
            for(var i = 0; i<retToDisplay.length; i++) {
                var templete = '<div class="retData row">'+
					                '<div class="col-xs-8" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" value="'+retToDisplay[i].name+'" retId="'+retToDisplay[i].ret_id+'" rowId="'+retToDisplay[i].rowId+'" id="retNameSearch" list="retList'+i+'" disabled placeholder="retailer name">'+
                                        '<datalist id="retList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
					            '</div>';
                $('.retList').append(templete);
                var target = '#retList'+i;
                var templeteOption = '';
                  
                $.each(updateDoc.retData, function() {
                    templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateDoc.retCount = retToDisplay.length;
            updateDoc.removeLoading();
        }).catch(function(error) {
            console.error(error);
            navigator.notification.alert(
                ' There is error in network',
                updateDoc.loaddoc,
                'Error',
                'Try Again'
            );
        });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+updateDoc.distCount+'" rowId="0" placeholder="distributor name">'+
                            '<datalist id="distList'+updateDoc.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="padding-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="padding-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+updateDoc.distCount;
        var templeteOption = '';
                  
        $.each(updateDoc.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        updateDoc.distCount = updateDoc.distCount + 1;
    },
    remove: function(obj) {
        obj.parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+updateDoc.retCount+'" rowId="0" placeholder="retailer name">'+
                        '<datalist id="retList'+updateDoc.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+updateDoc.retCount;
        var templeteOption = '';
                  
        $.each(updateDoc.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        updateDoc.retCount = updateDoc.retCount + 1;
    },
    isEmail: function() {
    var email = $('#docEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#docName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#docNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#docAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#docWorkingClinic').val().trim() === '') {
            navigator.notification.alert(
                            'working clinic can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        // Checking email field is empty or not
        if(!($('#docEmail').val().trim() === '')) {
            var checkEmail = updateDoc.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
                        return;
            }
        }
        updateDoc.addLoading();
        $('.loadingText').text('Sending......');
        updateDoc.sendRemote();
    },
    sendRemote: function() {
        var docInfo = [];
        docDetail = {
            name: $('#docName').val(),
            number: $('#docNumber').val(),
            lat: $('#latOfLoc').attr('lat'),
            lng: $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address: $('#docAddress').val(),
            workingClinic: $('#docWorkingClinic').val(),
            email: $('#docEmail').val(),
            viber: $('#docViber').val(),
            facebook: $('#docFacebook').val()
        }
        docInfo.push(docDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        docInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        docInfo.push(retList);
        console.log(docInfo);
        var values = {
            accessToken: updateDoc.globalAccessToken,
            id         : updateDoc.globID,
            docInfo    : docInfo
        };
        var path = updateDoc.ip + '?r=update-doctor/make-update';
        var result = updateDoc.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                updateDoc.removeLoading();
                window.location.href = '../index.html';
            } else if(data.status === 0) {
                updateDoc.removeLoading();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    data.msgTitle,
                    'OK'
                );
            } else if(data.status === 1) {
                updateDoc.loadDoc();
                updateDoc.remove();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    'data.msgTitle',
                    'OK'
                );
                $('.title').text('Doctor Detail');
                updateDoc.noEdit();
            }
        }).catch(function(error) {
            console.error(error);
            updateDoc.removeLoading();
            navigator.notification.alert(
                    'Network error occured',
                    null,
                    'ERROR',
                    'OK'
                );
        });
        // sending data to server using ajax
        // var tokenInformations = updateDoc.SQLLObj.pullToken();
        // tokenInformations.done(function(informations) {
        //     var accessToken = '';
        //     for (var idx in informations) {
        //         var information = informations[idx];
        //         //alert(information.name);
        //         accessToken = information.token;
        //     }
        //     if (accessToken.trim() === '') {
        //         console.log('there is no accessToken');
        //         window.location.href = "file:///android_asset/www/index.html";
        //         return;
        //     }
        //     /* First uploading image to server using
        //     file transfer protocal*/
        //     var imageData          = $('#photo').attr('src');
        //     var options            = new FileUploadOptions();
        //     options.fileKey        = "file";
        //     options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1);
        //     options.mimeType       = "image/jpeg";
        //     console.log(options.fileName);
        //     // Creating new object to stor field data
        //     var params             = new Object();
        //     params.accessToken     = accessToken;
        //     params.docInfo         = docInfo;
        //     // stroing params object to options
        //     options.params         = params;
        //     options.chunkedMode    = false;

        //     var ft = new FileTransfer();
        //     ft.upload(imageData, updateDoc.ip + '?r=master-doctor-list/handler', function(result){
        //         console.log('data is: ',result);
        //         updateDoc.removeLoading();
        //         var response = $.parseJSON(result.response);
        //         if(response.status === 1) {
        //             navigator.notification.alert(
        //                         response.msgBody,
        //                         null,
        //                         response.msgTitle,
        //                         'OK'
        //                     );
        //         window.location.href = 'file:///android_asset/www/views/updateDoc.html';
        //         }
        //         if(response.status === 0) {
        //             navigator.notification.alert(
        //                         response.msgBody,
        //                         null,
        //                         response.msgTitle,
        //                         'OK'
        //                     );
        //         }
        //         if(response.status === 2) {
        //             navigator.notification.alert(
        //                 response.msgBody,
        //                 null,
        //                 response.msgTitle,
        //                 'OK'
        //             );
        //             window.location.href = "file:///android_asset/www/index.html";
        //         }
        //     }, function(error){
        //         console.error(error);
        //         updateDoc.removeLoading();
        //         navigator.notification.alert(
        //                 'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
        //                 null,
        //                 'ERROR',
        //                 'OK'
        //             );
        //     }, options);
        // }).fail(function(error) {
        //     console.error(error);
        //     updateDoc.removeLoading();
        //     navigator.notification.alert(
        //                             'Error occured',
        //                             null,
        //                             'ERROR',
        //                             'OK'
        //                         );
        // });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    noEdit: function() {
        $('#docName').attr('disabled', true);
        $('#docNumber').attr('disabled', true);
        $('#postalCode').attr('disabled', true);
        $('#docAddress').attr('disabled', true);
        $('#docWorkingClinic').attr('disabled', true);
        $('#docEmail').attr('disabled', true);
        $('#docViber').attr('disabled', true);
        $('#docFacebook').attr('disabled', true);
        $('.send-Master-Doctor').attr('disabled', true);
    },
    edit: function() {
        updateDoc.editIndex = 1;
        $('#docName').attr('disabled', false);
        $('#docNumber').attr('disabled', false);
        $('#postalCode').attr('disabled', false);
        $('#docAddress').attr('disabled', false);
        $('#docWorkingClinic').attr('disabled', false);
        $('#docEmail').attr('disabled', false);
        $('#docViber').attr('disabled', false);
        $('#docFacebook').attr('disabled', false);
        $('.send-Master-Doctor').attr('disabled', false);
        $.each($('.distData'), function() {
            $(this).find('#distNameSearch').attr('disabled', false);
        });
        $.each($('.retData'), function() {
            $(this).find('#retNameSearch').attr('disabled', false);
        });
        $('.div-image-updater').load('./imageUpdater.html', function() {
            $('.div-prev-img').hide();
            $(document).on('click', '.div-def-img', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1',function() {
            updateDoc.camUpdate.openDefaultCamera();
        });
        $(document).on('click', '.div-img-change-opt-2', function() {
            updateDoc.camUpdate.openFilePicker();
        });
        $(document).on('click', '.div-change-image-container', function() {
            $('.div-change-image-container').css({left: '-100%'});
        });
        $(document).on('click', '.btn-done-upload', function() {
            updateDoc.addLoading();
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('.img-prev').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to store field data
            var params         = new Object();
            params.accessToken = updateDoc.globalAccessToken;
            params.id          = updateDoc.globID;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;
            // Creating object for file transfer class
            var ft = new FileTransfer();
            ft.upload(imageData, updateDoc.ip + '?r=update-doctor/photo-update', function(result){
              console.log('data is: ',result);
              updateDoc.removeLoading();
              var response = $.parseJSON(result.response);
              if(response.status === 1) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      $('#photo').attr('src',$('.img-prev').attr('src'));
                      $('.img-prev').hide();
              }
              if(response.status === 0) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                    $('.img-prev').hide();
              }
              if(response.status === 2) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      window.location.href = "../index.html";
              }
            }, function(error){
              console.error(error);
              updateDoc.removeLoading();
              $('.img-prev').hide();
              navigator.notification.alert(
                'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                null,
                'ERROR',
                'OK'
              );
            }, options);
        });
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
        });
    }
}
updateDoc.initialize();