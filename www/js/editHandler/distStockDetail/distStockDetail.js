var distStockDetail = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        distStockDetail.ip      = new ip();
        distStockDetail.ip      = distStockDetail.ip.remoteIpAddress();
        distStockDetail.SQLLObj = new SQLiteLogin();
        distStockDetail.ctr     = new controller();

        var passedData          = window.localStorage.getItem('dataToPass');
        var dataPassed          = JSON.parse(passedData);
        $('#photo').attr('src',dataPassed.image_url);
        $('.distName').text(dataPassed.distName);
        $('.evalDate').text(dataPassed.evaluation_date);
        distStockDetail.loadData(dataPassed);
    },
    loadData: function(dataPassed) {
        var tokenInformations = distStockDetail.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              distStockDetail.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }
          // console.log(accessToken);
          var values = {
              accessToken: accessToken,
              stockId: dataPassed.stockId
          };
          var path = distStockDetail.ip + '?r=display-dist-stock/stock-product';
          var result = distStockDetail.ajaxCall(path, values);
            result.then(function(data) {
                // console.log(data);
                if (data === 2) {
                    window.location.href = "../index.html";
                }
                var temp =     '<table class="table">'+
                                    '<thead>'+
                                        '<tr>'+
                                            '<th>Product Name</th>'+
                                            '<th>Unit</th>'+
                                            '<th>Qty</th>'+
                                            '<th>Batch</th>'+
                                            '<th>Amount</th>'+
                                        '</tr>'+
                                    '</thead>'+
                                    '<tbody>';
                // Body part of table that hold prod data
                for(var i = 0; i<data.length; i++) {
                    temp += '<tr>'+
                              '<td>'+data[i].prod_name+'</td>'+
                              '<td>'+data[i].unit+'</td>'+
                              '<td>'+data[i].qty+'</td>'+
                              '<td>' + data[i].batch + '</td>' +
                              '<td>' + data[i].amount + '</td>' +
                          '</tr>';
                }
                temp +=     '</tbody>'+
                        '</table>';
                $('.div-stock-list').empty();
                $('.div-stock-list').append(temp);
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  distStockDetail.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    }
}
distStockDetail.initialize();