var updateDist = {
    SQLLObj: null,
    ipObj  : null,
    ip     : null,
    imgPath: null,
    ctr    : null,
    locSet : null,
    cam    : null,
    globID : null,
    editIndex: 0,
    globalAccessToken: null,
    camUpdate: null,
    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateDist.ipObj      = new ip();
        updateDist.ip         = updateDist.ipObj.remoteIpAddress();
        updateDist.imgPath    = updateDist.ipObj.remoteImgIp();
        updateDist.ctr        = new controller();
        updateDist.SQLLObj    = new SQLiteLogin();
        updateDist.locSet     = new locationSet();
        updateDist.cam        = new camera();
        updateDist.camUpdate  = new cameraForUpdate();
        $('#google-map').hide();
        var passedData = window.localStorage.getItem('dataToPass');
        var dataPassed = JSON.parse(passedData);
        updateDist.globID = dataPassed.distId;
        updateDist.loadDist(updateDist.globID);
        updateDist.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#mapBtn', function() {
            if(!(updateDist.editIndex === 0)) {
                updateDist.loadMpa();
            }
        });
        $(document).on('click', '#gpsBtn', function() {
            if(!(updateDist.editIndex === 0)) {
                updateDist.setLocation();
            }
        });
        $(document).on('click', '#btn-edit', updateDist.edit);
        $(document).on('click', '#btnDistInfoInsert', updateDist.check);
    },
    // method that show loading module
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    // method that remove loading module
    removeLoading: function() {
        $('.loading').remove();
    },
    setLocation: function() {
        updateDist.locSet.setPosition();
    },
    loadMpa: function() {
        $('#google-map').show();
    },
    loadDist: function(distId) {
        updateDist.addLoading();
        var tokenInformations = updateDist.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
                updateDist.globalAccessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken: accessToken,
                id         : distId
            };
            var path = updateDist.ip + '?r=dist-update/distributor-info';
            var result = updateDist.ajaxCall(path, values);

            result.then(function(data) {
                updateDist.removeLoading();
                // console.log(data);
                if (data.status === 2) {
                    window.location.href = "../index.html";
                }
                $('#photo').attr('src', updateDist.imgPath+data.image_url);
                $('#distName').val(data.dist_name);
                $('#distOwner').val(data.dist_owner);
                $('#latOfLoc').attr('lat',data.latitude);
                $('#lngOfLoc').attr('lng',data.longitude);
                $('#latOfLoc').text(data.latitude);
                $('#lngOfLoc').text(data.longitude);
                $('#postalCode').val(data.dist_postalcode);
                $('#distCountry').val(data.dist_country);
                $('#distCity').val(data.dist_city);
                $('#distPhone').val(data.dist_phone);
                $('#distMobile').val(data.dist_mobile);
                $('#distEmail').val(data.dist_email);
                $('#distFacebook').val(data.dist_facebook);
                $('#distViber').val(data.dist_viber);
                $('#distCreditLimit').val(data.credit_limit);
            }).catch(function(error) {
                updateDist.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    updateDist.loadDist,
                    'Error',
                    'TRY AGAIN'
                );
            });
            updateDist.noEdit();
        }).fail(function(error) {
            updateDist.removeLoading();
            console.error(error);
        });
    },
    noEdit: function() {
        $('#distName').attr('disabled', true);
        $('#distOwner').attr('disabled', true);
        $('#postalCode').attr('disabled', true);
        $('#distCountry').attr('disabled', true);
        $('#distCity').attr('disabled', true);
        $('#distPhone').attr('disabled', true);
        $('#distMobile').attr('disabled', true);
        $('#distEmail').attr('disabled', true);
        $('#distFacebook').attr('disabled', true);
        $('#distViber').attr('disabled', true);
        $('#distCreditLimit').attr('disabled', true);
        $('#btnDistInfoInsert').attr('disabled', true);
    },
    edit: function() {
        updateDist.editIndex = 1;
        $('#distName').attr('disabled', false);
        $('#distOwner').attr('disabled', false);
        $('#postalCode').attr('disabled', false);
        $('#distCountry').attr('disabled', false);
        $('#distCity').attr('disabled', false);
        $('#distPhone').attr('disabled', false);
        $('#distMobile').attr('disabled', false);
        $('#distEmail').attr('disabled', false);
        $('#distFacebook').attr('disabled', false);
        $('#distViber').attr('disabled', false);
        $('#distCreditLimit').attr('disabled', false);
        $('#btnDistInfoInsert').attr('disabled', false);
        $('.title').text('Edit Distributor');
        $('.div-image-updater').load('./imageUpdater.html', function() {
            $('.div-prev-img').hide();
            $(document).on('click', '.div-def-img', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1',function() {
            updateDist.camUpdate.openDefaultCamera();
        });
        $(document).on('click', '.div-img-change-opt-2', function() {
            updateDist.camUpdate.openFilePicker();
        });
        $(document).on('click', '.div-change-image-container', function() {
            $('.div-change-image-container').css({left: '-100%'});
        });
        $(document).on('click', '.btn-done-upload', function() {
            updateDist.addLoading();
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('.img-prev').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to store field data
            var params         = new Object();
            params.accessToken = updateDist.globalAccessToken;
            params.id          = updateDist.globID;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;
            // Creating object for file transfer class
            var ft = new FileTransfer();
            ft.upload(imageData, updateDist.ip + '?r=dist-update/photo-update', function(result){
              console.log('data is: ',result);
              updateDist.removeLoading();
              var response = $.parseJSON(result.response);
              if(response.status === 1) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      $('#photo').attr('src',$('.img-prev').attr('src'));
                      $('.img-prev').hide();
              }
              if(response.status === 0) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                    $('.img-prev').hide();
              }
              if(response.status === 2) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      window.location.href = "../index.html";
              }
            }, function(error){
              console.error(error);
              updateDist.removeLoading();
              $('.img-prev').hide();
              navigator.notification.alert(
                'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                null,
                'ERROR',
                'OK'
              );
            }, options);
        });
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
        });
    },
    check: function() {
        //  geting data from page
    var distName        = $('#distName').val();
    var distOwner       = $('#distOwner').val();
    var distPostalCode  = $('#postalCode').val();
    var distLat         = $('#latOfLoc').attr('lat');
    var distLng         = $('#lngOfLoc').attr('lng');
    var distCountry     = $('#distCountry').val();
    var distCity        = $('#distCity').val();
    var distPhone       = $('#distPhone').val();
    var distMobile      = $('#distMobile').val();
    var distEmail       = $('#distEmail').val();
    var distFacebook    = $('#distFacebook').val();
    var distViber       = $('#distViber').val();
    var distCreditLimit = $('#distCreditLimit').val();


    if (distName.trim() === '') {
      navigator.notification.alert(
                    'please enter business name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distOwner.trim() === '') {
      navigator.notification.alert(
                    'please enter business owner name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distPostalCode.trim() === '') {
      navigator.notification.alert(
                    'postal code field can not be empty.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if(distLat.trim() === '' || distLng.trim() === '') {
      navigator.notification.alert(
                    'please select distributor location',
                    null,
                    'Error',
                    'OK'
                );
                return;
    }
    if (distCountry.trim() === '') {
      navigator.notification.alert(
                    'please enter business country name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distCity.trim() === '') {
      navigator.notification.alert(
                    'please select city name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distMobile.trim() === '') {
      navigator.notification.alert(
                    'please enter distributor mobile number.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    // checking if email address field is empty or not 
    if(!(distEmail.trim() === '')) {
      var checkEmail = updateDist.isEmail();
        if(checkEmail === 0) {
            navigator.notification.alert(
                            'please enter valid email.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
    }
    updateDist.updateToServer();
    },
    updateToServer: function() {
        updateDist.addLoading();
        var values = {
            accessToken     : updateDist.globalAccessToken,
            id              : updateDist.globID,
            dist_name       : $('#distName').val(),
            dist_owner      : $('#distOwner').val(),
            latitude        : $('#latOfLoc').attr('lat'),
            longitude       : $('#lngOfLoc').attr('lng'),
            dist_postalcode : $('#postalCode').val(),
            dist_country    : $('#distCountry').val(),
            dist_city       : $('#distCity').val(),
            dist_phone      : $('#distPhone').val(),
            dist_mobile     : $('#distMobile').val(),
            dist_email      : $('#distEmail').val(),
            dist_facebook   : $('#distFacebook').val(),
            dist_viber      : $('#distViber').val(),
            credit_limit    : $('#distCreditLimit').val()
        };
        console.log(values);
        var path = updateDist.ip + '?r=dist-update/make-update';
        var result = updateDist.ajaxCall(path, values);
        result.then(function(data) {
            updateDist.removeLoading();
            // console.log(data);
            if (data.status === 2) {
                window.location.href = "../index.html";
            }
            if(data.status === 0) {
                navigator.notification.alert(
                    data.msg,
                    null,
                    data.title,
                    'OK'
                );
            }
            updateDist.editIndex = 0;
            $('#photo').attr('src', updateDist.imgPath+data.image_url);
            $('#distName').val(data.dist_name);
            $('#distOwner').val(data.dist_owner);
            $('#latOfLoc').attr('lat',data.latitude);
            $('#lngOfLoc').attr('lng',data.longitude);
            $('#latOfLoc').text(data.latitude);
            $('#lngOfLoc').text(data.longitude);
            $('#postalCode').val(data.dist_postalcode);
            $('#distCountry').val(data.dist_country);
            $('#distCity').val(data.dist_city);
            $('#distPhone').val(data.dist_phone);
            $('#distMobile').val(data.dist_mobile);
            $('#distEmail').val(data.dist_email);
            $('#distFacebook').val(data.dist_facebook);
            $('#distViber').val(data.dist_viber);
            $('#distCreditLimit').val(data.credit_limit);
            $('.title').text('Distributor Detail');
        }).catch(function(error) {
            updateDist.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                null,
                'Error',
                'OK'
            );
        });
        updateDist.noEdit();
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    isEmail: function() {
    var email = $('#distEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  }
}
updateDist.initialize();