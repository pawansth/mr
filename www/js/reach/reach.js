var reach = {
    SQLLObj		: null,
    parentTr	: null,
    ipObj		: null,
    ip          : null,
    imageIp     : null,
    ctr			: null,
	distData	: null,
    retData		: null,
	tecData		: null,
	farData		: null,
	cattleData	: null,
	poultryData	: null,
	docData		: null,
    position    : null,
    SQLROObj    : null,
    bckBtn: 0,
    bckHou: null,
    bckMin: null,
    bckSec: null,
    initialize: function() {
        document.addEventListener('deviceready', function () {
            // alert('hi anil');
            // window.codePush.sync(null,{installMode: InstallMode.IMMEDIATE},null);
            screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
            reach.ipObj    = new ip();
            reach.ip       = reach.ipObj.remoteIpAddress();
            reach.imageIp  = reach.ipObj.remoteImgIp();
            reach.SQLLObj  = new SQLiteLogin();
            reach.SQLROObj = new SQLiteReachOrigin();
            reach.ctr      = new controller();
            reach.addLoading();
            reach.todayToDo();
            document.addEventListener("backbutton", function (e) {
                e.preventDefault();
                if(reach.bckBtn === 1) {
                    var date = new Date();
                    var hou = date.getHours();
                    var min = date.getMinutes();
                    var sec = date.getSeconds();
                    if(hou === reach.bckHou && min === reach.bckMin && ((sec-reach.bckSec)<2)) {
                        navigator.app.exitApp();
                    } else {
                        reach.bckHou = 0;
                        reach.bckMin = 0;
                        reach.bckSec = 0;
                        reach.bckBtn = 0;
                    }
                } else {
                    var date = new Date();
                    reach.bckHou = date.getHours();
                    reach.bckMin = date.getMinutes();
                    reach.bckSec = date.getSeconds();
                    reach.bckBtn = 1;
                }
            }, false );
            reach.bindEvents();
        }, false);
    },
    bindEvents: function() {
        $(document).on('change', '#locationType', reach.switcher);
        $(document).on('click', '.btn-reached', reach.check);
        $(document).on('input', '#selectName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('select-id',$(this).attr('chose_id'));
             }
           });
        });
        $(document).on('click', '.phone', function () {
            window.open('tel:' + $(this).text(), '_system')
        });
    },
    switcher: function() {
        var currType = $('#locationType').val();
        $('#selectName').val('');
        $('#selectName').attr('select-id','');
        switch (currType) {
                case '1':
                if(reach.distData === null) {
                    reach.addLoading();
                    reach.loadDist();
                }
                    reach.addDist();
                    break;
                case '2':
                if(reach.retData === null) {
                    reach.addLoading();
                    reach.loadRet();
                }
                    reach.addRet();
                    break;
                case '3':
                if(reach.farData === null) {
                    reach.addLoading();
                    reach.loadFar();
                }
                    reach.addFar();
                    break;
                case '4':
                if(reach.cattleData === null) {
                    reach.addLoading();
                    reach.loadCattle();
                }
                    reach.addCattle();
                    break;
                case '5':
                if(reach.poultryData === null) {
                    reach.addLoading();
                    reach.loadPoultry();
                }
                    reach.addPoultry();
                    break;
                case '6':
                if(reach.tecData === null) {
                    reach.addLoading();
                    reach.loadTec();
                }
                    reach.addTec();
                    break;
                case '7':
                if(reach.docData === null) {
                    reach.addLoading();
                    reach.loadDoc();
                }
                    reach.addDoc();
                    break;
            default:
                console.log('non of above option is matched');
            }
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    // code to show today todo
    todayToDo: function() {
        var tokenInformations = reach.SQLLObj.pullToken();
        tokenInformations.done(function (informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
                reach.globalAccessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            var values = {
                accessToken: accessToken
            };
            $.ajax({
                url: reach.ip + '?r=current-date-planning/today-planning',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function (jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function (data) {
                    if (data === 2) {
                        window.location.href = "../index.html";
                    }
                    // Showing distributors
                    if(data[0].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                                        '<div class="panel-heading">Distributor</div>' +
                                        '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for(var i = 0; i < data[0].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                        '<div style="float:left; padding-right:30px; width:30%;">'+
                                        '<img class="lazy" data-original="' + reach.imageIp + data[0][i].image_url +'" style="width: 32px; height: 32px;">'+
                                        '</div>' +
                                        '<div style="float:left; width:70%;">'+
                                            '<span>'+data[0][i].dist_name+'</span><br/>'+
                                            '<span>'+data[0][i].dist_city+'</span><br/>'+
                                            '<span class="phone" style="color:blue;">'+data[0][i].dist_mobile+'</span>'+
                                        '</div>'+
                                    '</div>'; 
                        }
                        temp +=     '</div>' +
                                '</div>';
                        $('.today-list-container').append(temp);
                    }
                    // Showing retailers
                    if (data[1].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                            '<div class="panel-heading">Retailers</div>' +
                            '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for (var i = 0; i < data[1].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                '<div style="float:left; padding-right:30px; width:30%;">' +
                                '<img id="test" class="lazy" data-original="' + reach.imageIp + data[1][i].image_url + '" style="width: 32px; height: 32px;">' +
                                '</div>' +
                                '<div style="float:left; width:70%;">' +
                                '<span>' + data[1][i].retail_name + '</span><br/>' +
                                '<span>' + data[1][i].retail_city + '</span><br/>' +
                                '<span class="phone" style="color:blue;">' + data[1][i].retail_mobile + '</span>' +
                                '</div>' +
                                '</div>';
                        }
                        temp += '</div>' +
                            '</div>';
                        $('.today-list-container').append(temp);
                    }
                    // Showing farmer
                    if (data[2].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                            '<div class="panel-heading">Farmer</div>' +
                            '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for (var i = 0; i < data[2].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                '<div style="float:left; padding-right:30px; width:30%;">' +
                                '<img class="lazy" data-original="' + reach.imageIp + data[2][i].image_url + '" style="width: 32px; height: 32px;">' +
                                '</div>' +
                                '<div style="float:left; width:70%;">' +
                                '<span>' + data[2][i].name + '</span><br/>' +
                                '<span>' + data[2][i].address + '</span><br/>' +
                                '<span class="phone" style="color:blue;">' + data[2][i].phone_no + '</span>' +
                                '</div>' +
                                '</div>';
                        }
                        temp += '</div>' +
                            '</div>';
                        $('.today-list-container').append(temp);
                    }
                    // Showing cattle farmer
                    if (data[3].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                            '<div class="panel-heading">Cattle Farmer</div>' +
                            '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for (var i = 0; i < data[3].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                '<div style="float:left; padding-right:30px; width:30%;">' +
                                '<img class="lazy" data-original="' + reach.imageIp + data[3][i].image_url + '" style="width: 32px; height: 32px;">' +
                                '</div>' +
                                '<div style="float:left; width:70%;">' +
                                '<span>' + data[3][i].name + '</span><br/>' +
                                '<span>' + data[3][i].address + '</span><br/>' +
                                '<span class="phone" style="color:blue;">' + data[3][i].number + '</span>' +
                                '</div>' +
                                '</div>';
                        }
                        temp += '</div>' +
                            '</div>';
                        $('.today-list-container').append(temp);
                    }
                    // Showing poultry farmer
                    if (data[4].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                            '<div class="panel-heading">Poultry Farmer</div>' +
                            '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for (var i = 0; i < data[4].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                '<div style="float:left; padding-right:30px; width:30%;">' +
                                '<img class="lazy" data-original="' + reach.imageIp + data[4][i].image_url + '" style="width: 32px; height: 32px;">' +
                                '</div>' +
                                '<div style="float:left; width:70%;">' +
                                '<span>' + data[4][i].name + '</span><br/>' +
                                '<span>' + data[4][i].address + '</span><br/>' +
                                '<span class="phone" style="color:blue;">' + data[4][i].number + '</span>' +
                                '</div>' +
                                '</div>';
                        }
                        temp += '</div>' +
                            '</div>';
                        $('.today-list-container').append(temp);
                    }
                    // Showing technican
                    if (data[5].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                            '<div class="panel-heading">Technician</div>' +
                            '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for (var i = 0; i < data[5].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                '<div style="float:left; padding-right:30px; width:30%;">' +
                                '<img class="lazy" data-original="' + reach.imageIp + data[5][i].image_url + '" style="width: 32px; height: 32px;">' +
                                '</div>' +
                                '<div style="float:left; width:70%;">' +
                                '<span>' + data[5][i].name + '</span><br/>' +
                                '<span>' + data[5][i].address + '</span><br/>' +
                                '<span class="phone" style="color:blue;">' + data[5][i].number + '</span>' +
                                '</div>' +
                                '</div>';
                        }
                        temp += '</div>' +
                            '</div>';
                        $('.today-list-container').append(temp);
                    }
                    // Showing doctor
                    if (data[6].length != 0) {
                        var temp = '<div class="panel panel-primary">' +
                            '<div class="panel-heading">Doctor</div>' +
                            '<div class="panel-body" style="padding: 0px !important; margin: 0px !important;">';
                        for (var i = 0; i < data[6].length; i++) {
                            temp += '<div style="padding: 5px 10px; border-bottom: 1px solid whitesmoke; min-height:60px;">' +
                                '<div style="float:left; padding-right:30px; width:30%;">' +
                                '<img class="lazy" data-original="' + reach.imageIp + data[6][i].image_url + '" style="width: 32px; height: 32px;">' +
                                '</div>' +
                                '<div style="float:left; width:70%;">' +
                                '<span>' + data[6][i].name + '</span><br/>' +
                                '<span>' + data[6][i].address + '</span><br/>' +
                                '<span class="phone" style="color:blue;">' + data[6][i].number + '</span>' +
                                '</div>' +
                                '</div>';
                        }
                        temp += '</div>' +
                            '</div>';
                        $('.today-list-container').append(temp);
                    }
                    $("img.lazy").lazyload({
                        threshold: 200
                    });
                    reach.loadDist();
                }
            }).fail(function (error) {
                console.error(error);
                navigator.notification.alert(
                    ' There is error in network',
                    reach.todayToDo,
                    'Error',
                    'TRY AGAIN'
                );
            });

        }).fail(function (error) {
            console.error(error);
        });
    },
    //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.distData = data;
                  reach.addDist();
                  reach.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadDist,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.retData = data;
                  reach.addRet();
                  reach.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadRet,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all technican data related to mr from server
    loadTec: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=planning/tec-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.tecData = data;
                  reach.addTec();
                  reach.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadTec,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all farmer list data related to mr from server
    loadFar: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=planning/far-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.farData = data;
                  reach.addFar();
                  reach.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadFar,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all cattle farmer list data related to mr from server
    loadCattle: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=planning/cattle-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.cattleData = data;
                  reach.addCattle();
                  reach.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadCattle,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all poultry farmer list data related to mr from server
    loadPoultry: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=planning/poultry-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.poultryData = data;
                  reach.addPoultry();
                  reach.removeLoading();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadPoultry,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all doctor list data related to mr from server
    loadDoc: function() {
      var tokenInformations = reach.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: reach.ip + '?r=planning/doc-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  reach.docData = data;
                  reach.addDoc();
                  reach.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  reach.loadDoc,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        reach.remove();
        var templeteOption = '';
                  
        $.each(reach.distData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    remove: function() {
        $("#nameList option").remove();;
    },
    addRet: function() {
        reach.remove();
        var templeteOption = '';
                  
        $.each(reach.retData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    addTec: function() {
        reach.remove();
        var templeteOption = '';
                  
        $.each(reach.tecData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    addFar: function() {
        reach.remove();
        var templeteOption = '';
                  
        $.each(reach.farData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    addCattle: function() {
        reach.remove();
        var templeteOption = '';
                  
        $.each(reach.cattleData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    addPoultry: function() {
        reach.remove();
        var templeteOption = '';
                  
        $.each(reach.poultryData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    addDoc: function() {
        reach.remove();
        var templeteOption = '';
        $.each(reach.docData, function() {
            templeteOption += '<option chose_id="'+this.id+'" value="'+this.name+'">';
        });
        $('#nameList').append(templeteOption);
    },
    getCurrentPosition: function() {
        cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                console.log("Location is " + (enabled ? "enabled" : "disabled"));
                if (enabled) {
                    // SpinnerDialog.show("Featching Location", "Feaching your current location....", true);
                    navigator.geolocation.getCurrentPosition(function(position) {
                        reach.position = position;
                        reach.sendToServer();
                    }, function() {
                        navigator.notification.alert(
                        'unable to featch your location',
                        null,
                        'Error',
                        'OK'
                 );
                    }, {
                        maximumAge: 60000,
                        timeout: 15000,
                        enableHighAccuracy: true
                    });
                } else {
                    cordova.dialogGPS("Your GPS is Disabled, this app needs to be enable to works.", //message
                                        "Use GPS, with wifi or 3G.", //description
                                        function(buttonIndex) { //callback
                                                        switch (buttonIndex) {
                                                                                case 0:
                                                                                    break; //cancel
                                                                                case 1:
                                                                                    break; //neutro option
                                                                                case 2:
                                                                                    break; //user go to configuration
                                                        }
                                        },
                                        "Please Turn on GPS", //title
                                        ["Cancel", "Go"] //buttons
                            ); 
                }
            }, function(error) {
                console.error("The following error occurred: " + error);
            });
    },
    check: function(e) {
        e.preventDefault();
        if(!($('#btn-start').hasClass('active'))) {
            navigator.notification.alert(
                'First start work for current day by clicking start button.',
                        null,
                        'Warrning',
                        'OK'
            );
            return;
        }
        reach.getCurrentPosition();
    },
    sendToServer: function() {
        reach.addLoading();
        var tokenInformations = reach.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }
            //  Origin data from database
            // reach.SQLROObj.setOrigin('27.694884','84.4241729');
            var dbOrigin = [];
            var origin = reach.SQLROObj.getOrigin();
            origin.done(function(informations) {
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    var originLoc = {
                        latitude : information.latitude,
                        longitude: information.longitude
                    };
                    dbOrigin.push(originLoc);
                }
                var time = reach.position.timestamp;
                var destinyLoc = {
                    latitude: reach.position.coords.latitude,
                    longitude: reach.position.coords.longitude
                };
                // alert(new Date(time));
                var values = {
                accessToken: accessToken,
                time: time,
                originLoc: dbOrigin,
                destinyLoc: destinyLoc,
                locationType: $('#locationType').val(),
                selct_id: $('#selectName').attr('select-id')
                };
            console.log('values: ',values);
            $.ajax({
                url: reach.ip + '?r=reach/handler',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    // console.log(data);
                    reach.removeLoading();
                    if (data === 2) {
                        window.location.href = "../index.html";
                    } else {
                        $('.messageHolder').remove();
                        var temp = '<div class="messageHolder">'+
                                    '<div class="msgHolder">'+
                                    '<h4 class="title"></h4>'+
                                    '<p class="msg"></p>'+
                                    '</div>'+
                                    '</div>';
                        $(temp).insertAfter('#disInfoContainer');
                        $('.msgHolder').css('padding','10px');
                        $('.messageHolder').css('margin-top','10px');
                        $('.messageHolder').css('margin-bottom','10px');
                    }
                    if(data.status === 1) {
                        $('.title').text(data.msgTitle);
                        $('.msg').text(data.msgBody);
                        $('.messageHolder').css('background','#E0F2F1');
                        reach.SQLROObj.updateOrigin(reach.position.coords.latitude, reach.position.coords.longitude);
                    } else if(data.status === 0) {
                        $('.title').text(data.msgTitle);
                        $('.msg').text(data.msgBody);
                        $('.messageHolder').css('background','#FFEBEE');
                    } 
                }
            }).fail(function(error) {
                reach.removeLoading();
                console.error(error);
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });
        }).fail(function(error) {
            reach.removeLoading();
            console.error(error);
        });

    }).fail(function(error) {
        reach.removeLoading();
        console.error(error);
        });
    }
}

reach.initialize();