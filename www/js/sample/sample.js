var sample = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    prodData: null,

    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        sample.ip      = new ip();
        sample.ip      = sample.ip.remoteIpAddress();
        sample.SQLLObj = new SQLiteLogin();
        sample.ctr     = new controller();

        sample.bindEvents();
        sample.addLoading();
        sample.loadDist();
        sample.loadProd();
        // $(document).on('click', '.test1', function() {
        //     $('#unitList option').remove();
        // });
    },
    bindEvents: function() {
        $(document).on('change', '#locationType', function() {
            sample.addLoading();
            sample.remove();
            sample.switcher();
            sample.removeLoading();
            $('#clientNameSearch').val('');
            $('#clientNameSearch').attr('clientId','');
        });
        $(document).on('click', '.addProd', sample.addProdTemplete);
        $(document).on('click', '.minusProd', sample.removeProdTemplete);
        $(document).on('click', '.btn-sample', sample.chick);
        $(document).on('focusout', '#clientNameSearch', function() {
            if($('#clientNameSearch').val().trim() === '') {
                $('#clientNameSearch').attr('clientId', '');
            }
        });
        $(document).on('focusout', '#productName', function() {
            if($('#productName').val().trim() === '') {
                $('#productName').attr('prodId', '');
                $('#productName').attr('baseUnitId', '');
                $('#productName').attr('price', '');
            }
        });
        $(document).on('focusout', '#productUnit', function() {
            if($('#productUnit').val().trim() === '') {
                $('#productUnit').attr('prodUnitId', '');
            }
        });
        $(document).on('input', '#clientNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('clientId',$(this).attr('client_id'));
             }
           });
        });
        $(document).on('input', '#productName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var parent = $(this).parent().parent();
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodId',$(this).attr('prod_id'));
               $(ob).attr('baseUnitId',$(this).attr('base_unit_id'));
               $(ob).attr('price',$(this).attr('base_price'));
               sample.addLoading();
               $('.loadingText').text('Loading units...');
               sample.searchCrosProdUnit(parent);
             }
           });
        });
        $(document).on('input', '#productUnit', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodUnitId',$(this).attr('unit_id'));
               return;
             }
           });
        });
    },
    switcher: function() {
        var currType = $('#locationType').val();
        $('#selectName').val('');
        $('#selectName').attr('select-id','');
        switch (currType) {
                case '1':
                    sample.loadDist();
                    break;
                case '2':
                    sample.loadRet();
                    break;
                case '3':
                    sample.loadFar();
                    break;
                case '4':
                    sample.loadCattle();
                    break;
                case '5':
                    sample.loadPoultry();
                    break;
                case '6':
                    sample.loadTec();
                    break;
                case '7':
                    sample.loadDoc();
                    break;
            default:
                console.log('non of above option is matched');
            }
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loadDist: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadDist,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadRet,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all technican data related to mr from server
    loadTec: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=planning/tec-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadTec,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all farmer list data related to mr from server
    loadFar: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=planning/far-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadFar,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all cattle farmer list data related to mr from server
    loadCattle: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=planning/cattle-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadCattle,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all poultry farmer list data related to mr from server
    loadPoultry: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=planning/poultry-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadPoultry,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all doctor list data related to mr from server
    loadDoc: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=planning/doc-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option client_id="'+this.id+'" value="'+this.name+'">';
                  });
                  // templete += '</ul>';
                  $('#clientList').append(templete);
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadDoc,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    remove: function() {
        $("#clientList option").remove();;
    },
    loadProd: function() {
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: sample.ip + '?r=order-handler/prod-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  sample.prodData = data;
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                  });
                  // templete += '</ul>';
                  $('#prodList').append(templete);
                  sample.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    /* searching corresponding product unit */
    searchCrosProdUnit: function(currentObj) {
      crossProductId = currentObj.find('#productName').attr('prodId');
    //   alert(crossProductId);
      //  Pulling access token form sqlite database
      var tokenInformations = sample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken,
              crossProductId: crossProductId
          };

          $.ajax({
              url: sample.ip + '?r=order-handler/search-cross-unit',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  console.log(data);
                  
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }

                  var templete = '';
                  
                  $.each(data, function() {
                      templete += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                  });
                  console.log(currentObj.find('#unitList'));
                  currentObj.find('#unitList').append(templete);
                  sample.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  sample.searchCrosProdUnit,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addProdTemplete: function() {
        $(this).parent().parent().css('margin-bottom','5px');
        var templete = '';
        templete += '<div class="planProduct">'+
                        '<div class="">'+
                            '<input type="text" class="form-control" id="productName" prodId="" baseUnitId="" price="" list="prodList" placeholder="product">'+
                            '<datalist id="prodList"></datalist>'+
                        '</div>'+
                        '<div class="row add-top-margin">'+
                            '<div class="col-xs-5 nopadding-right">'+
                                '<input type="text" class="form-control" id="productUnit" prodUnitId="" list="unitList" placeholder="unit">'+
                                '<datalist id="unitList"></datalist>'+
                            '</div>'+
                            '<div class="col-xs-4 nopadding-left">'+
                                '<input type="number" class="form-control" id="productQty" placeholder="qty">'+
                            '</div>'+
                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                            '<button type="button" class="btn btn-warning floating-minus minusProd glyphicon glyphicon-remove col-xs-2"></button>'+
                        '</div>'+
                    '</div>';
        $('.planProductList').append(templete);
    },
    removeProdTemplete: function() {
        $(this).parent().parent().remove();
    },
    chick: function() {
        if($('#clientNameSearch').val().trim() === '') {
            navigator.notification.alert(
                'Client name can not be empty',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        if($('#clientNameSearch').attr('clientId').trim() === '') {
            navigator.notification.alert(
                'Client name is not properly slected form list.'+
                ' Please select client name form list.',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        var count = 1;
        var eachStatus = false;
        $.each($('.planProduct'), function() {
            if($(this).find('#productName').val().trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productName').attr('prodId').trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' should be select form list of give product.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').val().trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').attr('prodUnitId').trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' should be select form list of given unit list.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productQty').val().trim() === '') {
                navigator.notification.alert(
                    'Product quantity in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            count = count + 1;
        });
        if(eachStatus === true) {
            eachStatus = false;
            return;
        }
        sample.addLoading();
        $('.loadingText').text('Submiting data')
        sample.submitSample();
    },
    submitSample: function() {
        var productValues = [];
        var clientType  = $('#locationType').val();
        var clientId    = $('#clientNameSearch').attr('clientId');
        var description = $('#desc').val();

        var clientInfo = {
            clientType : clientType,
            clientId   : clientId,
            description: description
        };
        var productId    = '';
        var productQty   = '';
        var productUnit  = '';

        $.each($('.planProduct'), function() {
            productId     = $(this).find('#productName').attr('prodId');
            productQty    = $(this).find('#productQty').val();
            productUnit   = $(this).find('#productUnit').attr('prodUnitId');
            var data = {
                productId    : productId,
                productQty   : productQty,
                productUnit  : productUnit
            };
            // pushing json formate data to array
            productValues.push(data);
        });
        console.log(productValues);
        // sending data to server using ajax
        var tokenInformations = sample.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken  : accessToken,
                clientInfo   : clientInfo,
                productValues: productValues
            };

            $.ajax({
                url: sample.ip + '?r=sample-handler/handler',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    sample.removeLoading();
                    // navigator.notification.alert(
                    //     'Network eror',
                    //     null,
                    //     'ERROR',
                    //     'OK'
                    // );
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    // console.log(data);
                    if (data === 1) {
                        sample.removeLoading();
                        navigator.notification.alert(
                            'Successfull',
                            null,
                            'Success',
                            'OK'
                        );
                        window.location.href = "./sample.html";
                    } else if (data === 0) {
                        sample.removeLoading();
                        navigator.notification.alert(
                            'Operation faild',
                            null,
                            'Error',
                            'OK'
                        );
                    } else if (data === 2) {
                        window.location.href = "../index.html";
                    }

                }
            }).fail(function(error) {
                console.error(error);
                sample.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });

        }).fail(function(error) {
            sample.removeLoading();
            navigator.notification.alert(
                'Error occured.',
                null,
                'ERROR',
                'OK'
            );
            console.error(error);
        });
    }
}

sample.initialize();