var SQLiteLocBtnFlag = function() {
  // getting sqlite connection
  var db = new SQLiteConnection();
  var service = {};

  service.pullLocBtnFlag = function() {
      var deferred = $.Deferred();
      db.transaction(function(tx) {
          tx.executeSql('SELECT * FROM loc_btn_flag', [], function(tx, res) {
              var informations = [];
              console.log(res.rows.length);
              for (var i = 0; i < res.rows.length; i++) {
                  var information = {
                      start_btn_flag: res.rows.item(i).start_btn_flag,
                      stop_btn_flag: res.rows.item(i).stop_btn_flag
                  };
                  //alert('name'+res.rows.item(i).name);
                  informations.push(information);
              }
              deferred.resolve(informations);

          }, function(e) {
              deferred.reject(e);
          });
      });
      return deferred.promise();
  },

  service.updateLocBtnFlag = function(startFlag, stopFlag) {
    // console.log('updateLocBtnFlag is called');
    var deferred = $.Deferred();
    // console.log(db);
    db.transaction(function(tx) {
      var query = 'UPDATE loc_btn_flag SET start_btn_flag = ?, stop_btn_flag = ? where id = ?';
        tx.executeSql(query, [startFlag, stopFlag,1], function(tx, res) {
          console.log('updated sucessfully');
            deferred.resolve();
        }, function(e) {
            deferred.reject(e);
        });
    });
    return deferred.promise();
  }

  // service.delete = function() {
  //   console.log('inside delte function');
  //   db.transaction(function(tx) {
  //       tx.executeSql('DELETE FROM job_start_info', [], function(tx, res) {
  //         console.log('delete sucessfully');
  //           deferred.resolve();
  //       }, function(e) {
  //           deferred.reject(e);
  //       });
  //   });
  //   return deferred.promise();
  // }

return service;
}
