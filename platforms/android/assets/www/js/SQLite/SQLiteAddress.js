var SQLiteAddress = function() {
    // getting sqlite connection
    var db = new SQLiteConnection();
    var service = {};

    service.zone = function() {
            //alert('inside pulldata function');
            db.transaction(function(tx) {
                tx.executeSql('SELECT * FROM lib_zone', [], function(tx, res) {
                    var option = '';
                    //alert('data length'+res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                            option += '<option zone_id="'+res.rows.item(i).id+'" value="'+res.rows.item(i).zoneName+'">'+res.rows.item(i).zoneName+'</option>';
                        //alert('name'+res.rows.item(i).name);
                    }
                    $('#zone').append(option);

                }, function(e) {
                    console.error('error:',e);
                });
            });
        },
        service.district = function() {
            //alert('inside pulldata function');
            db.transaction(function(tx) {
                tx.executeSql('SELECT * FROM district', [], function(tx, res) {
                    var option = '';
                    //alert('data length'+res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                            option += '<option zone_id="'+res.rows.item(i).zone_id+'" value="'+res.rows.item(i).District_name+'">'+res.rows.item(i).District_name+'</option>';
                        //alert('name'+res.rows.item(i).name);
                    }
                    $('#district').append(option);

                }, function(e) {
                    console.error('error:',e);
                });
            });
        },
        service.selectedDistrict = function(zoneId) {
            //alert('inside pulldata function');
            db.transaction(function(tx) {
                tx.executeSql('SELECT * FROM district where zone_id = ?', [zoneId], function(tx, res) {
                    var option = '';
                    //alert('data length'+res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                            option += '<option zone_id="'+res.rows.item(i).zone_id+'" value="'+res.rows.item(i).District_name+'">'+res.rows.item(i).District_name+'</option>';
                        //alert('name'+res.rows.item(i).name);
                    }
                    $('#district').empty();
                    $('#district').append(option);

                }, function(e) {
                    console.error('error:',e);
                });
            });
        }
        return service;
}