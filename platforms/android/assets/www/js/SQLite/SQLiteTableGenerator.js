var SQLiteTableGenerator = function() {
    var service = {};
var db = new SQLiteConnection();

    service.initialize = function() {
      // console.log('inside table generator');
        var deferred = $.Deferred();
        db.transaction(function(tx) {
                tx.executeSql(
                        'CREATE TABLE IF NOT EXISTS job_start_info ' +
                        '(id integer primary key AUTOINCREMENT, date_time BIGINT)', [],
                        function(tx, res) {

                            tx.executeSql(
                                    'CREATE TABLE IF NOT EXISTS loc_btn_flag ' +
                                    '(id integer primary key AUTOINCREMENT, start_btn_flag INT NOT NULL, stop_btn_flag INT NOT NULL)', [],
                                    function(tx, res) {

                                        tx.executeSql('SELECT * FROM loc_btn_flag', [], function(tx, res) {
                                                    if (res.rows.length < 1) {
                                                        tx.executeSql('INSERT INTO loc_btn_flag  (start_btn_flag, stop_btn_flag) VALUES (?,?)', [0, 1], function(tx, res) {
                                                            tx.executeSql('CREATE TABLE IF NOT EXISTS login_token'+
                                                            '(id integer primary key AUTOINCREMENT, token varchar(255) NOT NULL)',[],
                                                            function(tx,res){
                                                                //  Creating table for storing mr travling orgin location
                                                                tx.executeSql('CREATE TABLE IF NOT EXISTS mr_origin'+
                                                                '(id integer primary key AUTOINCREMENT, latitude varchar(100) NOT NULL,'+
                                                                'longitude varchar(100) NOT NULL)',[], function(tx, res) {
                                                                    // service.zone(tx);
                                                                    // service.district(tx);
                                                                    console.log('[database]: table genrated successfully');
                                                                    deferred.resolve(service);
                                                                },function(tx,res) {
                                                                    deferred.reject(e);
                                                                });
                                                            },function(tx,res){
                                                              deferred.reject(e);
                                                            });
                                                        }, function(e) {
                                                            deferred.reject(e);
                                                        });

                                                    }

                                            },
                                            function(e) {
                                                deferred.reject(e);
                                            });

                                },
                                function(tx, res) {
                                    deferred.reject('Error initializing database');
                                }
                        );

                    },
                    function(tx, res) {
                        deferred.reject('Error initializing database');
                    });
        });
    return deferred.promise();
}
// service.zone = function(tx) {
//     tx.executeSql('CREATE TABLE IF NOT EXISTS lib_zone ('+
//                       'id integer primary key AUTOINCREMENT,'+
//                       'zoneName varchar(50) NOT NULL,)',[], function(tx, res) {
//                           tx.executeSql("INSERT INTO lib_zone (id, zoneName) VALUES"+
//                                     "(1, 'Mechi'),"+
//                                     "(2, 'Koshi'),"+
//                                     "(3, 'Sagarmatha'),"+
//                                     "(4, 'Janakpur'),"+
//                                     "(5, 'Bagmati'),"+
//                                     "(6, 'Narayani'),"+
//                                     "(7, 'Gandaki'),"+
//                                     "(8, 'Lumbini'),"+
//                                     "(9, 'Daulagiri'),"+
//                                     "(10, 'Rapti'),"+
//                                     "(11, 'Karnali'),"+
//                                     "(12, 'Bheri'),"+
//                                     "(13, 'Seti'),"+
//                                     "(14, 'Mahakali')",[], function(tx, res) {
//                                         console.log(res);
//                                         return;
//                             },function(tx,res) {
//                                 console.error('error: ',res);
//                                 return;
//                             });
//                     },function(tx,res) {
//                         console.error('error: ',res);
//                         return;
//                     });
// },
// service.district = function(tx) {
//     tx.executeSql("CREATE TABLE IF NOT EXISTS district ("+
//                       "District_Id integer"+
//                       "District_name varchar(50) DEFAULT NULL,"+
//                       "District_nepname varchar(50) DEFAULT NULL,"+
//                       "zone_id integer"+
//                     ")",[], function(tx, res) {
//                           tx.executeSql("INSERT INTO district (District_Id, District_name, District_nepname, zone_id) VALUES"+
//                                 "(1, 'Taplejung', 'tfKn]h@@@@@Ë', 1),"+
//                                 "(2, 'Panchthar', 'kf_ry<', 1),"+
//                                 "(3, 'Ilam', 'O{nfd', 1),"+
//                                 "(4, 'Jhapa', 'emfkf', 1),"+
//                                 "(5, 'Sankhuwasabha', ';_v@@@@@jf;ef', 2),"+
//                                 "(6, 'Tehrathum', 't]x|y@@@@@d', 2),"+
//                                 "(7, 'Bhojpur', 'ef]hk@@@@@<', 2),"+
//                                 "(8, 'Dhankuta', 'wgs@@@@@^f', 2),"+
//                                 "(9, 'Morang', 'df]<Ë', 2),"+
//                                 "(10, 'Sunsari', ';@@@@@g;<L', 2),"+
//                                 "(11, 'Solukhumbu', ';f]n@@@@@v@@@@@Dj@@@@@', 3),"+
//                                 "(12, 'Khotang', 'vf]^fË', 3),"+
//                                 "(13, 'Okhaldhunga', 'cf]vn(@@@@@_uf', 3),"+
//                                 "(14, 'Udaypur', 'pbok@@@@@<', 3),"+
//                                 "(15, 'Saptari', ';Kt<L', 3),"+
//                                 "(16, 'Siraha', 'l;<fxf', 3),"+
//                                 "(17, 'Dolakha', 'bf]nvf', 4),"+
//                                 "(18, 'Ramechap', '<fd]%fk', 4),"+
//                                 "(19, 'Sindhuli', 'l;Gw@@@@@nL', 4),"+
//                                 "(20, 'Dhanusha', 'wg@@@@@iff', 4),"+
//                                 "(21, 'Mahottari', 'dxf]Q<L', 4),"+
//                                 "(22, 'Sarlahi', ';nf{xL', 4),"+
//                                 "(23, 'Rasuwa', '<;@@@@@jf', 4),"+
//                                 "(24, 'Dhading', 'wflbË', 6),"+
//                                 "(25, 'Nuwakot', 'g@@@@@jfsf]^', 6),"+
//                                 "(26, 'Kathmandu', 'sf&df)*f}', 6),"+
//                                 "(27, 'Bhaktapur', 'eQmk@@@@@<', 6),"+
//                                 "(28, 'Lalitpur', 'nlntk@@@@@<', 6),"+
//                                 "(29, 'Kavrepalanchowk', 'sfe|]knfGrf]s', 6),"+
//                                 "(30, 'Sindhupalchowk', 'l;Gw@@@@@kfNrf]s', 6),"+
//                                 "(31, 'Makwanpur', 'dsjfgk@@@@@<', 5),"+
//                                 "(32, 'Rautahat', '<f}tx^', 5),"+
//                                 "(33, 'Bara', 'jf<f', 5),"+
//                                 "(34, 'Parsa', 'k;f{', 5),"+
//                                 "(35, 'Chitwan', 'lrtjg', 5),"+
//                                 "(36, 'Gorkha', 'uf]<vf', 8),"+
//                                 "(37, 'Manang', 'dgfË', 9),"+
//                                 "(38, 'Lamjung', 'ndh@@@@@Ë', 8),"+
//                                 "(39, 'Kaski', 'sf:sL', 8),"+
//                                 "(40, 'Tanahu', 'tgx@@@@@', 8),"+
//                                 "(41, 'Shyanja', ':ofËhf', 8),"+
//                                 "(42, 'Gulmi', 'u@@@@@NdL', 7),"+
//                                 "(43, 'Palpa', 'kfNkf', 7),"+
//                                 "(44, 'Argakhanchi', 'cwf{vf_rL', 7),"+
//                                 "(45, 'Nawalparasi', 'gjnk<f;L', 7),"+
//                                 "(46, 'Rupandehi', '?kGb]xL', 7),"+
//                                 "(47, 'Kapilbastu', 'slknj:t@@@@@', 7),"+
//                                 "(48, 'Mustang', 'd@@@@@:tfË', 9),"+
//                                 "(49, 'Myagdi', 'DofUbL', 9),"+
//                                 "(50, 'Baglung', 'jfUn@@@@@Ë', 8),"+
//                                 "(51, 'Parbat', 'kj{t', 9),"+
//                                 "(52, 'Rukum', '?s@@@@@d', 10),"+
//                                 "(53, 'Rolpa', '<f]Nkf', 10),"+
//                                 "(54, 'Pyuthan', 'Ko@@@@@&fg', 10),"+
//                                 "(55, 'Salyan', ';Nofg', 10),"+
//                                 "(56, 'Dang', 'bfË', 10),"+
//                                 "(57, 'Dolpa', '*f]Nkf', 12),"+
//                                 "(58, 'Mugu', 'd@@@@@u@@@@@', 12),"+
//                                 "(59, 'Jumla', 'h@@@@@Dnf', 12),"+
//                                 "(60, 'Kalikot', 'sflnsf]^', 12),"+
//                                 "(61, 'Humla', 'x@@@@@Dnf', 12),"+
//                                 "(62, 'Jajarkot', 'hfh<sf]^', 11),"+
//                                 "(63, 'Dailekh', 'b}n]v', 11),"+
//                                 "(64, 'Surkhet', ';@@@@@v{]t', 11),"+
//                                 "(65, 'Banke', 'af_s]', 11),"+
//                                 "(66, 'Bardia', 'jlb{of', 11),"+
//                                 "(67, 'Bajura', 'jfh@@@@@<f', 13),"+
//                                 "(68, 'Acham', 'c%fd', 13),"+
//                                 "(69, 'Bajhang', 'jemfË', 13),"+
//                                 "(70, 'Doti', '*f]^L', 13),"+
//                                 "(71, 'Kailali', 's}nfnL', 13),"+
//                                 "(72, 'Darchula', 'bfr@@@@@{nf', 14),"+
//                                 "(73, 'Baitadi', 'j}t*L', 14),"+
//                                 "(74, 'Dadeldhuda', '**]nw@@@@@<f', 14),"+
//                                 "(75, 'Kanchanpur', 'sGrgk@@@@@<', 14)",[], function(tx, res) {
//                                 console.log(res);
//                                 return;
//                             },function(tx,res) {
//                                 console.error("error:",res);
//                                 return;
//                             });
//                     },function(tx,res) {
//                         console.error("error:",res);
//                         return;
//                     });
// }
return service;
}
