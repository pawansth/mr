var SQLiteStartDateHandler = function() {
    // getting sqlite connection
    var db = new SQLiteConnection();
    var service = {};

    service.pullStartDate = function() {
            //alert('inside pulldata function');
            var deferred = $.Deferred();
            db.transaction(function(tx) {
                tx.executeSql('SELECT * FROM job_start_info', [], function(tx, res) {
                    var informations = [];
                    console.log(res.rows.length);
                    //alert('data length'+res.rows.length);
                    for (var i = 0; i < res.rows.length; i++) {
                        var information = {
                            date_time: res.rows.item(i).date_time
                        };
                        //alert('name'+res.rows.item(i).name);
                        informations.push(information);
                    }
                    deferred.resolve(informations);

                }, function(e) {
                    deferred.reject(e);
                });
            });
            return deferred.promise();
        },

        service.pushStartDate = function(date_time) {
            var deferred = $.Deferred();
            console.log(db);
            db.transaction(function(tx) {
                tx.executeSql('INSERT INTO job_start_info (date_time) VALUES (?)', [date_time], function(tx, res) {
                    deferred.resolve();
                }, function(e) {
                    deferred.reject(e);
                });
            });
            return deferred.promise();
        },

        service.deleteStartDate = function() {
            var deferred = $.Deferred();
            console.log(db);
            db.transaction(function(tx) {
                tx.executeSql('DELETE FROM job_start_info', [], function(tx, res) {
                    deferred.resolve();
                }, function(e) {
                    deferred.reject(e);
                });
            });
            return deferred.promise();
        }

    return service;
}
