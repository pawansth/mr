// var orderForm = function() {
var orderTemplete = {
    SQLLObj: null,
    parentTr: null,
    ip: null,
    ctr: null,
    prodData: null,

    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        this.ip              = new ip();
        this.ip              = this.ip.remoteIpAddress();
        this.SQLLObj = new SQLiteLogin();
        this.ctr             = new controller();
        orderTemplete.loadDist();
        orderTemplete.dynamicTemplete();
        orderTemplete.removeTemplete();
        orderTemplete.keypressEvent();

        $(document).on('click', '#btn-order', orderTemplete.remoteOrderPlace);
        /* Tracking the selected option from datalist for distributor */
        $("#distNameSearch").on('input', function () {
          var value = $('#distNameSearch').val();

          $('#distList option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $('#distNameSearch').attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        /* Tracking the selected option from datalist for retailer */
        $("#retNameSearch").on('input', function () {
          var value = $('#retNameSearch').val();

          $('#retList option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $('#retNameSearch').attr('retId',$(this).attr('ret_id'));
             }
           });
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    //  method that load all distributor data related to server from database
    loadDist: function() {
        orderTemplete.addLoading();
      var tokenInformations = orderTemplete.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: orderTemplete.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                orderTemplete.loadProd();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    loadProd: function() {
      var tokenInformations = orderTemplete.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: orderTemplete.ip + '?r=order-handler/prod-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  orderTemplete.prodData = data;
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                  });
                  // templete += '</ul>';
                  $('#prodList').append(templete);
                  orderTemplete.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  orderTemplete.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
  //  method that dynamically add templete related to the item added by mr for order
    dynamicTemplete: function() {
        $(document).on('click', '#floating-btn', function(e) {
            e.preventDefault();
            if (!($('#productName').val().trim().length>0)) {
              navigator.notification.alert(
                     'Product name filed can not be empty',
                     null,
                     'Warrning',
                     'OK'
                 );
                 return;
            }
            if (!($('#productName').attr('prodId').trim().length>0)) {
              navigator.notification.alert(
                     'Product name filed is not set properly please try again.',
                     null,
                     'Warrning',
                     'OK'
                 );
                 return;
            }
            if (!($('#productUnits').val().trim().length>0)) {
              navigator.notification.alert(
                     'Product unit filed is not set please try again.',
                     null,
                     'Warrning',
                     'OK'
                 );
                 return;
            }
            if (!($('#productQty').val().trim().length>0)) {
              navigator.notification.alert(
                     'Product qty filed can not be empty.',
                     null,
                     'Warrning',
                     'OK'
                 );
                 return;
            }
            var float= /^\s*(\+|-)?((\d+(\.\d+)?)|(\.\d+))\s*$/;
            var a = $('#productPrice').val();
            if (!(float.test(a))) {
              navigator.notification.alert(
                     'Price should be in decimal or float number',
                     null,
                     'Warrning',
                     'OK'
                 );
                 return;
                }
            var totalItemCal = parseInt($('#totalItem').attr('totalItem'))+1;
            $('#totalItem').attr('totalItem',totalItemCal);
            $('#totalItem').text('Total Item:  '+totalItemCal);
            if ($('#productPrice').val().trim().length>0) {
              var totalAmountCal = parseFloat($('#totalAmount').attr('totalAmount'))+parseFloat($('#productPrice').val());
              $('#totalAmount').attr('totalAmount',totalAmountCal);
              $('#totalAmount').text('Total Amount:  '+totalAmountCal);
            }
            var inputList = '<!-- div for added items -->'+
                                      '<div class="addedItem">'+
                                      '<div class="itemContainer">'+
                                      '<div class=""><h5 class="productName" productId="'+$('#productName').attr('prodId')+'">'+$('#productName').val()+
                                      '<button id="floating-btn-minus" class="btn btn-warning glyphicon glyphicon-remove pull-right"></button>'+
                                      '</h5></div>'+
                                      '<div class="pull-left"><span class="productQty" productQty="'+$('#productQty').val()+'">&nbsp;&nbsp;&nbsp;&nbsp;QTY:&nbsp;&nbsp;'+$('#productQty').val()+'</span></div>'+
                                      '<div class="pull-left"><span class="productUnit" productUnit="'+$('#productUnits').attr('prodUnitId')+'">&nbsp;&nbsp;&nbsp;&nbsp;UNIT:&nbsp;&nbsp;'+$("#productUnits").val()+'</span></div>'+
                                      '<div class=""><span class="productPrice" productPrice="'+$('#productPrice').val()+'">&nbsp;&nbsp;&nbsp;&nbsp;PRICE:&nbsp;&nbsp;'+$('#productPrice').val()+'</span></div>'+
                                      '</div>'+
                                      '</div>';
            $('.addedList').append(inputList);
            orderTemplete.clearOrderFields();
        });
    },
    clearOrderFields: function() {
      $('#productName').val('');
      $('#productName').attr('productId','');
      $('#productQty').val('');
      $('.indUnit').remove();
      $('#productPrice').val(0);
    },

    removeTemplete: function() {
        $(document).on('click', '#floating-btn-minus', function(e) {
            e.preventDefault();
            var totalItemCal = parseInt($('#totalItem').attr('totalItem'))-1;
            $('#totalItem').attr('totalItem',totalItemCal);
            $('#totalItem').text('Total Item:  '+totalItemCal);
            if ($(this).parents('.addedItem').find('.productPrice').attr('productPrice').trim().length>0) {
              var totalAmountCal = parseFloat($('#totalAmount').attr('totalAmount')) - parseFloat($(this).parents('.addedItem').find('.productPrice').attr('productPrice'));
              $('#totalAmount').attr('totalAmount',totalAmountCal);
              $('#totalAmount').text('Total Amount:  '+totalAmountCal);
            }
            $(this).parents('.addedItem').remove();
        });
    },

    keypressEvent: function() {
        $(document).on('click', '#estDelivaryDate', orderTemplete.showDatePicker);
        $(document).on('focusout', '#distNameSearch', function() {
            if($('#distNameSearch').val().trim() === '') {
                $('#distNameSearch').attr('distId', '');
            }
        });
        $(document).on('focusout', '#productName', function() {
            if($('#productName').val().trim() === '') {
                $('#productName').attr('prodId', '');
                $('#productName').attr('baseUnitId', '');
                $('#productName').attr('price', '');
            }
            orderTemplete.calculateAmount($(this).parent().parent());
        });
        $(document).on('focusout', '#productUnits', function() {
            if($('#productUnits').val().trim() === '') {
                $('#productUnits').attr('prodUnitId', '');
            }
            orderTemplete.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('focusout', '#productQty', function() {
            orderTemplete.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('input', '#productName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var parent = $(this).parent().parent();
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodId',$(this).attr('prod_id'));
               $(ob).attr('baseUnitId',$(this).attr('base_unit_id'));
               $(ob).attr('price',$(this).attr('base_price'));
               orderTemplete.addLoading();
               $('.loadingText').text('Loading units...');
               orderTemplete.searchCrosProdUnit(parent);
             }
           });
        });
        $(document).on('input', '#productUnits', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodUnitId',$(this).attr('unit_id'));
               return;
             }
           });
        });
    },
    showDatePicker: function() {
      var options = {
          date: new Date(),
          minDate: Date.parse(new Date()),
          mode: 'date',
          windowTitle: 'Estimate Delivary Date'
        };

        datePicker.show(options, function(d){
          var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
          $('#estDelivaryDate').val(datestring);
        });
    },
    /* searching corresponding product unit */
    searchCrosProdUnit: function(currentObj) {
      crossProductId = currentObj.find('#productName').attr('prodId');
    //   alert(crossProductId);
      //  Pulling access token form sqlite database
      var tokenInformations = orderTemplete.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken,
              crossProductId: crossProductId
          };

          $.ajax({
              url: orderTemplete.ip + '?r=order-handler/search-cross-unit',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  console.log(data);
                  
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }

                  var templete = '';
                  
                  $.each(data, function() {
                      templete += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                  });
                  console.log(currentObj.find('#unitList'));
                  currentObj.find('#unitList').append(templete);
                  orderTemplete.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  orderTemplete.searchCrosProdUnit,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    calculateAmount: function(parentObj) {
        if(!(parentObj.find('#productName').val().trim() === '' || parentObj.find('#productName').attr('prodId').trim() === '')) {
            if(!(parentObj.find('#productUnits').val().trim() === '' || parentObj.find('#productUnits').attr('prodUnitId').trim() === '')) {
                if(!(parentObj.find('#productQty').val().trim() === '')) {
                    var tokenInformations = orderTemplete.SQLLObj.pullToken();
                    tokenInformations.done(function(informations) {
                        var accessToken = '';
                        for (var idx in informations) {
                            var information = informations[idx];
                            //alert(information.name);
                            accessToken = information.token;
                        }
                        if (accessToken.trim() === '') {
                            console.log('there is no accessToken');
                            window.location.href = "../index.html";
                            return;
                        }

                        // console.log(accessToken);

                        var values = {
                            accessToken: accessToken,
                            productId  : parentObj.find('#productName').attr('prodId'),
                            uid        : parentObj.find('#productUnits').attr('prodUnitId'),
                            base_uid   : parentObj.find('#productName').attr('baseUnitId'),
                            qtyToChange: parentObj.find('#productQty').val()
                        };
                        orderTemplete.addLoading();
                        $('.loadingText').text('calcuation amount');
                    $.ajax({
                        url: orderTemplete.ip + '?r=major-dist-plan/unit-to-price',
                        method: "POST",
                        dataType: 'json',
                        data: values,

                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                            navigator.notification.alert(
                            'There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                        },

                        success: function(data) {
                            orderTemplete.removeLoading();
                            if (data === 2) {
                                window.location.href = "../index.html";
                            }
                            parentObj.find('#productPrice').val(data);
                        }
                    }).fail(function(error) {
                        console.error(error);
                        orderTemplete.removeLoading();
                        navigator.notification.alert(
                            ' There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                    });
                    }).fail(function(error) {
                        console.error(error);
                    });
                }
            }
        }
        return;
    },

    remoteOrderPlace: function(e) {
        e.preventDefault();
        var distName = 0;
        var retName  = 0;
        var poId        = 0;
        var estDate   = 0;
        if (!($('#distNameSearch').val().trim().length>0)) {
          navigator.notification.alert(
                 'Distributor field must be provided',
                 null,
                 'Warrning',
                 'OK'
             );
             return;
        }
        if (!($('#poId').val().trim().length>0)) {
          navigator.notification.alert(
                 'POID must be Provided',
                 null,
                 'Warrning',
                 'OK'
             );
             return;
        }
        if (!($('#estDelivaryDate').val().trim().length>0)) {
          navigator.notification.alert(
                 'Delivery Estimated date must be provided',
                 null,
                 'Warrning',
                 'OK'
             );
             return;
        }
        // if ($('#distNameSearch').val() === '' && $('#retNameSearch').val() === '') {
        //     navigator.notification.alert(
        //         'One of the distributor or retailer field must be provide',
        //         null,
        //         'Warrning',
        //         'OK'
        //     );
        //     return;
        // } else if (nameTest === 1) {
        //     navigator.notification.alert(
        //         ' Product name field can not be empty',
        //         null,
        //         'Warrning',
        //         'OK'
        //     );
        //     return;
        // } else if (idTest === 1) {
        //     navigator.notification.alert(
        //         ' Product id field can not be empty',
        //         null,
        //         'Warrning',
        //         'OK'
        //     );
        //     return;
        // } else if (qtyTest === 1) {
        //     navigator.notification.alert(
        //         ' Quantity field can not be empty',
        //         null,
        //         'Warrning',
        //         'OK'
        //     );
        //     return;
        // } else {
        //     orderTemplete.orderSendToRemote();
        // }
        orderTemplete.orderSendToRemote();

    },

    orderSendToRemote: function() {
        var productValues = [];
        var distId                     = $('#distNameSearch').attr('distId');
        var poId                       = $('#poId').val();
        var est_delevary_date = new Date($('#estDelivaryDate').val()).getTime() / 1000;
        var desc                       = $('#order_desc').val();
        var totalItem                = $('#totalItem').attr('totalItem');
        var totalAmount          = $('#totalAmount').attr('totalAmount');
        var customeInfo          = {
                                                distId                     : distId,
                                                poId                      : poId,
                                                est_delevary_date: est_delevary_date,
                                                desc                      : desc,
                                                totalItem               : totalItem,
                                                totalAmount         : totalAmount
                                            };
        // console.log(customeInfo);
        var productId      = '';
        var productQty   = '';
        var productUnit  = '';
        var productPrice = '';

        $.each($('.addedItem'), function() {
            productId      = $(this).find('.productName').attr('productId');
            productQty   = $(this).find('.productQty').attr('productQty');
            productUnit  = $(this).find('.productUnit').attr('productUnit');
            productPrice = $(this).find('.productPrice').attr('productPrice');
            var data = {
                productId     : productId,
                productQty  : productQty,
                productUnit : productUnit,
                productPrice: productPrice
            };
            // pushing json formate data to array
            productValues.push(data);
        });
        console.log(productValues);
        // sending data to server using ajax
        var tokenInformations = orderTemplete.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken: accessToken,
                customeInfo: customeInfo,
                productValues: productValues
            };

            $.ajax({
                url: orderTemplete.ip + '?r=order-handler/company-order',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    // console.log(data);
                    if (data === 1) {
                        navigator.notification.alert(
                            ' Successfully placed order.',
                            null,
                            'Success',
                            'OK'
                        );
                        window.location.href = "../views/orderForm.html";
                    } else if (data === 0) {
                        navigator.notification.alert(
                            ' Failed to place order',
                            null,
                            'Error',
                            'OK'
                        );
                    } else if (data === 2) {
                        window.location.href = "../index.html";
                    }

                }
            }).fail(function(error) {
                console.error(error);
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });

        }).fail(function(error) {
            console.error(error);
        });
    }

}

// calling orderTemplete's initialize method
orderTemplete.initialize();
