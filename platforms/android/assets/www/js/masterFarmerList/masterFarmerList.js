var masterFarmerList = {
    SQLLObj  : null,
    ip       : null,
    ctr      : null,
    locSet   : null,
    lanConv  : null,
    currType : null,
    distData : null,
    retData  : null,
    distCount: null,
    retCount : null,
    cam      : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        masterFarmerList.ip      = new ip();
        masterFarmerList.ip      = masterFarmerList.ip.remoteIpAddress();
        masterFarmerList.ctr     = new controller();
        masterFarmerList.SQLLObj = new SQLiteLogin();
        masterFarmerList.locSet  = new locationSet();

        masterFarmerList.addLoading();
        masterFarmerList.loadDist();

        masterFarmerList.distCount = 0;
        masterFarmerList.retCount  = 0;
        masterFarmerList.currType  = '1';
        masterFarmerList.lanConv   = new landConverter();
        //  Creatng object of camera
        masterFarmerList.cam       = new camera();
        masterFarmerList.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#camera', function(e) {
            masterFarmerList.cam.openDefaultCamera();
        });
        $(document).on('click','#gallary', function(e) {
            masterFarmerList.cam.openFilePicker();
        });
        $(document).on('click', '#mapBtn', masterFarmerList.loadMpa);
        $(document).on('click', '#gpsBtn', masterFarmerList.setLocation);
        $(document).on('change', '#landMeasure', masterFarmerList.areaConverter);
        $(document).on('click', '.dist-add', masterFarmerList.addDist);
        $(document).on('click', '.dist-minus', masterFarmerList.remove);
        $(document).on('click', '.ret-add', masterFarmerList.addRet);
        $(document).on('click', '.ret-minus', masterFarmerList.remove);
        $(document).on('click', '.crop-add' , masterFarmerList.addCrop);
        $(document).on('click', '.crop-minus', masterFarmerList.remove);
        $(document).on('click', '.send-Master-farmer', masterFarmerList.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
    },
    setLocation: function() {
    masterFarmerList.locSet.setPosition();
  },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    //  Function that convert land area to different area
    areaConverter: function() {
        var value = masterFarmerList.lanConv.initialize($('#farArea').val(),masterFarmerList.currType,$('#landMeasure').val());
        masterFarmerList.currType = $('#landMeasure').val();
        $('#farArea').val(value);
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = masterFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterFarmerList.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterFarmerList.distData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  masterFarmerList.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = masterFarmerList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterFarmerList.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterFarmerList.retData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#retList').append(templete);
                  masterFarmerList.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+masterFarmerList.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+masterFarmerList.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+masterFarmerList.distCount;
        var templeteOption = '';
                  
        $.each(masterFarmerList.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        masterFarmerList.distCount = masterFarmerList.distCount + 1;
    },
    remove: function() {
        $(this).parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+masterFarmerList.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+masterFarmerList.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+masterFarmerList.retCount;
        var templeteOption = '';
                  
        $.each(masterFarmerList.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        masterFarmerList.retCount = masterFarmerList.retCount + 1;
    },
    addCrop: function() {
        var templete = '';
        templete += '<div class="cropData row">'+
                        '<div class="col-xs-9" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="cultCrop" placeholder="cultivated crop">'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 crop-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus crop-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:5px;"></button>'+
                    '</div>';
        $('.cropList').append(templete);
    },
    isEmail: function() {
    var email = $('#farEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#farName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#farArea').val().trim() === '') {
            navigator.notification.alert(
                            'area can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        // Checking if email address is empty or not
        if(!($('#farEmail').val().trim() === '')) {
            var checkEmail = masterFarmerList.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
            return;
            }
        }
        masterFarmerList.addLoading();
        $('.loadingText').text('Sending......');
        masterFarmerList.sendRemote();
    },
    sendRemote: function() {
        var farmerInfo = [];
        farDetail = {
            name: $('#farName').val(),
            number: $('#farNumber').val(),
            lat: $('#latOfLoc').attr('lat'),
            lng: $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address: $('#farAddress').val(),
            email: $('#farEmail').val(),
            viber: $('#farViber').val(),
            facebook: $('#farFacebook').val(),
            area: $('#farArea').val(),
            measureType: $('#landMeasure').val()
        }
        farmerInfo.push(farDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                    dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        farmerInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        farmerInfo.push(retList);
        var cropList = [];
        $.each($('.cropData'), function() {
            if(!($(this).find('#cultCrop').val().trim() === '')) {
                var data = {
                crop: $(this).find('#cultCrop').val()
                }
                cropList.push(data);
            }
        });
        farmerInfo.push(cropList);
        console.log(farmerInfo);
        // sending data to server using ajax
        var tokenInformations = masterFarmerList.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('#photo').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1);
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to stor field data
            var params             = new Object();
            params.accessToken     = accessToken;
            params.farmerInfo      = farmerInfo;
             // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;

            var ft = new FileTransfer();
            ft.upload(imageData, masterFarmerList.ip + '?r=master-farmer-list/handler', function(result){
                console.log('data is: ',result);
                masterFarmerList.removeLoading();
                var response = $.parseJSON(result.response);
                if(response.status === 1) {
                navigator.notification.alert(
                            response.msgBody,
                            null,
                            response.msgTitle,
                            'OK'
                        );
                     window.location.href = './masterFarmerList.html';
                }
                if(response.status === 0) {
                navigator.notification.alert(
                            response.msgBody,
                            null,
                            response.msgTitle,
                            'OK'
                        );
                }
                if(response.status === 2) {
                navigator.notification.alert(
                            response.msgBody,
                            null,
                            response.msgTitle,
                            'OK'
                        );
                        window.location.href = "../index.html";
                }
            }, function(error){
                console.error(error);
                masterFarmerList.removeLoading();
                navigator.notification.alert(
                    'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                    null,
                    'ERROR',
                    'OK'
                );
            }, options);
        }).fail(function(error) {
            console.error(error);
            masterFarmerList.removeLoading();
            navigator.notification.alert(
                'Error occured',
                null,
                'ERROR',
                'OK'
            );
        });
    }
}
masterFarmerList.initialize();