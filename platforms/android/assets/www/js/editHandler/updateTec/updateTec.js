var updateTec = {
    SQLLObj  : null,
    ip       : null,
    ipObj    : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    distCount: null,
    retCount : null,
    cam      : null,
    globID   : null,
    camUpdate: null,
    editIndex: 0,
    globalAccessToken: null,
    imgPath  : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateTec.ipObj     = new ip();
        updateTec.ip        = updateTec.ipObj.remoteIpAddress();
        updateTec.imgPath   = updateTec.ipObj.remoteImgIp();
        updateTec.ctr       = new controller();
        updateTec.SQLLObj   = new SQLiteLogin();
        updateTec.locSet    = new locationSet();
        updateTec.camUpdate = new cameraForUpdate();

        updateTec.addLoading();
        updateTec.loadDist();

        updateTec.distCount = 0;
        updateTec.retCount  = 0;
        var passedData      = window.localStorage.getItem('dataToPass');
        var dataPassed      = JSON.parse(passedData);
        updateTec.globID    = dataPassed.tecId;
        updateTec.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#mapBtn', function() {
            if(!(updateTec.editIndex === 0)) {
                updateTec.loadMpa();
            }
        });
        $(document).on('click', '#gpsBtn', function() {
            if(!(updateTec.editIndex === 0)) {
                updateTec.setLocation();
            }
        });
        $(document).on('click', '.dist-add', function() {
            if(!(updateTec.editIndex === 0)) {
                updateTec.addDist();
            }
        });
        $(document).on('click', '.dist-minus', function() {
            var obj = $(this);
            if(!(updateTec.editIndex === 0)) {
                updateTec.remove(obj);
            }
        });
        $(document).on('click', '.ret-add', function() {
            if(!(updateTec.editIndex === 0)) {
                updateTec.addRet();
            }
        });
        $(document).on('click', '.ret-minus', function() {
            var obj = $(this);
            if(!(updateTec.editIndex === 0)) {
                updateTec.remove(obj);
            }
        });
        $(document).on('click', '.prodFocus-add' , function() {
            if(!(updateTec.editIndex === 0)) {
                updateTec.addProdFocus();
            }
        });
        $(document).on('click', '.prodFocus-minus', function() {
            var obj = $(this);
            if(!(updateTec.editIndex === 0)) {
                updateTec.remove(obj);
            }
        });
        $(document).on('click', '.send-Master-technican', updateTec.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('click', '#btn-edit', function() {
            $('.title').text('Technician Edit');
            updateTec.edit();
        });
        updateTec.noEdit();
    },
    setLocation: function() {
        updateTec.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = updateTec.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateTec.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: updateTec.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateTec.distData = data;
                  updateTec.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateTec.loadDist,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
          var values = {
              accessToken: updateTec.globalAccessToken
          };
          var path = updateTec.ip + '?r=order-handler/ret-list';
          var result = updateTec.ajaxCall(path,values);
            result.then(function(data) {
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            updateTec.retData = data;
            updateTec.loadTec();
        }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateTec.loadRet,
                  'Error',
                  'Try Again'
              );
          });
    },
    // Method to load all data of a technican
    loadTec: function() {
        var values = {
            accessToken: updateTec.globalAccessToken,
            id         : updateTec.globID
        };
        var path = updateTec.ip + '?r=update-techinican/tec-info';
        var result = updateTec.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = '../index.html';
            }
            console.log(data);
            updateTec.removeLoading();
            $('#photo').attr('src',updateTec.imgPath+data[0].image_url);
            $('#tecName').val(data[0].name);
            $('#tecNumber').val(data[0].number);
            $('#latOfLoc').attr('lat',data[0].latitude);
            $('#latOfLoc').text(data[0].latitude);
            $('#lngOfLoc').attr('lng',data[0].longitude);
            $('#lngOfLoc').text(data[0].longitude);
            $('#postalCode').val(data[0].postal_code);
            $('#tecAddress').val(data[0].address);
            $('#tecWorkingArea').val(data[0].working_area);
            $('#tecEmail').val(data[0].email);
            $('#tecViber').val(data[0].viber);
            $('#tecFacebook').val(data[0].facebook);
            // code to display dist data
            var distToDisplay = [];
            for(var i = 0; i<updateTec.distData.length; i++) {
                for(var j = 0; j<data[2].length; j++) {
                    if(updateTec.distData[i].id === data[2][j].dist_id) {
                        var presentData = {
                            rowId  : data[2][j].id,
                            dist_id: data[2][j].dist_id,
                            name: updateTec.distData[i].dist_name
                        };
                        distToDisplay.push(presentData);
                    }
                }
            }
            $('.distList').empty();
            for(var i = 0; i<distToDisplay.length; i++) {
                var templete = '<div class="distData row">'+
                                    '<div class="col-xs-8" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" id="distNameSearch" value="'+distToDisplay[i].name+'" distId="'+distToDisplay[i].dist_id+'" rowId="'+distToDisplay[i].rowId+'" list="distList'+i+'" disabled placeholder="distributor name">'+
                                        '<datalist id="distList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus dist-add col-xs-2" style="margin-top:5px;"></button>'+
					            '</div>';
                $('.distList').append(templete);
                var target = '#distList'+i;
                var templeteOption = '';
                  
                $.each(updateTec.distData, function() {
                    templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateTec.distCount = distToDisplay.length;
            // code to display ret data
            var retToDisplay = [];
            for(var i = 0; i<updateTec.retData.length; i++) {
                for(var j = 0; j<data[1].length; j++) {
                    if(updateTec.retData[i].id === data[1][j].ret_id) {
                        var presentData = {
                            rowId  : data[1][j].id,
                            ret_id: data[1][j].ret_id,
                            name: updateTec.retData[i].retail_name
                        };
                        retToDisplay.push(presentData);
                    }
                }
            }
            $('.retList').empty();
            for(var i = 0; i<retToDisplay.length; i++) {
                var templete = '<div class="retData row">'+
					                '<div class="col-xs-8" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" value="'+retToDisplay[i].name+'" retId="'+retToDisplay[i].ret_id+'" rowId="'+retToDisplay[i].rowId+'" id="retNameSearch" list="retList'+i+'" disabled placeholder="retailer name">'+
                                        '<datalist id="retList'+i+'"></datalist>'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
					            '</div>';
                $('.retList').append(templete);
                var target = '#retList'+i;
                var templeteOption = '';
                  
                $.each(updateTec.retData, function() {
                    templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                });
                 $(target).append(templeteOption);
            }
            updateTec.retCount = retToDisplay.length;
            // Code for cultivated crop
            $('.prodFocusList').empty();
            for(var i = 0; i<data[3].length; i++) {
                var templete = '<div class="prodFocusData row">'+
                                    '<div class="col-xs-9" style="padding-top:5px;">'+
                                        '<input type="text" class="form-control" id="prodFocus" value="'+data[3][i].product_focuse+'" rowId="'+data[3][i].id+'" disabled placeholder="Product Focus">'+
                                    '</div>'+
                                    '<button type="button" class="btn btn-warning col-xs-1 prodFocus-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                                '</div>';
                $('.prodFocusList').append(templete);
            }
        }).catch(function(error) {
            console.error(error);
            navigator.notification.alert(
                'There is error in network',
                updateTec.loadTec,
                'ERROR',
                'Try Again'
            );
        });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+updateTec.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+updateTec.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+updateTec.distCount;
        var templeteOption = '';
                  
        $.each(updateTec.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        updateTec.distCount = updateTec.distCount + 1;
    },
    remove: function(obj) {
        obj.parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+updateTec.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+updateTec.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+updateTec.retCount;
        var templeteOption = '';
                  
        $.each(updateTec.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        updateTec.retCount = updateTec.retCount + 1;
    },
    addProdFocus: function() {
        var templete = '';
        templete += '<div class="prodFocusData row">'+
                        '<div class="col-xs-9" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="prodFocus" rowId="0" placeholder="Product Focus">'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 prodFocus-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus prodFocus-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:5px;"></button>'+
                    '</div>';
        $('.prodFocusList').append(templete);
    },
    isEmail: function() {
    var email = $('#tecEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#tecName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#tecNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#tecAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#tecWorkingArea').val().trim() === '') {
            navigator.notification.alert(
                            'working area can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if(!($('#tecEmail').val().trim() === '')) {
            var checkEmail = updateTec.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
                return;
            }
        }
        updateTec.addLoading();
        $('.loadingText').text('updating......');
        updateTec.sendRemote();
    },
    sendRemote: function() {
        var tecInfo = [];
        tecDetail = {
            name        : $('#tecName').val(),
            number      : $('#tecNumber').val(),
            lat         : $('#latOfLoc').attr('lat'),
            lng         : $('#lngOfLoc').attr('lng'),
            postalCode  : $('#postalCode').val(),
            address     : $('#tecAddress').val(),
            working_area: $('#tecWorkingArea').val(),
            email       : $('#tecEmail').val(),
            viber       : $('#tecViber').val(),
            facebook    : $('#tecFacebook').val()
        }
        tecInfo.push(tecDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                    id: $(this).find('#distNameSearch').attr('rowId'),
                    dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        tecInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                    id: $(this).find('#retNameSearch').attr('rowId'),
                    ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        tecInfo.push(retList);
        var prodFocus = [];
        $.each($('.prodFocusData'), function() {
            if(!($(this).find('#prodFocus').val().trim() === '')) {
                var data = {
                    id: $(this).find('#prodFocus').attr('rowId'),
                    product_focuse: $(this).find('#prodFocus').val()
                }
                prodFocus.push(data);
            }
        });
        tecInfo.push(prodFocus);
        console.log(tecInfo);
        var values = {
            accessToken: updateTec.globalAccessToken,
            id         : updateTec.globID,
            tecInfo    : tecInfo
        }
        var path = updateTec.ip+'?r=update-techinican/make-update';
        var result = updateTec.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                updateTec.removeLoading();
                window.location.href = '../index.html';
            } else if(data.status === 0) {
                updateTec.removeLoading();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    data.msgTitle,
                    'OK'
                );
            } else if(data.status === 1) {
                updateTec.loadTec();
                updateTec.remove();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    'data.msgTitle',
                    'OK'
                );
                $('.title').text('Technician Detail');
                updateTec.noEdit();
            }
        }).catch(function(error) {
            updateTec.removeLoading();
            console.error(error);
            navigator.notification.alert(
                ' There is error in network',
                null,
                'Error',
                'OK'
            );
        });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    noEdit: function() {
        $('#tecName').attr('disabled', true);
        $('#tecNumber').attr('disabled', true);
        $('#postalCode').attr('disabled', true);
        $('#tecAddress').attr('disabled', true);
        $('#tecWorkingArea').attr('disabled', true);
        $('#tecEmail').attr('disabled', true);
        $('#tecViber').attr('disabled', true);
        $('#tecFacebook').attr('disabled', true);
        $('.send-Master-technican').attr('disabled', true);
    },
    edit: function() {
        updateTec.editIndex = 1;
        $('#tecName').attr('disabled', false);
        $('#tecNumber').attr('disabled', false);
        $('#postalCode').attr('disabled', false);
        $('#tecAddress').attr('disabled', false);
        $('#tecWorkingArea').attr('disabled',false);
        $('#tecEmail').attr('disabled', false);
        $('#tecViber').attr('disabled', false);
        $('#tecFacebook').attr('disabled', false);
        $('.send-Master-technican').attr('disabled', false);
        $.each($('.distData'), function() {
            $(this).find('#distNameSearch').attr('disabled', false);
        });
        $.each($('.retData'), function() {
            $(this).find('#retNameSearch').attr('disabled', false);
        });
        $.each($('.prodFocusData'), function() {
            $(this).find('#prodFocus').attr('disabled', false);
        });
        $('.div-image-updater').load('./imageUpdater.html', function() {
            $('.div-prev-img').hide();
            $(document).on('click', '.div-def-img', function() {
            $('.div-change-image-container').css({left: 0});
        });
        $(document).on('click', '.div-img-change-opt-1',function() {
            updateTec.camUpdate.openDefaultCamera();
        });
        $(document).on('click', '.div-img-change-opt-2', function() {
            updateTec.camUpdate.openFilePicker();
        });
        $(document).on('click', '.div-change-image-container', function() {
            $('.div-change-image-container').css({left: '-100%'});
        });
        $(document).on('click', '.btn-done-upload', function() {
            updateTec.addLoading();
            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('.img-prev').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to store field data
            var params         = new Object();
            params.accessToken = updateTec.globalAccessToken;
            params.id          = updateTec.globID;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;
            // Creating object for file transfer class
            var ft = new FileTransfer();
            ft.upload(imageData, updateTec.ip + '?r=update-techinican/photo-update', function(result){
              console.log('data is: ',result);
              updateTec.removeLoading();
              var response = $.parseJSON(result.response);
              if(response.status === 1) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      $('#photo').attr('src',$('.img-prev').attr('src'));
                      $('.img-prev').hide();
              }
              if(response.status === 0) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                    $('.img-prev').hide();
              }
              if(response.status === 2) {
                navigator.notification.alert(
                          response.msgBody,
                          null,
                          response.msgTitle,
                          'OK'
                      );
                      window.location.href = "../index.html";
              }
            }, function(error){
              console.error(error);
              updateTec.removeLoading();
              $('.img-prev').hide();
              navigator.notification.alert(
                'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                null,
                'ERROR',
                'OK'
              );
            }, options);
        });
        $(document).on('click', '.btn-cancel-upload', function() {
            $('.div-prev-img').hide();
        });
        });
    }
}
updateTec.initialize();