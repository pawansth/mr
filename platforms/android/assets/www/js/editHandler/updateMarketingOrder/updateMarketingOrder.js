var updateMarketingOrder = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    globalAccessToken: null,
    orderID: null,
    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateMarketingOrder.ip      = new ip();
        updateMarketingOrder.ip      = updateMarketingOrder.ip.remoteIpAddress();
        updateMarketingOrder.SQLLObj = new SQLiteLogin();
        updateMarketingOrder.ctr     = new controller();

        var passedData                  = window.localStorage.getItem('dataToPass');
        var dataPassed                  = JSON.parse(passedData);
        updateMarketingOrder.orderID    = dataPassed.orderID;
        $('#clientNameSearch').val(updateMarketingOrder.clientName);

        updateMarketingOrder.bindEvents();
        updateMarketingOrder.addLoading();
        updateMarketingOrder.loadMrketOrder();
    },
    bindEvents: function() {
        $(document).on('click', '.action', updateMarketingOrder.updateOne);
        $(document).on('click', '.all-action', updateMarketingOrder.updateAll);
        $(document).on('click', '#btn-edit', function() {
            var dataToPass = {
                'orderID': updateMarketingOrder.orderID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './editMarketingOrder.html';
            }, 30);
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    updateAll: function(e) {
        var status = $(this).val();

        var values = {
            accessToken: updateMarketingOrder.globalAccessToken,
            id         : updateMarketingOrder.orderID,
            status     : status
        };
        updateMarketingOrder.addLoading();
        var path = updateMarketingOrder.ip + '?r=update-marketing-order/update-all';
        var result = updateMarketingOrder.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = "../index.html";
            }
            $.each($('.action'), function() {
                if($(this).val() === status) {
                    $(this).attr('checked', true);
                }
            });
            updateMarketingOrder.removeLoading();
            navigator.notification.alert(
                  data.msgBody,
                  null,
                  data.msgTitle,
                  'Ok'
              );
        }).catch(function(error) {
            console.error(error);
            updateMarketingOrder.removeLoading();
            navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'Ok'
              );
        });
    },
    updateOne: function() {
        var status = $(this).val();
        var rowId = $(this).parent().parent().parent().attr('rowId');

        var values = {
            accessToken: updateMarketingOrder.globalAccessToken,
            id         : rowId,
            status     : status
        };
        updateMarketingOrder.addLoading();
        var path = updateMarketingOrder.ip + '?r=update-marketing-order/update-one';
        var result = updateMarketingOrder.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = "../index.html";
            }
            updateMarketingOrder.removeLoading();
            navigator.notification.alert(
                  data.msgBody,
                  null,
                  data.msgTitle,
                  'Ok'
              );
        }).catch(function(error) {
            console.error(error);
            updateMarketingOrder.removeLoading();
            navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'Ok'
              );
        });
    },
    loadMrketOrder: function() {
      var tokenInformations = updateMarketingOrder.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateMarketingOrder.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }
          var values = {
              accessToken: updateMarketingOrder.globalAccessToken,
              id: updateMarketingOrder.orderID
          };
          var path = updateMarketingOrder.ip + '?r=update-marketing-order/marketing-order-detail';
          var result = updateMarketingOrder.ajaxCall(path, values);
          result.then(function(data) {
              console.log(data);
              if(data.status === 2) {
                  window.location.href = "../index.html";
              }
              var d = new Date(parseInt(data[0].est_delivery_date)*1000);
              var date = d.getFullYear() +'-'+ (d.getMonth() +1) +'-'+ d.getDate();
              $('#retNameSearch').val(data[0].ret_name);
              $('#retNameSearch').attr('retId', data[0].ret_id);
              $('#distNameSearch').val(data[0].dist_name);
              $('#distNameSearch').attr('distId',data[0].dist_id);
              $('#poId').val(data[0].po_id);
              $('#estDelivaryDate').val(date);
              $('#order_desc').val(data[0].order_desc);
              for(var i = 0; i<data[1].length; i++) {
                  var radio = '';
                  if(data[1][i].status === 1){
                      radio =   '<div class="div-action">'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" checked name="action'+i+'" value="1"> send</label>'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" name="action'+i+'" value="2"> cancle</label>'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" name="action'+i+'" value="0"> panding</label>'+
                                    '<hr/>'+
                                '</div>';
                  } else if(data[1][i].status === 2){
                      radio =   '<div class="div-action">'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" name="action'+i+'" value="1"> send</label>'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" checked name="action'+i+'" value="2"> cancle</label>'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" name="action'+i+'" value="0"> panding</label>'+
                                    '<hr/>'+
                                '</div>';
                  } else if(data[1][i].status === 0){
                      radio =   '<div class="div-action">'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" name="action'+i+'" value="1"> send</label>'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" name="action'+i+'" value="2"> cancle</label>'+
                                    '<label class="checkbox-inline"><input type="radio" class="action" style="-webkit-appearance: checkbox;-moz-appearance:-ms-appearance" checked name="action'+i+'" value="0"> panding</label>'+
                                    '<hr/>'+
                                '</div>';
                  }
                  var templete =    '<div class="div-order-container row" rowId="'+data[1][i].id+'" style="padding-left:5px;padding-right:5px;">'+
                                        '<label class="nopadding-right col-xs-1">'+i+'</label>'+
                                        '<label class="nopadding col-xs-7">'+data[1][i].product_name+'</label>'+
                                        '<label class="nopadding col-xs-2">'+data[1][i].unit_name+'</label>'+
                                        '<label class="nopadding-left col-xs-2">'+data[1][i].product_qty+'</label>'+
                                        radio+
                                    '</div>';
                    $('.div-order-list').append(templete); 
              }
              updateMarketingOrder.removeLoading();
          }).catch(function(error) {
              console.error(error);
              updateMarketingOrder.removeLoading();
              navigator.notification.alert(
                  ' There is error in network',
                  updateMarketingOrder.loadMrketOrder,
                  'Error',
                  'Try Again'
              );
          });
        }).fail(function(error) {
          console.error(error);
      });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    }
}
updateMarketingOrder.initialize();