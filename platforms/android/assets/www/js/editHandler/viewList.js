var viewList = {
    SQLLObj: null,
    ipObj  : null,
    ip     : null,
    ctr    : null,
    globalAccessToken: null,
    imgPath: null,
    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        viewList.ipObj           = new ip();
        viewList.ip              = viewList.ipObj.remoteIpAddress();
        viewList.imgPath         = viewList.ipObj.remoteImgIp();
        viewList.ctr             = new controller();
        viewList.SQLLObj         = new SQLiteLogin();
        viewList.bindEvents();
        $('.data-type').val(1);
        viewList.loadDist();
    },
    bindEvents: function() {
        $(document).on('change', '.data-type', viewList.typeSelect);
        $(document).on('click', '.div-dist-detail', function(e) {
            e.preventDefault();
            var distributorID = $(this).find('#dist-id').attr('distId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'distId': distributorID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateDist.html';
            }, 30);
        });
        $(document).on('click', '.div-ret-detail', function(e) {
            e.preventDefault();
            var retailerID = $(this).find('#ret-id').attr('retId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'retId': retailerID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateRet.html';
            }, 30);
        });
        $(document).on('click', '.div-far-detail', function(e) {
            e.preventDefault();
            var farmerID = $(this).find('#far-id').attr('farId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'farId': farmerID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateFar.html';
            }, 30);
        });
        $(document).on('click', '.div-tec-detail', function(e) {
            e.preventDefault();
            var techinicanID = $(this).find('#tec-id').attr('tecId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'tecId': techinicanID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateTec.html';
            }, 30);
        });
        $(document).on('click', '.div-cattle-detail', function(e) {
            e.preventDefault();
            var cattleID = $(this).find('#cattle-id').attr('cattleId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'cattleId': cattleID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateCattle.html';
            }, 30);
        });
        $(document).on('click', '.div-poultry-detail', function(e) {
            e.preventDefault();
            var poultryID = $(this).find('#poultry-id').attr('poultryId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'poultryId': poultryID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updatePoultry.html';
            }, 30);
        });
        $(document).on('click', '.div-doc-detail', function(e) {
            e.preventDefault();
            var docID = $(this).find('#doc-id').attr('docId');
            var dataToPass = {
                'data-type': $('.dataType').val(),
                'docId': docID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateDoc.html';
            }, 30);
        });
        $(document).on('change', '#locationType', viewList.loadSample);
        $(document).on('click', '.div-sample-detail', function(e) {
            e.preventDefault();
            var sampleID = $(this).find('#sample-id').attr('sampleId');
            var clientName = $(this).find('#sample-id').attr('clientName');
            var clientId = $(this).find('#sample-id').attr('clientId');
            var dataToPass = {
                'clientType': $('#locationType').val(),
                'sampleId': sampleID,
                'clientName': clientName,
                'clientId': clientId
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateSample.html';
            }, 30);
        });
        $(document).on('click', '.div-mark-order-detail', function(e) {
            e.preventDefault();
            var orderID = $(this).find('#mark-order-id').attr('orderId');
            var dataToPass = {
                'orderID': orderID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateMarketingOrder.html';
            }, 30);
        });
        $(document).on('click', '.div-comp-order-detail', function(e) {
            e.preventDefault();
            var orderID = $(this).find('#comp-order-id').attr('orderId');
            var dataToPass = {
                'orderID': orderID
            };
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './updateCompanyOrder.html';
            }, 30);
        });
        $(document).on('input', '#distNameSearch', function () {
          var value     = $(this).val();
          var ob        = $(this);
          var optionSet = '#'+$(this).attr('list');
          var dist_id   = '';

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
                  dist_id = $(this).attr('dist_id');
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
           viewList.loadDistDetail(dist_id);
        });
        $(document).on('click', '.div-dist-stock-detail', function(e) {
            e.preventDefault();
            var stockId         = $(this).find('#dist-id').attr('stockId');
            var distName        = $(this).find('#dist-id').attr('distName');
            var distId          = $(this).find('#dist-id').attr('distID');
            var evaluation_date = $(this).find('#dist-id').attr('date');
            var image_url       = $(this).parent().find('.dist-stock-img').attr('src');
            var dataToPass      = {
                'stockId'        : stockId,
                'distId'         : distId,
                'distName'       : distName,
                'image_url'      : image_url,
                'evaluation_date': evaluation_date
            };
            console.log(dataToPass);
            window.localStorage.setItem("dataToPass", "");
            window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
            setTimeout(function() {
                window.location.href = './distStockDetail.html';
            }, 30);
        });
    },
    // method that show loading module
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    // method that remove loading module
    removeLoading: function() {
        $('.loading').remove();
    },
    typeSelect: function(e) {
        e.preventDefault();
        var selectValue = $(this).val();
        // Switch case for selecting data type and calling approprate method
        switch(selectValue) {
            case '1':
                viewList.loadDist();
                break;
            case '2':
                viewList.loadRet();
                break;
            case '3':
                viewList.loadFar();
                break;
            case '4':
                viewList.loadTec();
                break;
            case '5':
                viewList.loadCattle();
                break;
            case '6':
                viewList.loadPoultry();
                break;
            case '7':
                viewList.loadDoc();
                break;
            case '8':
                var templete = '<div class="div-client-type">'+
                                    '<select id="locationType" class="form-control">'+
                                        '<option>-CLIENT TYPE-</option>'+
                                        '<option value="1">Distributor</option>'+
                                        '<option value="2">Retailer</option>'+
                                        '<option value="3">Farmer</option>'+
                                        '<option value="4">Cattle Farmer</option>'+
                                        '<option value="5">Poultry Farmer</option>'+
                                        '<option value="6">Technican</option>'+
                                        '<option value="7">Doctor</option>'+
                                    '</select>'+
                                '</div>';
                $('.div-data-list').empty();
                $('.div-data-list').append(templete);
                break;
            case '9':
                viewList.loadCompOrder();
                break;
            case '10':
                viewList.loadMarkOrder();
                break;
            case '11':
                viewList.loadDistStock();
                break;
            default:
        }
    },
    loadDist: function() {
        viewList.addLoading();
        var tokenInformations = viewList.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
                viewList.globalAccessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken: accessToken
            };
            var path = viewList.ip + '?r=order-handler/dest-list';
            var result = viewList.ajaxCall(path, values);

            result.then(function(data) {
                viewList.removeLoading();
                // console.log(data);
                if (data === 2) {
                    window.location.href = "../index.html";
                }
                console.log(data);
                var templete = '<div class="div-dist-list div-list list-group">';
                for(var i = 0; i < data.length; i++) {
                    templete += '<div class="div-dist-list div-list list-group-item">'+
                    '<div class="div-dist-img div-img">'+
                    '<img class="dist-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                    '</div>'+
                    '<div class="div-dist-detail div-detail">'+
                    '<span id="dist-id" distId="'+data[i]['id']+'"></span>'+
                    data[i]['dist_name']+
                    '</div>'+
                    '</div>';
                }
                templete += '</div>';
                $('.div-data-list').empty();
                $('.div-data-list').append(templete);
                $("img.lazy").lazyload({
                    threshold: 200
                });
            }).catch(function(error) {
                viewList.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    viewList.loadDist,
                    'Error',
                    'TRY AGAIN'
                );
            });
        }).fail(function(error) {
            viewList.removeLoading();
            console.error(error);
        });
    },
    loadRet: function() {
        viewList.addLoading();
        var values = {
              accessToken: viewList.globalAccessToken
        };
        var path = viewList.ip + '?r=order-handler/ret-list';
        var result = viewList.ajaxCall(path, values);
        result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            var templete = '<div class="div-ret-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                templete += '<div class="div-ret-list div-list list-group-item">'+
                '<div class="div-ret-img div-img">'+
                '<img class="ret-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-ret-detail div-detail">'+
                '<span id="ret-id" retId="'+data[i]['id']+'"></span>'+
                data[i]['retail_name']+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').empty();
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadRet,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadFar: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken
          };
          var path = viewList.ip + '?r=planning/far-list';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            var templete = '<div class="div-far-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                templete += '<div class="div-far-list div-list list-group-item">'+
                '<div class="div-far-img div-img">'+
                '<img class="far-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-far-detail div-detail">'+
                '<span id="far-id" farId="'+data[i]['id']+'"></span>'+
                data[i]['name']+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').empty();
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadFar,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadTec: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken
          };
          var path = viewList.ip + '?r=planning/tec-list';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            var templete = '<div class="div-tec-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                templete += '<div class="div-tec-list div-list list-group-item">'+
                '<div class="div-tec-img div-img">'+
                '<img class="tec-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-tec-detail div-detail">'+
                '<span id="tec-id" tecId="'+data[i]['id']+'"></span>'+
                data[i]['name']+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').empty();
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadTec,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadCattle: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken
          };
          var path = viewList.ip + '?r=planning/cattle-list';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            var templete = '<div class="div-cattle-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                templete += '<div class="div-cattle-list div-list list-group-item">'+
                '<div class="div-cattle-img div-img">'+
                '<img class="cattle-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-cattle-detail div-detail">'+
                '<span id="cattle-id" cattleId="'+data[i]['id']+'"></span>'+
                data[i]['name']+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').empty();
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadTec,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadPoultry: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken
          };
          var path = viewList.ip + '?r=planning/poultry-list';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            var templete = '<div class="div-poultry-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                templete += '<div class="div-poultry-list div-list list-group-item">'+
                '<div class="div-poultry-img div-img">'+
                '<img class="poultry-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-poultry-detail div-detail">'+
                '<span id="poultry-id" poultryID="'+data[i]['id']+'"></span>'+
                data[i]['name']+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').empty();
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadTec,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadDoc: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken
          };
          var path = viewList.ip + '?r=planning/doc-list';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            var templete = '<div class="div-doc-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                templete += '<div class="div-doc-list div-list list-group-item">'+
                '<div class="div-doc-img div-img">'+
                '<img class="doc-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-doc-detail div-detail">'+
                '<span id="doc-id" docID="'+data[i]['id']+'"></span>'+
                data[i]['name']+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').empty();
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadDoc,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadSample: function() {
        viewList.addLoading();
        var clientType = $('#locationType').val();
          var values = {
              accessToken: viewList.globalAccessToken,
              clientType: clientType
          };
          var path = viewList.ip + '?r=update-sample/sample-info';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data.status === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            $('.sample').remove();
            var templete = '<div class="sample"><div class="div-sample-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                var d    = new Date(parseInt(data[i]['sample_time'])*1000);
                var date = d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate();
                var time = '';
                if(d.getHours() > 12) {
                    time = (d.getHours() - 12) + ':' + d.getMinutes() + ':' + d.getSeconds()+'&nbsp;&nbsp; PM';
                } else {
                    time = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()+'&nbsp;&nbsp; AM';
                }
                templete += '<div class="div-sample-list div-list list-group-item">'+
                '<div class="div-sample-img div-img">'+
                '<img class="sample-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-sample-detail div-detail">'+
                '<span id="sample-id" clientId="'+data[i]['client_id']+'" clientName="'+data[i]['name']+'" sampleID="'+data[i]['sample_id']+'"></span>'+
                data[i]['name']+'</br>'+
                date +'&nbsp;&nbsp;&nbsp;'+
                time+
                '</div>'+
                '</div></div>';
            }
            templete += '</div>';
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadSample,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadMarkOrder: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken,
          };
          var path = viewList.ip + '?r=update-marketing-order/marketing-order-info';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data.status === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            $('.div-data-list').empty();
            var templete = '<div class="div-mark-order-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                var d    = new Date(parseInt(data[i]['order_date'])*1000);
                var date = d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate();
                var time = '';
                if(d.getHours() > 12) {
                    time = (d.getHours() - 12) + ':' + d.getMinutes() + ':' + d.getSeconds()+'&nbsp;&nbsp; PM';
                } else {
                    time = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()+'&nbsp;&nbsp; AM';
                }
                templete += '<div class="div-mark-order-list div-list list-group-item">'+
                '<div class="div-mark-order-img div-img">'+
                '<img class="mark-order-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-mark-order-detail div-detail">'+
                '<span id="mark-order-id" orderId="'+data[i]['order_id']+'"></span>'+
                data[i]['ret_name']+'</br>'+
                date +'&nbsp;&nbsp;&nbsp;'+
                time+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadSample,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadCompOrder: function() {
        viewList.addLoading();
          var values = {
              accessToken: viewList.globalAccessToken,
          };
          var path = viewList.ip + '?r=update-company-order/company-order-info';
          var result = viewList.ajaxCall(path, values);
          result.then(function(data) {
            viewList.removeLoading();
            // console.log(data);
            if (data.status === 2) {
                window.location.href = "../index.html";
            }
            console.log(data);
            $('.div-data-list').empty();
            var templete = '<div class="div-comp-order-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                var d    = new Date(parseInt(data[i]['order_date'])*1000);
                var date = d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate();
                var time = '';
                if(d.getHours() > 12) {
                    time = (d.getHours() - 12) + ':' + d.getMinutes() + ':' + d.getSeconds()+'&nbsp;&nbsp; PM';
                } else {
                    time = d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds()+'&nbsp;&nbsp; AM';
                }
                templete += '<div class="div-comp-order-list div-list list-group-item">'+
                '<div class="div-comp-order-img div-img">'+
                '<img class="comp-order-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-comp-order-detail div-detail">'+
                '<span id="comp-order-id" orderId="'+data[i]['order_id']+'"></span>'+
                data[i]['dist_name']+'</br>'+
                date +'&nbsp;&nbsp;&nbsp;'+
                time+
                '</div>'+
                '</div>';
            }
            templete += '</div>';
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
        }).catch(function(error) {
            viewList.removeLoading();
            navigator.notification.alert(
                ' There is error in network',
                viewList.loadSample,
                'Error',
                'TRY AGAIN'
            );
        });
    },
    loadDistStock: function() {
        viewList.addLoading();

          var values = {
              accessToken: viewList.globalAccessToken
          };
          var path = viewList.ip + '?r=order-handler/dest-list';
          var result = viewList.ajaxCall(path, values);
              result.then(function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  viewList.removeLoading();
                  var opt = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      opt += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                var templete = '<div class="div-dist-selector"><div class="form-group">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList" placeholder="Select distributor">'+
                            '<datalist id="distList">'+opt+'</datalist>'+            
                        '</div></div>';
                $('.div-data-list').empty();
                $('.div-data-list').append(templete);
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  viewList.loadDistStock,
                  'Error',
                  'OK'
              );
          });
    },
    loadDistDetail: function(dist_id) {
        viewList.addLoading();

          var values = {
              accessToken: viewList.globalAccessToken,
              dist_id: dist_id
          };
          var path = viewList.ip + '?r=display-dist-stock/stock-info';
          var result = viewList.ajaxCall(path, values);
              result.then(function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  $('.div-dist-stock-list-container').remove();
            var templete = '<div class="div-dist-stock-list-container"><div class="div-dist-stock-list div-list list-group">';
            for(var i = 0; i < data.length; i++) {
                var d    = new Date(parseInt(data[i]['evaluation_date'])*1000);
                var date = d.getFullYear() + '-' + (d.getMonth()+1) + '-' + d.getDate();
                templete += '<div class="div-dist-stock-list div-list list-group-item">'+
                '<div class="div-dist-img div-img">'+
                '<img class="dist-stock-img img lazy" data-original="'+viewList.imgPath+data[i]['image_url']+'">'+
                '</div>'+
                '<div class="div-dist-stock-detail div-detail">'+
                '<span id="dist-id" date="'+date+'" stockId="'+data[i]['stock_id']+'" distName="'+data[i]['dist_name']+'" distID="'+data[i]['dist_id']+'"></span>'+
                data[i]['dist_name']+'</br>'+
                date
                '</div>'+
                '</div></div>';
            }
            templete += '</div>';
            $('.div-data-list').append(templete);
            $("img.lazy").lazyload({
                threshold: 200
            });
                  viewList.removeLoading();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  viewList.loadDistDetail,
                  'Error',
                  'OK'
              );
          });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    }
}
viewList.initialize();