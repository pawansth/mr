var updateCompanyOrder = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    prodData: null,
    globalAccessToken: null,
    orderId: null,

    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateCompanyOrder.ip      = new ip();
        updateCompanyOrder.ip      = updateCompanyOrder.ip.remoteIpAddress();
        updateCompanyOrder.SQLLObj = new SQLiteLogin();
        updateCompanyOrder.ctr     = new controller();

        var passedData          = window.localStorage.getItem('dataToPass');
        var dataPassed          = JSON.parse(passedData);
        updateCompanyOrder.orderId   = dataPassed.orderID;
        
        updateCompanyOrder.addLoading();
        updateCompanyOrder.loadProd();
        updateCompanyOrder.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '.addProd', updateCompanyOrder.addProdTemplete);
        $(document).on('click', '.minusProd', updateCompanyOrder.removeProdTemplete);
        $(document).on('click', '#btn-order-update', updateCompanyOrder.chick);
        $(document).on('focusout', '#productName', function() {
            if($('#productName').val().trim() === '') {
                $('#productName').attr('prodId', '');
                $('#productName').attr('baseUnitId', '');
                $('#productName').attr('price', '');
            }
            updateCompanyOrder.calculateAmount($(this).parent().parent());
        });
        $(document).on('focusout', '#productUnit', function() {
            if($('#productUnit').val().trim() === '') {
                $('#productUnit').attr('prodUnitId', '');
            }
            updateCompanyOrder.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('focusout', '#productQty', function() {
            updateCompanyOrder.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        /* Tracking the selected option from datalist for retailer */
        $("#retNameSearch").on('input', function () {
          var value = $('#retNameSearch').val();

          $('#retList option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $('#retNameSearch').attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('input', '#productName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var parent = $(this).parent().parent();
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodId',$(this).attr('prod_id'));
               $(ob).attr('baseUnitId',$(this).attr('base_unit_id'));
               $(ob).attr('price',$(this).attr('base_price'));
               updateCompanyOrder.addLoading();
               $('.loadingText').text('Loading units...');
               updateCompanyOrder.searchCrosProdUnit(parent);
             }
           });
        });
        $(document).on('input', '#productUnit', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodUnitId',$(this).attr('unit_id'));
               return;
             }
           });
        });
        $(document).on('click', '#estDelivaryDate', updateCompanyOrder.showDatePicker);
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loadProd: function() {
      var tokenInformations = updateCompanyOrder.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateCompanyOrder.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }
          // console.log(accessToken);
          var values = {
              accessToken: accessToken
          };
          var path = updateCompanyOrder.ip + '?r=order-handler/prod-list';
          var result = updateCompanyOrder.ajaxCall(path, values);
            result.then(function(data) {
                // console.log(data);
                if (data === 2) {
                    window.location.href = "../index.html";
                }
                updateCompanyOrder.prodData = data;
                var templete = '';

                $.each(data, function() {
                  // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                    templete += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                });
                // templete += '</ul>';
                $('#prodList').append(templete);
                updateCompanyOrder.loadCompanyOrder();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateCompanyOrder.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    loadCompanyOrder: function() {
          var values = {
              accessToken: updateCompanyOrder.globalAccessToken,
              id: updateCompanyOrder.orderId
          };
          var path = updateCompanyOrder.ip + '?r=update-company-order/company-order-detail';
          var result = updateCompanyOrder.ajaxCall(path, values);
          result.then(function(data) {
              console.log(data);
              if(data.status === 2) {
                  window.location.href = "../index.html";
              }
              var d = new Date(parseInt(data[0].estimated_delevery_date)*1000);
              var date = d.getFullYear() +'-'+ (d.getMonth() +1) +'-'+ d.getDate();
              $('#distNameSearch').val(data[0].dist_name);
              $('#distNameSearch').attr('distId',data[0].dist_id);
              $('#poId').val(data[0].po_id);
              $('#estDelivaryDate').val(date);
              $('#order_desc').val(data[0].order_desc);
              $('.orderProductList').empty();
              for(var i = 0; i<data[1].length; i++) {
                  updateCompanyOrder.unitInit(data[1][i]); 
              }
              updateCompanyOrder.removeLoading();
          }).catch(function(error) {
              console.error(error);
              updateCompanyOrder.removeLoading();
              navigator.notification.alert(
                  ' There is error in network',
                  updateCompanyOrder.loadCompanyOrder,
                  'Error',
                  'Try Again'
              );
          });
    },
    unitInit: function(orderData) {
        var order = orderData;
        var values = {
                    accessToken: updateCompanyOrder.globalAccessToken,
                    crossProductId: order.product_id
                };
                var path = updateCompanyOrder.ip + '?r=order-handler/search-cross-unit';
                var result = updateCompanyOrder.ajaxCall(path, values);
                result.then(function(data) {
                    var unitName = '';
                    for(var j = 0; j<data.length; j++) {
                        if(order.unit === data[j].id) {
                            unitName = data[j].name;
                        }
                    }
                    var temp = '';
                  
                    $.each(data, function() {
                        temp += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                    });
                    var prodDataTemp = '';
                    $.each(updateCompanyOrder.prodData, function() {
                        prodDataTemp += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                    });
                    var templete = '<div class="orderProduct">'+
                                        '<div class="">'+
                                            '<input type="text" class="form-control" id="productName" value="'+order.product_name+'" prodId="'+order.product_id+'" baseUnitId="'+order.base_unit+'" price="'+order.base_price+'" rowId="'+order.id+'" list="prodList" placeholder="product">'+
                                            '<datalist id="prodList">'+prodDataTemp+'</datalist>'+
                                        '</div>'+
                                        '<div class="row add-top-margin">'+
                                            '<div class="col-xs-4 nopadding-right">'+
                                                '<input type="text" class="form-control" id="productUnit" prodUnitId="'+order.product_unit+'" value="'+order.unit_name+'" list="unitList" placeholder="unit">'+
                                                '<datalist id="unitList">'+temp+'</datalist>'+
                                            '</div>'+
                                            '<div class="col-xs-2 nopadding-left nopadding-right">'+
                                                '<input type="number" class="form-control" id="productQty" value="'+order.product_qty+'" placeholder="qty">'+
                                            '</div>'+
                                            '<div class="col-xs-3 nopadding-left">'+
                                                '<input type="text" class="form-control" id="productAmount" value="'+order.est_price+'" placeholder="NPR">'+
                                            '</div>'+
                                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                                        '</div>'+
                                    '</div>';
                    $('.orderProductList').append(templete);
                    $('.orderProduct').css({'margin-top':'5px', 'margin-bottom':'5px'});
                }).catch(function(error) {
                    navigator.notification.alert(
                        'Network error occure. Please try again',
                        updateCompanyOrder.unitInit,
                        'ERROR',
                        'Try Again'
                    );
                });
    },
    /* searching corresponding product unit */
    searchCrosProdUnit: function(currentObj) {
      crossProductId = currentObj.find('#productName').attr('prodId');

          var values = {
              accessToken: updateCompanyOrder.globalAccessToken,
              crossProductId: crossProductId
          };
          var path = updateCompanyOrder.ip + '?r=order-handler/search-cross-unit';
          var result = updateCompanyOrder.ajaxCall(path, values);

            result.then(function(data) {
                if (data === 2) {
                    window.location.href = "../index.html";
                }
                var templete = '';
                
                $.each(data, function() {
                    templete += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                });
                console.log(currentObj.find('#unitList'));
                currentObj.find('#unitList').append(templete);
                updateCompanyOrder.removeLoading();
          }).catch(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateCompanyOrder.searchCrosProdUnit,
                  'Error',
                  'Try Again'
              );
          });
    },
    addProdTemplete: function() {
        $(this).parent().parent().css('margin-bottom','5px');
        var templete = '';
        templete += '<div class="orderProduct">'+
                        '<div class="">'+
                            '<input type="text" class="form-control" id="productName" prodId="" baseUnitId="" price="" rowId="0" list="prodList" placeholder="product">'+
                            '<datalist id="prodList"></datalist>'+
                        '</div>'+
                        '<div class="row add-top-margin">'+
                            '<div class="col-xs-4 nopadding-right">'+
                                '<input type="text" class="form-control" id="productUnit" prodUnitId="" list="unitList" placeholder="unit">'+
                                '<datalist id="unitList"></datalist>'+
                            '</div>'+
                            '<div class="col-xs-2 nopadding-left nopadding-right">'+
                                '<input type="number" class="form-control" id="productQty" placeholder="qty">'+
                            '</div>'+
                            '<div class="col-xs-3 nopadding-left">'+
                                '<input type="text" class="form-control" id="productAmount" placeholder="NPR">'+
                            '</div>'+
                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                            '<button type="button" class="btn btn-warning floating-minus minusProd glyphicon glyphicon-remove col-xs-2"></button>'+
                        '</div>'+
                    '</div>';
        $('.orderProductList').append(templete);
    },
    removeProdTemplete: function() {
        $(this).parent().parent().remove();
    },
    calculateAmount: function(parentObj) {
        if(!(parentObj.find('#productName').val().trim() === '' || parentObj.find('#productName').attr('prodId').trim() === '')) {
            if(!(parentObj.find('#productUnit').val().trim() === '' || parentObj.find('#productUnit').attr('prodUnitId').trim() === '')) {
                if(!(parentObj.find('#productQty').val().trim() === '')) {
                    var tokenInformations = updateCompanyOrder.SQLLObj.pullToken();
                    tokenInformations.done(function(informations) {
                        var accessToken = '';
                        for (var idx in informations) {
                            var information = informations[idx];
                            //alert(information.name);
                            accessToken = information.token;
                        }
                        if (accessToken.trim() === '') {
                            console.log('there is no accessToken');
                            window.location.href = "../index.html";
                            return;
                        }

                        // console.log(accessToken);

                        var values = {
                            accessToken: accessToken,
                            productId  : parentObj.find('#productName').attr('prodId'),
                            uid        : parentObj.find('#productUnit').attr('prodUnitId'),
                            base_uid   : parentObj.find('#productName').attr('baseUnitId'),
                            qtyToChange: parentObj.find('#productQty').val()
                        };
                        updateCompanyOrder.addLoading();
                        $('.loadingText').text('calcuation amount');
                    $.ajax({
                        url: updateCompanyOrder.ip + '?r=major-dist-plan/unit-to-price',
                        method: "POST",
                        dataType: 'json',
                        data: values,

                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                            navigator.notification.alert(
                            'There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                        },

                        success: function(data) {
                            updateCompanyOrder.removeLoading();
                            if (data === 2) {
                                window.location.href = "../index.html";
                            }
                            parentObj.find('#productAmount').val(data);
                        }
                    }).fail(function(error) {
                        console.error(error);
                        updateCompanyOrder.removeLoading();
                        navigator.notification.alert(
                            ' There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                    });
                    }).fail(function(error) {
                        console.error(error);
                    });
                }
            }
        }
        return;
    },
    showDatePicker: function() {
      var options = {
          date: new Date(),
          minDate: Date.parse(new Date()),
          mode: 'date',
          windowTitle: 'Estimate Delivary Date'
        };

        datePicker.show(options, function(d){
          var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
          $('#estDelivaryDate').val(datestring);
        });
    },
    chick: function(e) {
        e.preventDefault();
        var count = 1;
        var eachStatus = false;
        $.each($('.orderProduct'), function() {
            if($(this).find('#productName').val().trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productName').attr('prodId').trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' should be select form list of give product.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').val().trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').attr('prodUnitId').trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' should be select form list of given unit list.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productQty').val().trim() === '') {
                navigator.notification.alert(
                    'Product quantity in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            count = count + 1;
        });
        if(eachStatus === true) {
            eachStatus = false;
            return;
        }
        updateCompanyOrder.addLoading();
        $('.loadingText').text('Updating....')
        updateCompanyOrder.editOrder();
    },
    editOrder: function() {
        var productValues = [];
        var poId = $('#poId').val();
        var est_delevary_date = new Date($('#estDelivaryDate').val()).getTime() / 1000;
        var desc = $('#order_desc').val();
        // gettering infor about order 
        var customeInfo = {
            poId             : poId,
            est_delevary_date: est_delevary_date,
            desc             : desc
        };
        // gettering all order product info in productValues array
        var rowId        = '';
        var productId    = '';
        var productQty   = '';
        var productUnit  = '';
        var productAmount = '';

        $.each($('.orderProduct'), function() {
            rowId         = $(this).find('#productName').attr('rowId');
            productId     = $(this).find('#productName').attr('prodId');
            productQty    = $(this).find('#productQty').val();
            productUnit   = $(this).find('#productUnit').attr('prodUnitId');
            productAmount = $(this).find('#productAmount').val();
            var data = {
                rowId        : rowId,
                productId    : productId,
                productQty   : productQty,
                productUnit  : productUnit,
                productAmount: productAmount
            };
            // pushing json formate data to array
            productValues.push(data);
        });

            var values = {
                id           : updateCompanyOrder.orderId,
                accessToken  : updateCompanyOrder.globalAccessToken,
                customeInfo  : customeInfo,
                productValues: productValues
            };
            var path = updateCompanyOrder.ip + '?r=update-company-order/make-update';
            var result = updateCompanyOrder.ajaxCall(path,values);
            result.then(function(data) {
                if (data.status === 2) {
                    window.location.href = "../index.html";
                }
                if(data.status === 1) {
                    updateCompanyOrder.loadCompanyOrder();
                }
                updateCompanyOrder.removeLoading();
                navigator.notification.alert(
                    data.msgBody,
                    null,
                    data.msgTitle,
                    'OK'
                );
            }).catch(function(error) {
                console.error(error);
                updateCompanyOrder.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    }
}
updateCompanyOrder.initialize();