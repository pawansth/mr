var updateSample = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    prodData: null,
    clientId: null,
    sampleId: null,
    clientType: null,
    clientName: null,
    editIndex: 0,
    globalAccessToken: null,
    prodToDisplay: [],

    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        updateSample.ip      = new ip();
        updateSample.ip      = updateSample.ip.remoteIpAddress();
        updateSample.SQLLObj = new SQLiteLogin();
        updateSample.ctr     = new controller();

        var passedData          = window.localStorage.getItem('dataToPass');
        var dataPassed          = JSON.parse(passedData);
        updateSample.sampleId   = dataPassed.sampleId;
        updateSample.clientId   = dataPassed.clientId;
        updateSample.clientType = dataPassed.clientType;
        updateSample.clientName = dataPassed.clientName;
        $('#clientNameSearch').val(updateSample.clientName);

        updateSample.bindEvents();
        updateSample.addLoading();
        updateSample.loadProd();
    },
    bindEvents: function() {
        $(document).on('click', '.addProd', function() {
            if(!(updateSample.editIndex === 0)) {
                updateSample.addProdTemplete();
            }
        });
        $(document).on('click', '.minusProd', function() {
            var obj = $(this);
            if(!(updateSample.editIndex === 0)) {
                updateSample.removeProdTemplete(obj);
            }
        });
        $(document).on('click', '.btn-sample', updateSample.chick);
        $(document).on('focusout', '#productName', function() {
            if($('#productName').val().trim() === '') {
                $('#productName').attr('prodId', '');
                $('#productName').attr('baseUnitId', '');
                $('#productName').attr('price', '');
            }
        });
        $(document).on('focusout', '#productUnit', function() {
            if($('#productUnit').val().trim() === '') {
                $('#productUnit').attr('prodUnitId', '');
            }
        });
        $(document).on('input', '#productName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var parent = $(this).parent().parent();
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodId',$(this).attr('prod_id'));
               $(ob).attr('baseUnitId',$(this).attr('base_unit_id'));
               $(ob).attr('price',$(this).attr('base_price'));
               updateSample.addLoading();
               $('.loadingText').text('Loading units...');
               updateSample.searchCrosProdUnit(parent);
             }
           });
        });
        $(document).on('input', '#productUnit', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodUnitId',$(this).attr('unit_id'));
               return;
             }
           });
        });
        $(document).on('click', '#btn-edit', function() {
            $('.title').text('Edit Sample');
            updateSample.edit();
        });
        updateSample.noEdit();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loadProd: function() {
      var tokenInformations = updateSample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              updateSample.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: updateSample.ip + '?r=order-handler/prod-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateSample.prodData = data;
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                  });
                  // templete += '</ul>';
                  $('#prodList').append(templete);
                  updateSample.loadSample();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateSample.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method to load old sample to edit
    loadSample: function() {
        var values = {
            accessToken: updateSample.globalAccessToken,
            id         : updateSample.sampleId
        };
        var path = updateSample.ip + '?r=update-sample/sample-detail';
        var result = updateSample.ajaxCall(path, values);
        result.then(function(data) {
            if(data.status === 2) {
                window.location.href = '../index.html';
            }
            updateSample.removeLoading();
            $('#clientNameSearch').val(updateSample.clientName);
            $('#desc').val(data[0].description);
            // code to display dist data
            updateSample.prodToDisplay = [];
            for(var i = 0; i<updateSample.prodData.length; i++) {
                for(var j = 0; j<data[1].length; j++) {
                    if(updateSample.prodData[i].id === data[1][j].product_id) {
                        var presentData = {
                            rowId  : data[1][j].id,
                            product_id: data[1][j].product_id,
                            unit: data[1][j].unit_id,
                            qty: data[1][j].qty,
                            name: updateSample.prodData[i].product_name,
                            base_unit_id: updateSample.prodData[i].bas_unit,
                            bas_price: updateSample.prodData[i].bas_price
                        };
                        updateSample.prodToDisplay.push(presentData);
                    }
                }
            }
            $('.planProductList').empty();
            for(var i = 0; i<updateSample.prodToDisplay.length; i++) {
                updateSample.unitInit(updateSample.prodToDisplay[i].product_id, i);
            }
            updateSample.removeLoading();
        }).catch(function(error) {
            console.error(error);
            navigator.notification.alert(
                ' There is error in network',
                updateSample.loadFar,
                'Error',
                'Try Again'
            );
        });
    },
    unitInit: function(prodId, indx) {
        var prodId = prodId;
        var indx   = indx;
        var values = {
                    accessToken: updateSample.globalAccessToken,
                    crossProductId: prodId
                };
                var path = updateSample.ip + '?r=order-handler/search-cross-unit';
                var result = updateSample.ajaxCall(path, values);
                result.then(function(data) {
                    var unitName = '';
                    for(var j = 0; j<data.length; j++) {
                        if(updateSample.prodToDisplay[indx].unit === data[j].id) {
                            unitName = data[j].name;
                        }
                    }
                    var temp = '';
                  
                    $.each(data, function() {
                        temp += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                    });
                    var prodDataTemp = '';
                    $.each(updateSample.prodData, function() {
                        prodDataTemp += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                    });
                    var templete = '<div class="planProduct">'+
                        '<div class="">'+
                            '<input type="text" class="form-control" id="productName" value="'+updateSample.prodToDisplay[indx].name+'" prodId="'+updateSample.prodToDisplay[indx].product_id+'" baseUnitId="'+updateSample.prodToDisplay[indx].base_unit_id+'" price="'+updateSample.prodToDisplay[indx].bas_price+'" rowId="'+updateSample.prodToDisplay[indx].rowId+'" disabled list="prodList" placeholder="product">'+
                            '<datalist id="prodList">'+prodDataTemp+'</datalist>'+
                        '</div>'+
                        '<div class="row add-top-margin">'+
                            '<div class="col-xs-5 nopadding-right">'+
                                '<input type="text" class="form-control" id="productUnit" value="'+unitName+'" prodUnitId="'+updateSample.prodToDisplay[indx].unit+'" list="unitList" disabled placeholder="unit">'+
                                '<datalist id="unitList">'+temp+'</datalist>'+
                            '</div>'+
                            '<div class="col-xs-4 nopadding-left">'+
                                '<input type="number" class="form-control" id="productQty" value="'+updateSample.prodToDisplay[indx].qty+'" disabled placeholder="qty">'+
                            '</div>'+
                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                        '</div>'+
                    '</div>';
                    $('.planProductList').append(templete);
                    $('.planProduct').css({'margin-top':'5px', 'margin-bottom':'5px'});
                }).catch(function(error) {
                    navigator.notification.alert(
                        'Network error occure. Please try again',
                        updateSample.unitInit,
                        'ERROR',
                        'Try Again'
                    );
                });
    },
    /* searching corresponding product unit */
    searchCrosProdUnit: function(currentObj) {
      crossProductId = currentObj.find('#productName').attr('prodId');
    //   alert(crossProductId);
      //  Pulling access token form sqlite database
      var tokenInformations = updateSample.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken,
              crossProductId: crossProductId
          };

          $.ajax({
              url: updateSample.ip + '?r=order-handler/search-cross-unit',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  console.log(data);
                  
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  updateSample.crossUnit = data;

                  var templete = '';
                  
                  $.each(data, function() {
                      templete += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                  });
                  console.log(currentObj.find('#unitList'));
                  currentObj.find('#unitList').append(templete);
                  updateSample.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  updateSample.searchCrosProdUnit,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addProdTemplete: function() {
        $(this).parent().parent().css('margin-bottom','5px');
        var templete = '';
        templete += '<div class="planProduct">'+
                        '<div class="">'+
                            '<input type="text" class="form-control" id="productName" prodId="" baseUnitId="" price="" rowId="0" list="prodList" placeholder="product">'+
                            '<datalist id="prodList"></datalist>'+
                        '</div>'+
                        '<div class="row add-top-margin">'+
                            '<div class="col-xs-5 nopadding-right">'+
                                '<input type="text" class="form-control" id="productUnit" prodUnitId="" list="unitList" placeholder="unit">'+
                                '<datalist id="unitList"></datalist>'+
                            '</div>'+
                            '<div class="col-xs-4 nopadding-left">'+
                                '<input type="number" class="form-control" id="productQty" placeholder="qty">'+
                            '</div>'+
                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                            '<button type="button" class="btn btn-warning floating-minus minusProd glyphicon glyphicon-remove col-xs-2"></button>'+
                        '</div>'+
                    '</div>';
        $('.planProductList').append(templete);
    },
    removeProdTemplete: function(obj) {
        obj.parent().parent().remove();
    },
    chick: function() {
        if($('#clientNameSearch').val().trim() === '') {
            navigator.notification.alert(
                'Client name can not be empty',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        var count = 1;
        var eachStatus = false;
        $.each($('.planProduct'), function() {
            if($(this).find('#productName').val().trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productName').attr('prodId').trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' should be select form list of give product.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').val().trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').attr('prodUnitId').trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' should be select form list of given unit list.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productQty').val().trim() === '') {
                navigator.notification.alert(
                    'Product quantity in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            count = count + 1;
        });
        if(eachStatus === true) {
            eachStatus = false;
            return;
        }
        updateSample.addLoading();
        $('.loadingText').text('updating...')
        updateSample.submitupdateSample();
    },
    submitupdateSample: function() {
        var productValues = [];
        var clientType  = updateSample.clientType;
        var clientId    = updateSample.clientId;
        var description = $('#desc').val();

        var clientInfo = {
            clientType : clientType,
            clientId   : clientId,
            description: description
        };
        var productId    = '';
        var productQty   = '';
        var productUnit  = '';

        $.each($('.planProduct'), function() {
            id            = $(this).find('#productName').attr('rowId');
            productId     = $(this).find('#productName').attr('prodId');
            productQty    = $(this).find('#productQty').val();
            productUnit   = $(this).find('#productUnit').attr('prodUnitId');
            var data = {
                id           : id,
                productId    : productId,
                productQty   : productQty,
                productUnit  : productUnit
            };
            // pushing json formate data to array
            productValues.push(data);
        });
        console.log(productValues);
        // sending data to server using ajax
            var values = {
                id           : updateSample.sampleId,
                accessToken  : updateSample.globalAccessToken,
                clientInfo   : clientInfo,
                productValues: productValues
            };
            var path   = updateSample.ip + '?r=update-sample/make-update';
            var result = updateSample.ajaxCall(path, values);
            result.then(function(data) {
                // console.log(data);
                if (data.status === 1) {
                    updateSample.loadSample();
                    updateSample.noEdit();
                    navigator.notification.alert(
                        data.msgBody,
                        null,
                        'data.msgTitle',
                        'OK'
                    );
                    $('.title').text('Sample Detail');
                    updateSample.removeLoading();
                } else if (data.status === 2) {
                    window.location.href = "../index.html";
                }
            }).catch(function(error) {
                console.error(error);
                updateSample.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    },
    noEdit: function() {
        $('#desc').attr('disabled', true);
        $('.btn-sample').attr('disabled', true);
    },
    edit: function() {
        updateSample.editIndex = 1;
        $('#desc').attr('disabled', false);
        $('.btn-sample').attr('disabled', false);
        $.each($('.planProduct'), function() {
            $(this).find('#productName').attr('disabled', false);
            $(this).find('#productUnit').attr('disabled', false);
            $(this).find('#productQty').attr('disabled', false);
        });
    }
}
updateSample.initialize();