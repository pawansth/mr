var camera = function() {
    var cam = {
        openDefaultCamera: function(selection) {
            var srcType  = Camera.PictureSourceType.CAMERA;
            var destType = Camera.DestinationType.FILE_URI;
            var options  = cam.setOptions(srcType, destType);
            // var func = createNewFileEntry;

            if (selection == "picker-thmb") {
                // To downscale a selected image,
                // Camera.EncodingType (e.g., JPEG) must match the selected image type.
                options.targetHeight = 100;
                options.targetWidth = 100;
            }
// imageUri
            navigator.camera.getPicture(function cameraSuccess(imageData) {
                $('#photo').attr('src','');
                $('#photo').attr('src',imageData);
            }, function cameraError(error) {
                console.debug("Unable to obtain picture: " + error, "app");

            }, options);
        },
        openFilePicker: function(selection) {

            var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
            var destType = Camera.DestinationType.FILE_URI;
            var options = cam.setOptions(srcType, destType);

            if (selection == "picker-thmb") {
                // To downscale a selected image,
                // Camera.EncodingType (e.g., JPEG) must match the selected image type.
                options.targetHeight = 100;
                options.targetWidth = 100;
            }

            navigator.camera.getPicture(function cameraSuccess(imageData) {

                // Do something with image
                $('#photo').attr('src','');
                $('#photo').attr('src',imageData);

            }, function cameraError(error) {
                console.debug("Unable to obtain picture: " + error, "app");

            }, options);
            // alert(cam.imageData);
            return cam.imageData;
        },
        createNewFileEntry: function(imgUri) {
            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {

                // JPEG file
                dirEntry.getFile("tempFile.jpeg", { create: true, exclusive: false }, function (fileEntry) {

                    // Do something with it, like write to it, upload it, etc.
                    // writeFile(fileEntry, imgUri);
                    console.log("got file: " + fileEntry.fullPath);
                    // displayFileData(fileEntry.fullPath, "File copied to");

                }, onErrorCreateFile);

            }, onErrorResolveUrl);
        },
        tempClean: function() {
            navigator.camera.cleanup(onSuccess, onFail);

            function onSuccess() {
                console.log("Camera cleanup success.")
            }

            function onFail(message) {
                alert('Failed because: ' + message);
            }
        },
        setOptions: function(srcType,destType) {
            var options = {
                // Some common settings are 20, 50, and 100
                quality: 50,
                destinationType: destType,
                // In this app, dynamically set the picture source, Camera or photo gallery
                sourceType: srcType,
                encodingType: Camera.EncodingType.JPEG,
                mediaType: Camera.MediaType.PICTURE,
                allowEdit: true,
                correctOrientation: true,  //Corrects Android orientation quirks
            }
            return options;
        }
    }
    return cam;
}