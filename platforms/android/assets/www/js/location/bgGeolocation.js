var bgGeolocation = function() {
    var bgLocation = {
      ip: null,
        startInBackground: function() {
          this.ip      = new ip();
          this.ip       = this.ip.remoteIpAddress();
          this.ip     +='?r=location';

            //            document.addEventListener('deviceready', startBackgroundTrack, false);
            //        console.log('ok everything is fine');
            startBackgroundTrack(this.ip);
        },

        stopInBackground: function() {
            stopBackgroundTrack();
        }
    }
    return bgLocation;
}



function startBackgroundTrack(ip) {
  var fullAddress = ip;
    console.log('inside startBackgroundTrack function');

    // creating SQLiteLocBtnFlagForGeoloc object
    var SLBF = new SQLiteLocBtnFlag();

    //creating SQliteBridge object
    var SSDHObj            = new SQLiteStartDateHandler();
    //  creating object of SQLiteLogin class
    var SQLLObj = new SQLiteLogin();
    var SQLROObj = new SQLiteReachOrigin();
    var notificationStatus = 0;

    /**
     * This callback will be executed every time a geolocation is recorded in the background.
     */
    var callbackFn = function(location) {
        console.log('[js] BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);
        console.log('location', location);
        var dt = new Date();
        var time = dt.getHours();
        console.log('Hours is : '+ time);
        if (time >= 22) {
          //  pulling accessToken value from local database
          var tokenInformations = SQLLObj.pullToken();
          //  Feaching tooken value from tokenInformations variable that store promise value
          tokenInformations.done(function(informations) {
              var accessToken = '';
              for (var idx in informations) {
                  var information = informations[idx];
                  //alert(information.name);
                  accessToken = information.token;
              }
                var dbOrigin = [];
                var origin = SQLROObj.getOrigin();
                origin.done(function(informations) {
                    for (var idx in informations) {
                        var information = informations[idx];
                        //alert(information.name);
                        var originLoc = {
                            latitude : information.latitude,
                            longitude: information.longitude
                        };
                        dbOrigin.push(originLoc);
                    }
                    var time = location.time;
                    var destinyLoc = {
                        latitude : location.latitude,
                        longitude: location.longitude
                    };
                    // alert(new Date(time));
                    var values = {
                    accessToken: accessToken,
                    time       : time,
                    originLoc  : dbOrigin,
                    destinyLoc : destinyLoc
                    };
                    console.log('values: ',values);
                    $.ajax({
                        url: fullAddress+'/stop-location',
                        method: "POST",
                        dataType: 'json',
                        data: values,

                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                        },

                        success: function(data) {
                            if(data.status === 1) {
                                SSDHObj.deleteStartDate();
                                SQLROObj.deleteOrigin();
                                var result = SLBF.updateLocBtnFlag(0,1);
                                result.done(function() {
                                    console.log('stop button log is successfully recorded');
                                }).fail(function(error) {
                                    console.error(error);
                                });
                                // stoping background process of location tracking
                                stopBackgroundTrack();
                            } 
                        }
                    }).fail(function(error) {
                        console.error(error);
                    });
                    }).fail(function(error) {
                    console.error(error);
                });
            }).fail(function(error) {
              console.error(error);
            });

    } else {
      //  pulling accessToken value from local database
      var tokenInformations = SQLLObj.pullToken();
      //  Feaching tooken value from tokenInformations variable that store promise value
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          //  if accessToken is empty the redirect user to login page
          // if (accessToken.trim() === '') {
          //     $('.container').empty();
          //     $('.container').load('././views/login.html');
          //     return;
          // }

          var values = {
            accessToken: accessToken,
              latitude: location.latitude,
              longitude: location.longitude
          };
          console.log('not shutdown');
          $.ajax({
              method: 'POST',
              dataType: 'json',
              url: fullAddress+'/travel-locations',
              data: values,
              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                console.log('data uploaded to travilling server');
                  console.log(data);
              }
          });

        }).fail(function(error) {
          console.error(error);
        });
        if(notificationStatus === 0) {
            var informations = SSDHObj.pullStartDate();
            console.log(informations);
            informations.done(function(informations) {
                var recordedStartTime;
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    recordedStartTime = information.date_time;
                }
                var now = new Date();
                var stopTime = now.getTime();
                //subtract from stopTime to startTime to get his working hours
                var difference = stopTime - recordedStartTime;
                var workedHours = parseInt((difference) / (1000 * 60 * 60));
                if (workedHours >= 8) {
                    var notic = new notification();
                    notic.pushNotification('Job finished','Your working hours is finished. Now go to app and press stop button.');
                    notificationStatus = 1;
                } 
            }).fail(function(error) {
                console.warn(error);
            });
        }

    }

        // Do your HTTP request here to POST location to your server.
        // jQuery.post(url, JSON.stringify(location));

        /*
        IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
        and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
        IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
        */
        backgroundGeolocation.finish();
    };

    var failureFn = function(error) {
        console.log('BackgroundGeolocation error');
    };

    // BackgroundGeolocation is highly configurable. See platform specific configuration options
    backgroundGeolocation.configure(callbackFn, failureFn, {
        interval: 60000,
        desiredAccuracy: 10,
        stationaryRadius: 20,
        distanceFilter: 30
    });

    // Turn ON the background-geolocation system.  The user will be tracked whenever they suspend the app.
    backgroundGeolocation.start();

}

function stopBackgroundTrack() {

    /**
     * This callback will be executed every time a geolocation is recorded in the background.
     */
    var callbackFn = function(location) {
        console.log('[js] BackgroundGeolocation callback:  ' + location.latitude + ',' + location.longitude);

        // Do your HTTP request here to POST location to your server.
        // jQuery.post(url, JSON.stringify(location));

        /*
        IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
        and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
        IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
        */
        backgroundGeolocation.finish();
    };

    var failureFn = function(error) {
        console.log('BackgroundGeolocation error');
    };

    // BackgroundGeolocation is highly configurable. See platform specific configuration options
    backgroundGeolocation.configure(callbackFn, failureFn, {
        desiredAccuracy: 10,
        stationaryRadius: 20,
        distanceFilter: 30,
        interval: 1000
    });

    // If you wish to turn OFF background-tracking, call the #stop method.
    backgroundGeolocation.stop();
}
