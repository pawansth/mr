
var distributor = {
  SQLLObj: null,
  SQLAObj: null,
  ip     : null,
  ctr    : null,
  locSet : null,
  cam    : null,

  initialize: function() {
    $('#google-map').hide();
    screen.orientation.unlock();
    // Lock orientation to portrait mode
    screen.orientation.lock('portrait');
    document.addEventListener('deviceready', function () {
    distributor.ip      = new ip();
    distributor.ip      = distributor.ip.remoteIpAddress();
    distributor.ctr     = new controller();
    distributor.SQLLObj = new SQLiteLogin();
    // distributor.SQLAObj = new SQLiteAddress();
    distributor.locSet  = new locationSet();
    distributor.cam     = new camera();
    // distributor.SQLAObj.zone();
    // distributor.SQLAObj.district();
    distributor.bindEvents();
    },false);
  },

  bindEvents: function() {
    $(document).on('click', '#camera', function(e) {
      e.preventDefault();
      distributor.cam.openDefaultCamera();
    });
    $(document).on('click','#gallary', function(e) {
      e.preventDefault();
      distributor.cam.openFilePicker();
    });
    $(document).on('click', '#btnDistInfoInsert', distributor.pushDistToRemote);
    $(document).on('click', '#mapBtn', distributor.loadMpa);
    $(document).on('click', '#gpsBtn', distributor.setLocation);
  },
  setLocation: function() {
    distributor.locSet.setPosition();
  },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
    // $('#google-map').removeClass('noDisplay');
    //   $('#google-map').addClass('google-map');
  },
  addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },


  pushDistToRemote: function(e) {
    e.preventDefault();

    //  geting data from page
    var distName        = $('#distName').val();
    var distOwner       = $('#distOwner').val();
    var distPostalCode  = $('#postalCode').val();
    var distLat         = $('#latOfLoc').attr('lat');
    var distLng         = $('#lngOfLoc').attr('lng');
    var distCountry     = $('#distCountry').val();
    var distCity        = $('#distCity').val();
    var distPhone       = $('#distPhone').val();
    var distMobile      = $('#distMobile').val();
    var distEmail       = $('#distEmail').val();
    var distFacebook    = $('#distFacebook').val();
    var distViber       = $('#distViber').val();
    var distCreditLimit = $('#distCreditLimit').val();


    if (distName.trim() === '') {
      navigator.notification.alert(
                    'please enter business name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distOwner.trim() === '') {
      navigator.notification.alert(
                    'please enter business owner name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distPostalCode.trim() === '') {
      navigator.notification.alert(
                    'postal code field can not be empty.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if(distLat.trim() === '' || distLng.trim() === '') {
      navigator.notification.alert(
                    'please select distributor location',
                    null,
                    'Error',
                    'OK'
                );
                return;
    }
    if (distCountry.trim() === '') {
      navigator.notification.alert(
                    'please enter business country name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distCity.trim() === '') {
      navigator.notification.alert(
                    'please select city name.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    if (distMobile.trim() === '') {
      navigator.notification.alert(
                    'please enter distributor mobile number.',
                    null,
                    'Error',
                    'OK'
                );
      return;
    }
    // checking if email address field is empty or not 
    if(!(distEmail.trim() === '')) {
      var checkEmail = distributor.isEmail();
        if(checkEmail === 0) {
            navigator.notification.alert(
                            'please enter valid email.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
    }

    distributor.addLoading();
    $('.loadingText').text('sending data...');
    var tokenInformations = distributor.SQLLObj.pullToken();
    tokenInformations.done(function(informations) {
      var accessToken = '';
      for (var idx in informations) {
          var information = informations[idx];
          //alert(information.name);
          accessToken = information.token;
      }
      if (accessToken.trim() === '') {
        console.log('there is no accessToken');
          window.location.href = "../index.html";
          return;
      }

      console.log(accessToken);
    /* First uploading image to server using
      file transfer protocal*/
      var imageData          = $('#photo').attr('src');
      var options            = new FileUploadOptions();
      options.fileKey        = "file";
      options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1)+'.jpg';
      options.mimeType       = "image/jpeg";
      console.log(options.fileName);
      // Creating new object to stor field data
      var params             = new Object();
      params.accessToken     = accessToken;
      params.distName        = distName;
      params.distOwner       = distOwner;
      params.distPostalCode  = distPostalCode;
      params.distLat         = distLat;
      params.distLng         = distLng;
      params.distCountry     = distCountry;
      params.distCity        = distCity;
      params.distPhone       = distPhone;
      params.distMobile      = distMobile;
      params.distEmail       = distEmail;
      params.distFacebook    = distFacebook;
      params.distViber       = distViber;
      params.distCreditLimit = distCreditLimit;
      // stroing params object to options
      options.params         = params;
      options.chunkedMode    = false;

      var ft = new FileTransfer();
      ft.upload(imageData, distributor.ip+'?r=customer-information-pusher/distributor', function(result){
        console.log('data is: ',result);
        distributor.removeLoading();
        var response = $.parseJSON(result.response);
        if(response.status === 1) {
          navigator.notification.alert(
                    response.msgBody,
                    null,
                    response.msgTitle,
                    'OK'
                );
                window.location.href = './distributorInsertForm.html';
        }
        if(response.status === 0) {
          navigator.notification.alert(
                    response.msgBody,
                    null,
                    response.msgTitle,
                    'OK'
                );
        }
        if(response.status === 2) {
          navigator.notification.alert(
                    response.msgBody,
                    null,
                    response.msgTitle,
                    'OK'
                );
                window.location.href = "../index.html";
        }
      }, function(error){
        console.error(error);
        distributor.removeLoading();
        navigator.notification.alert(
          'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
          null,
          'ERROR',
          'OK'
        );
      }, options);

    }).fail(function(error) {
      distributor.removeLoading();
        navigator.notification.alert(
          'Error occured',
          null,
          'ERROR',
          'OK'
        );
    });
  },

  isEmail: function() {
    var email = $('#distEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  }

}
distributor.initialize();
