var planning = {
    SQLLObj		: null,
    parentTr	: null,
    ip			: null,
    ctr			: null,
	distData	: null,
    retData		: null,
	tecData		: null,
	farData		: null,
	cattleData	: null,
	poultryData	: null,
	docData		: null,
    distCount	: null,
    retCount	: null,
	tecCount	: null,
	farCount	: null,
	cattleCount	: null,
	poultryCount: null,
	docCount	: null,
    allEva   : null,
    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
		this.ip              = new ip();
        this.ip              = this.ip.remoteIpAddress();
        this.SQLLObj         = new SQLiteLogin();
        this.ctr             = new controller();
        planning.addLoading();
        planning.initCalendar();
		planning.distCount    = 0;
		planning.retCount     = 0;
		planning.tecCount     = 0;
		planning.farCount     = 0;
		planning.cattleCount  = 0;
		planning.poultryCount = 0;
		planning.docCount     = 0;
		planning.bindEvents();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
	loadCalendar: function(data) {
		$(document).ready(function() {
			$('.mainPlanning').hide();
			var d = new Date();
    		var m = (d.getMonth()+1);
        	var datestring = '';
        	if(!((m.toString().length)>1)) {
            	datestring = d.getFullYear() + "-0" + (d.getMonth()+1) + "-"+ d.getDate();
        	} else {
            	datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
        	}
		
			$('#calendar').fullCalendar({
				height: 500,
				header: {
					left: 'prev,next today',
					center: 'title',
					right: 'month,agendaWeek,agendaDay'
				},
				defaultDate: datestring,
				navLinks: true, // can click day/week names to navigate views
				selectable: true,
				selectHelper: true,
            	dayClick: function(date, jsEvent, view) {
                    console.log($('#calendar').fullCalendar('getDate'));

                // alert('Clicked on: ' + date.format());

                // alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

                // alert('Current view: ' + view.name);

                // change the day's background color just for fun
					planning.addTemplete(date.format());
                	$(this).css('background-color', 'red');

            	},
				editable: true,
				eventLimit: true, // allow "more" link when too many events
				events: data
			});
		});
	},
    initCalendar: function() {
        var tokenInformations = planning.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=planning/calendar-init',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.loadDist();
                  planning.loadCalendar(data);
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.initCalendar,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
	bindEvents: function() {
		$(document).on('click', '.closePlan', planning.closeDiv);
        $(document).on('click', '.dist-add', planning.addDist);
        $(document).on('click', '.dist-minus', planning.remove);
        $(document).on('click', '.ret-add', planning.addRet);
        $(document).on('click', '.ret-minus', planning.remove);
        $(document).on('click', '.tec-add', planning.addTec);
        $(document).on('click', '.tec-minus', planning.remove);
        $(document).on('click', '.far-add', planning.addFar);
        $(document).on('click', '.far-minus', planning.remove);
        $(document).on('click', '.cattle-add', planning.addCattle);
        $(document).on('click', '.cattle-minus', planning.remove);
        $(document).on('click', '.poultry-add', planning.addPoultry);
        $(document).on('click', '.poultry-minus', planning.remove);
        $(document).on('click', '.doc-add', planning.addDoc);
        $(document).on('click', '.doc-minus', planning.remove);
        $(document).on('click', '.send-planning', planning.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('input', '#tecNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('tecId',$(this).attr('tec_id'));
             }
           });
        });
        $(document).on('input', '#farNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('farId',$(this).attr('far_id'));
             }
           });
        });
        $(document).on('input', '#cattleFarNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('cattleId',$(this).attr('cattle_id'));
             }
           });
        });
        $(document).on('input', '#poultryFarNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('poultryId',$(this).attr('poultry_id'));
             }
           });
        });
        $(document).on('input', '#docNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('docId',$(this).attr('doc_id'));
             }
           });
        });
    },
	addTemplete: function(date) {
        var templete = '<!-- div container for contaning all data in page -->'+
  '<div class="mainPlanning">'+
    '<div class="planningContainer">'+
      '<!-- Planning header div -->'+
      '<div class="planHeader row">'+
        '<h4 class="col-xs-8">Planning</h4>'+
			  '<button id="closeMain" class="btn btn-warning glyphicon glyphicon-remove closePlan pull-right col-xs-3"></button>'+
      '</div>'+
						
			'<form role="form">'+
        '<!-- Span that hold the date for planning -->'+
        '<span id="planDate" data-date=""></span>'+
				'<!-- div to list vist plan for all distributor -->'+
				'<div class="distPlanList planList">'+
          '<h4>Distributor List</h4>'+
					'<div class="distPlan row">'+
            '<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList" placeholder="distributor name">'+
              '<datalist id="distList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus dist-add col-xs-1"></button>'+
					'</div>'+
        '</div>'+

				'<!-- Div to list visit plan for all retailer -->'+
				'<div class="retPlanList planList">'+
					'<h4>Retailer List</h4>'+
					'<div class="retPlan row">'+
					  '<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList" placeholder="retailer name">'+
              '<datalist id="retList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-1"></button>'+
					'</div>'+
				'</div>'+

				'<!-- Div to list visit plan for all Technican -->'+
				'<div class="tecPlanList planList">'+
					'<h4>Technican List</h4>'+
					'<div class="tecPlan row">'+
					  '<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" tecId="" id="tecNameSearch" list="tecList" placeholder="technican name">'+
              '<datalist id="tecList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus tec-add col-xs-1"></button>'+
					'</div>'+
				'</div>'+

				'<!-- Div to list visit plan for all master farmer -->'+
				'<div class="farPlanList planList">'+
					'<h4>Master Farmer List</h4>'+
					'<div class="farPlan row">'+
					  '<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" farId="" id="farNameSearch" list="farList" placeholder="farmer name">'+
              '<datalist id="farList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus far-add col-xs-1"></button>'+
					'</div>'+
				'</div>'+
        '<!-- Div to list visit plan for all Cattle farmer list -->'+
				'<div class="cattleFarPlanList planList">'+
					'<h4>Master Cattle Farmer List</h4>'+
					'<div class="cattleFarPlan row">'+
					  '<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" cattleId="" id="cattleFarNameSearch" list="cattleFarList" placeholder="farmer name">'+
              '<datalist id="cattleFarList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus cattle-add col-xs-1"></button>'+
					'</div>'+
				'</div>'+
        '<!-- Div to list visit plan for all Poultry farmer list -->'+
				'<div class="poultryFarPlanList planList">'+
					'<h4>Master Poultry Farmer List</h4>'+
					'<div class="poultryFarPlan row">'+
					  '<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" poultryId="" id="poultryFarNameSearch" list="poultryFarList" placeholder="farmer name">'+
              '<datalist id="poultryFarList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus poultry-add col-xs-1"></button>'+
					'</div>'+
				'</div>'+

			  '<!-- Div to list visit plan for all doctor -->'+
				'<div class="docPlanList planList">'+
					'<h4>Doctor List</h4>'+
					'<div class="docPlan row">'+
						'<div class="form-group col-xs-9">'+
              '<input type="text" class="form-control" docId="" id="docNameSearch" list="docList" placeholder="Doctor name">'+
              '<datalist id="docList"></datalist>'+
            '</div>'+
            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus doc-add col-xs-1"></button>'+
					'</div>'+
				'</div>'+
        '<button type="button" class="btn btn-block btn-primary send-planning">DONE</button>'+
			'</form>'+
						
		'</div>'+
  '</div>';
  $(templete).insertAfter('.slideBar');
		$('#planDate').attr('data-date', date);
		$('#planDate').text('Date: '+date);
        var templete = '';
                  $.each(planning.distData, function() {
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
    $('#distList').append(templete);
    templete = '';
    $.each(planning.retData, function() {
        templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
    });
    $('#retList').append(templete);
    templete = '';
                  $.each(planning.tecData, function() {
                    templete += '<option tec_id="'+this.id+'" value="'+this.name+'">';
                  });
                  $('#tecList').append(templete);  
        templete = '';
                  $.each(planning.farData, function() {
                    templete += '<option far_id="'+this.id+'" value="'+this.name+'">';
                  });
                  $('#farList').append(templete); 
        templete = '';
            $.each(planning.cattleData, function() {
                  templete += '<option cattle_id="'+this.id+'" value="'+this.name+'">';
                  });
                  $('#cattleFarList').append(templete); 
        templete = '';
            $.each(planning.poultryData, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option poultry_id="'+this.id+'" value="'+this.name+'">';
                  });
                  // templete += '</ul>';
                  $('#poultryFarList').append(templete);
        templete = '';
            $.each(planning.docData, function() {
                      templete += '<option doc_id="'+this.id+'" value="'+this.name+'">';
                  });
                  $('#docList').append(templete);
	},
	closeDiv: function() {
		// e.preventDefault();
		// console.log('hi anil');
		// $('.mainPlanning').find('.planningContainer').remove();
		$('.mainPlanning').remove();
	},
	//  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.distData = data;
                  planning.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadDist,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.retData = data;
                  planning.loadTec();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadRet,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all technican data related to mr from server
    loadTec: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=planning/tec-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.tecData = data;
                  planning.loadFar();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadTec,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all farmer list data related to mr from server
    loadFar: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=planning/far-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.farData = data;
                  planning.loadCattle();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadFar,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all cattle farmer list data related to mr from server
    loadCattle: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=planning/cattle-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.cattleData = data;
                  planning.loadPoultry();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadCattle,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all poultry farmer list data related to mr from server
    loadPoultry: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=planning/poultry-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.poultryData = data;
                  planning.loadDoc();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadPoultry,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all doctor list data related to mr from server
    loadDoc: function() {
      var tokenInformations = planning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: planning.ip + '?r=planning/doc-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  planning.removeLoading();
                  planning.docData = data;
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  planning.loadDoc,
                  'Error',
                  'TRY AGAIN'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distPlan row">'+
                        '<div class="form-group col-xs-9">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+planning.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+planning.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus dist-add col-xs-1"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-1"></button>'+
					'</div>';
        $('.distPlanList').append(templete);
        var target = '#distList'+planning.distCount;
        var templeteOption = '';
                  
        $.each(planning.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        planning.distCount = planning.distCount + 1;
    },
    remove: function() {
        $(this).parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retPlan row">'+
					  '<div class="form-group col-xs-9">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+planning.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+planning.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2"></button>'+
					'</div>';
        $('.retPlanList').append(templete);
        var target = '#retList'+planning.retCount;
        var templeteOption = '';
                  
        $.each(planning.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        planning.retCount = planning.retCount + 1;
    },
    addTec: function() {
        var templete = '';
        templete += '<div class="tecPlan row">'+
					  '<div class="form-group col-xs-9">'+
                        '<input type="text" class="form-control" tecId="" id="tecNameSearch" list="tecList'+planning.tecCount+'" placeholder="technican name">'+
                        '<datalist id="tecList'+planning.tecCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus tec-add col-xs-1"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus tec-minus glyphicon glyphicon-remove col-xs-1"></button>'+
					'</div>';
        $('.tecPlanList').append(templete);
        var target = '#tecList'+planning.tecCount;
        var templeteOption = '';
                  
        $.each(planning.tecData, function() {
            templeteOption += '<option tec_id="'+this.id+'" value="'+this.name+'">';
        });
        $(target).append(templeteOption);
        planning.tecCount = planning.tecCount + 1;
    },
    addFar: function() {
        var templete = '';
        templete += '<div class="farPlan row">'+
					  '<div class="form-group col-xs-9">'+
                        '<input type="text" class="form-control" farId="" id="farNameSearch" list="farList'+planning.farCount+'" placeholder="farmer name">'+
                        '<datalist id="farList'+planning.farCount+'"></datalist>'+
                      '</div>'+
                      '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus far-add col-xs-1"></button>'+
					  '<button type="button" class="btn btn-warning floating-minus far-minus glyphicon glyphicon-remove col-xs-1"></button>'+
                    '</div>';
        $('.farPlanList').append(templete);
        var target = '#farList'+planning.farCount;
        var templeteOption = '';
                  
        $.each(planning.farData, function() {
            templeteOption += '<option far_id="'+this.id+'" value="'+this.name+'">';
        });
        $(target).append(templeteOption);
        planning.farCount = planning.farCount + 1;
    },
    addCattle: function() {
        var templete = '';
        templete += '<div class="cattleFarPlan row">'+
					  '<div class="form-group col-xs-9">'+
                        '<input type="text" class="form-control" cattleId="" id="cattleFarNameSearch" list="cattleFarList'+planning.cattleCount+'" placeholder="farmer name">'+
                        '<datalist id="cattleFarList'+planning.cattleCount+'"></datalist>'+
                      '</div>'+
                      '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus cattle-add col-xs-1"></button>'+
                      '<button type="button" class="btn btn-warning floating-minus cattle-minus glyphicon glyphicon-remove col-xs-1"></button>'+
                    '</div>';
        $('.cattleFarPlanList').append(templete);
        var target = '#cattleFarList'+planning.cattleCount;
        var templeteOption = '';
                  
        $.each(planning.cattleData, function() {
            templeteOption += '<option cattle_id="'+this.id+'" value="'+this.name+'">';
        });
        $(target).append(templeteOption);
        planning.cattleCount = planning.cattleCount + 1;
    },
    addPoultry: function() {
        var templete = '';
        templete += '<div class="poultryFarPlan row">'+
					  '<div class="form-group col-xs-9">'+
                        '<input type="text" class="form-control" poultryId="" id="poultryFarNameSearch" list="poultryFarList'+planning.poultryCount+'" placeholder="farmer name">'+
                        '<datalist id="poultryFarList'+planning.poultryCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus poultry-add col-xs-1"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus poultry-minus glyphicon glyphicon-remove col-xs-1"></button>'+
                    '</div>';
        $('.poultryFarPlanList').append(templete);
        var target = '#poultryFarList'+planning.poultryCount;
        var templeteOption = '';
                  
        $.each(planning.poultryData, function() {
            templeteOption += '<option poultry_id="'+this.id+'" value="'+this.name+'">';
        });
        $(target).append(templeteOption);
        planning.poultryCount = planning.poultryCount + 1;
    },
    addDoc: function() {
        var templete = '';
        templete += '<div class="docPlan row">'+
						'<div class="form-group col-xs-9">'+
                            '<input type="text" class="form-control" docId="" id="docNameSearch" list="docList'+planning.docCount+'" placeholder="Doctor name">'+
                            '<datalist id="docList'+planning.docCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus doc-add col-xs-1"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus doc-minus glyphicon glyphicon-remove col-xs-1"></button>'+
                    '</div>';
        $('.docPlanList').append(templete);
        var target = '#docList'+planning.docCount;
        var templeteOption = '';
                  
        $.each(planning.docData, function() {
            templeteOption += '<option doc_id="'+this.id+'" value="'+this.name+'">';
        });
        $(target).append(templeteOption);
        planning.docCount = planning.docCount + 1;
    },
    check: function(e) {
        e.preventDefault();
        var dstStatus = 0;
        $.each($('.distPlan'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retPlan'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var tecStatus = 0;
        $.each($('.tecPlan'), function() {
            if(($(this).find('#tecNameSearch').val().trim() ==='')) {
                $(this).find('#tecNameSearch').attr('tecId','');
            } else {
                if($(this).find('#tecNameSearch').attr('tecId').trim() === '') {
                    tecStatus = 1;
                }
            }
        });
        if(tecStatus === 1) {
            navigator.notification.alert(
                            'technican information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var farStatus = 0;
        $.each($('.farPlan'), function() {
            if(($(this).find('#farNameSearch').val().trim() ==='')) {
                $(this).find('#farNameSearch').attr('farId','');
            } else {
                if($(this).find('#farNameSearch').attr('farId').trim() === '') {
                    farStatus = 1;
                }
            }
        });
        if(farStatus === 1) {
            navigator.notification.alert(
                            'master farmer information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var cattleFarStatus = 0;
        $.each($('.cattleFarPlan'), function() {
            if(($(this).find('#cattleFarNameSearch').val().trim() ==='')) {
                $(this).find('#cattleFarNameSearch').attr('cattleId','');
            } else {
                if($(this).find('#cattleFarNameSearch').attr('cattleId').trim() === '') {
                    cattleFarStatus = 1;
                }
            }
        });
        if(cattleFarStatus === 1) {
            navigator.notification.alert(
                            'master cattle farmer information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var poultryFarStatus = 0;
        $.each($('.poultryFarPlan'), function() {
            if(($(this).find('#poultryFarNameSearch').val().trim() ==='')) {
                $(this).find('#poultryFarNameSearch').attr('poultryId','');
            } else {
                if($(this).find('#poultryFarNameSearch').attr('poultryId').trim() === '') {
                    poultryFarStatus = 1;
                }
            }
        });
        if(poultryFarStatus === 1) {
            navigator.notification.alert(
                            'master poultry farmer information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var docStatus = 0;
        $.each($('.docPlan'), function() {
            if(($(this).find('#docNameSearch').val().trim() ==='')) {
                $(this).find('#docNameSearch').attr('docId','');
            } else {
                if($(this).find('#docNameSearch').attr('docId').trim() === '') {
                    docStatus = 1;
                }
            }
        });
        if(docStatus === 1) {
            navigator.notification.alert(
                            'master doctor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        planning.addLoading();
        $('.loadingText').text('sending....');
        planning.sendRemote();
    },
    sendRemote: function() {
        var planInfo = [];
        var distList = [];
        $.each($('.distPlan'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        planInfo.push(distList);
        var retList = [];
        $.each($('.retPlan'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        planInfo.push(retList);
        var tecList = [];
        $.each($('.tecPlan'), function() {
            if(!($(this).find('#tecNameSearch').attr('tecId').trim() === '')) {
                var data = {
                tec_id: $(this).find('#tecNameSearch').attr('tecId')
                }
                tecList.push(data);
            }
        });
        planInfo.push(tecList);
        var farList = [];
        $.each($('.farPlan'), function() {
            if(!($(this).find('#farNameSearch').attr('farId').trim() === '')) {
                var data = {
                far_id: $(this).find('#farNameSearch').attr('farId')
                }
                farList.push(data);
            }
        });
        planInfo.push(farList);
        var cattleList = [];
        $.each($('.cattleFarPlan'), function() {
            if(!($(this).find('#cattleFarNameSearch').attr('cattleId').trim() === '')) {
                var data = {
                cattle_id: $(this).find('#cattleFarNameSearch').attr('cattleId')
                }
                cattleList.push(data);
            }
        });
        planInfo.push(cattleList);
        var poultryList = [];
        $.each($('.poultryFarPlan'), function() {
            if(!($(this).find('#poultryFarNameSearch').attr('poultryId').trim() === '')) {
                var data = {
                poultry_id: $(this).find('#poultryFarNameSearch').attr('poultryId')
                }
                poultryList.push(data);
            }
        });
        planInfo.push(poultryList);
        var docList = [];
        $.each($('.docPlan'), function() {
            if(!($(this).find('#docNameSearch').attr('docId').trim() === '')) {
                var data = {
                doc_id: $(this).find('#docNameSearch').attr('docId')
                }
                docList.push(data);
            }
        });
        planInfo.push(docList);
        console.log(planInfo);
        // sending data to server using ajax
        var tokenInformations = planning.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken: accessToken,
                date: $('#planDate').attr('data-date'),
                planInfo: planInfo
            };
            $.ajax({
                url: planning.ip + '?r=planning/handler',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    // console.log(data);
                    planning.removeLoading();
                    if(data === 3) {
                        navigator.notification.alert(
                            ' There is no data to record.',
                            null,
                            'Warnning',
                            'OK'
                        );
                    } else if (data === 1) {
                        navigator.notification.alert(
                            ' Successfully recorded plannig.',
                            null,
                            'Success',
                            'OK'
                        );
                        // window.location.href = "file:///android_asset/www/views/planning.html";
                        planning.loadEventToCalender();
                        planning.closeDiv();
                    } else if (data === 0) {
                        navigator.notification.alert(
                            ' Failed to record master technican data',
                            null,
                            'Error',
                            'OK'
                        );
                    } else if (data === 2) {
                        window.location.href = "../index.html";
                    }

                }
            }).fail(function(error) {
                console.error(error);
                planning.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });

        }).fail(function(error) {
            planning.removeLoading();
            console.error(error);
        });
    },
    loadEventToCalender: function() {
        $.each($('.distPlan'), function() {
            var data = {
                        title: 'distributor:- '+$('#distNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
        $.each($('.retPlan'), function() {
            var data = {
                        title: 'retailer:- '+$('#retNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
        $.each($('.tecPlan'), function() {
            var data = {
                        title: 'technican:- '+$('#tecNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
        $.each($('.farPlan'), function() {
            var data = {
                        title: 'farmer:- '+$('#farNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
        $.each($('.cattleFarPlan'), function() {
            var data = {
                        title: 'cattle:- '+$('#cattleFarNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
        $.each($('.poultryFarPlan'), function() {
            var data = {
                        title: 'poultry:- '+$('#poultryFarNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
        $.each($('.docPlan'), function() {
            var data = {
                        title: 'doctor:- '+$('#docNameSearch').val(),
					    start: $('#planDate').attr('data-date'),
            };
            $('#calendar').fullCalendar('renderEvent', data, true); // stick? = true
        });
    }
}
planning.initialize();