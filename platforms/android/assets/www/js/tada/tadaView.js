var tadaView = {
    SQLLObj          : null,
    ip               : null,
    ctr              : null,
    locSet           : null,
    globalAccessToken: null,
    init: function() {
        document.addEventListener('deviceready', function () {
            // unlock orentation
            screen.orientation.unlock();
            // set to either landscape 
            screen.orientation.lock('landscape');
            tadaView.ip      = new ip();
            tadaView.ip      = tadaView.ip.remoteIpAddress();
            tadaView.ctr     = new controller();
            tadaView.SQLLObj = new SQLiteLogin();
            tadaView.locSet  = new locationSet();
            tadaView.eventListener();
        },false);
    },
    eventListener: function() {
        $(document).on('click', '#btnDate', function(e) {
            e.preventDefault();
            tadaView.loadData();
        });
        $(document).on('click', '#fromDate', function(e) {
            e.preventDefault();
            tadaView.showDatePicker('#fromDate');
        });
        $(document).on('click', '#toDate', function(e) {
            e.preventDefault();
            tadaView.showDatePicker('#toDate');
        });
        $(document).on('click', '.btn-edit', function(e) {
            e.preventDefault();
            // $(this).animate({
            //   width: [ "toggle", "swing" ],
            //   height: [ "toggle", "swing" ],
            //   opacity: '0'
            // }, 250, "linear", function() {
            //   alert('done');
            // });
            var parent = $(this).parent().parent();
            tadaView.updateTada(parent);
        });
    },
    showDatePicker: function(element) {
      var options = {
          date: new Date(),
          mode: 'date',
          windowTitle: 'Date for TADA'
        };

        datePicker.show(options, function(d){
          var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
          $(element).val(datestring);
        });
    },
    loadData: function() {
        if(!($('#fromDate').val().trim() === '' && $('#toDate').val().trim() === '')) {
            var tokenInformations = tadaView.SQLLObj.pullToken();
            tokenInformations.done(function(informations) {
                var accessToken = '';
                for (var idx in informations) {
                    var information = informations[idx];
                    //alert(information.name);
                    accessToken = information.token;
                    tadaView.globalAccessToken = information.token;
                }
                if (accessToken.trim() === '') {
                    console.log('there is no accessToken');
                    window.location.href = "../index.html";
                    return;
                }
            
                // console.log(accessToken);
            
                var values = {
                    accessToken: accessToken,
                    from: $('#fromDate').val(),
                    to: $('#toDate').val()
                };
                  tadaView.addLoading();
            
                $.ajax({
                    url: tadaView.ip + '?r=tada/tadarec',
                    method: "POST",
                    dataType: 'json',
                    data: values,
                
                    error: function(jqXHR, textStatus, errorThrown) {
                        console.error(jqXHR.status + '\n' + jqXHR.responseText);
                    },
                
                    success: function(data) {
                        tadaView.removeLoading();
                        // console.log(data);
                        if (data === 2) {
                            window.location.href = "../index.html";
                        }
                        $('.tadaTable tbody').empty();
                        // alert(data[8].tadaMetas.length);
                        for(var i =0; i < data.length; i++) {
                            var temp = '<tr>';
                            var leave = '';
                            if(parseInt(data[i].is_leave) === 1) {
                                leave = 'leave';
                                temp += '<td id="date" tadaId="'+data[i].id+'" style="word-wrap: break-word; white-space: nowrap;">'+data[i].date+'</td>'+
                                        '<td id="from"></td>'+
                                        '<td id="to"></td>'+
                                        '<td id="km"></td>'+
                                        '<td id="taAmount"></td>'+
                                        '<td id="daAmount"></td>'+
                                        '<td id="totalAmount"></td>'+
                                        '<td id="leave" isLeave="'+data[i].is_leave+'" style="word-wrap: break-word;">leave</td>';
                            } else {
                            temp += '<td id="date" tadaId="'+data[i].id+'" style="word-wrap: break-word; white-space: nowrap;">'+data[i].date+'</td>';
                            }
                            for(var j =0; j < data[i].tadaMetas.length; j++) {
                                temp +=     '<td id="from" style="word-wrap: break-word; white-space: nowrap;">'+data[i].tadaMetas[j].ta_from+'</td>'+
                                            '<td id="to" style="word-wrap: break-word; white-space: nowrap;">'+data[i].tadaMetas[j].ta_to+'</td>'+
                                            '<td id="km" style="word-wrap: break-word; white-space: nowrap;">'+data[i].tadaMetas[j].km+'</td>'+
                                            '<td id="taAmount" style="word-wrap: break-word; white-space: nowrap;">'+data[i].tadaMetas[j].ta_amount+'</td>'+
                                            '<td id="daAmount" style="word-wrap: break-word; white-space: nowrap;">'+data[i].tadaMetas[j].da_amount+'</td>'+
                                            '<td id="totalAmount" style="word-wrap: break-word; white-space: nowrap;">'+data[i].tadaMetas[j].total_amount+'</td>'+
                                            '<td id="leave" isLeave="'+data[i].is_leave+'"></td>'+
                                            '<td style="word-wrap: break-word; white-space: nowrap;">'+
                                                '<span type="text" style="background-color: transparent; font-size: 14px; border: none;" class="glyphicon glyphicon-pencil btn-edit"></span>'+
                                            '</td>';
                            }
                            temp += '</tr>';
                            $('.tadaTable tbody').append(temp);
                        }
                    }
                }).fail(function(error) {
                    tadaView.removeLoading();
                    console.error(error);
                    navigator.notification.alert(
                        ' There is error in network',
                        null,
                        'Error',
                        'Ok'
                    );
                });
            
            }).fail(function(error) {
                console.error(error);
            });
        } else {
            navigator.notification.alert(
                        'Select from and to date.',
                        null,
                        'Warrning',
                        'Ok'
                    );
        }
    },
    updateTada: function(obj) {
        var dataToPass = {
            id         : obj.find('#date').attr('tadaId'),
            accessToken: tadaView.globalAccessToken,
            date       : obj.find('#date').text(),
            from       : obj.find('#from').text(),
            to         : obj.find('#to').text(),
            km         : obj.find('#km').text(),
            taAmount   : obj.find('#taAmount').text(),
            daAmount   : obj.find('#daAmount').text(),
            totalAmount: obj.find('#totalAmount').text(),
            isLeave    : obj.find('#leave').attr('isLeave')
        };
        console.log(dataToPass);
        // cleaning data that is stored in dataToPass variable
        window.localStorage.setItem("dataToPass", "");
        // storing data tada data to dataToPass local stroage variable
        window.localStorage.setItem("dataToPass", JSON.stringify(dataToPass));
        setTimeout(function() {
            // calling updateTada html file after 30 mili second of edit button click
            window.location.href = './tadaUpdate.html';
        }, 30);
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    }
};
tadaView.init();