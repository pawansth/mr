var tada = {
    SQLLObj: null,
    ip     : null,
    ctr    : null,
    locSet : null,
    globalAccessToken: null,
    taAmount: null,
    daAmount1: null,
    daAmount2: null,
    daAmount3: null,
    init: function() {
        document.addEventListener('deviceready', function () {
            // $('#disInfoContainer').css({'padding-top': '20px','width':'100vh'});
            // unlock orentation
            screen.orientation.unlock();
            // set to either landscape 
            screen.orientation.lock('landscape');
            tada.taAmount = 0;
            tada.daAmount1 = 0;
            tada.daAmount2 = 0;
            tada.daAmount3 = 0;
            tada.ip      = new ip();
            tada.ip      = tada.ip.remoteIpAddress();
            tada.ctr     = new controller();
            tada.SQLLObj = new SQLiteLogin();
            tada.locSet  = new locationSet();
            tada.loadData();
            tada.eventListener();
        }, false);
    },
    eventListener: function() {
        $(document).on('click', '.tadaDate', function(e) {
            e.preventDefault();
            tada.showDatePicker($(this));
        });
        $(document).on('click', '.tada-add', function() {
            tada.addRows();
        });
        $(document).on('click', '.tada-remove', function() {
            $(this).parent().parent().remove();
        });
        $(document).on('focusout', '.tadaKm', function() {
            var parent = $(this).parent().parent();
            if(!((parent.find('.tadaKm').val().trim() === '') && (parent.find('.tadaTA').val().trim() === '') && (parent.find('.tadaDA').val().trim() === ''))) {
                parent.find('.tadaTotal').val((parseInt(parent.find('.tadaKm').val())*(parseInt(parent.find('.tadaTA').val())))+parseInt(parent.find('.tadaDA').val()));
            }
        });
        $(document).on('change', '.tadaDA', function() {
            var parent = $(this).parent().parent();
            if(!((parent.find('.tadaKm').val().trim() === '') && (parent.find('.tadaTA').val().trim() === '') && (parent.find('.tadaDA').val().trim() === ''))) {
                parent.find('.tadaTotal').val((parseInt(parent.find('.tadaKm').val())*(parseInt(parent.find('.tadaTA').val())))+parseInt(parent.find('.tadaDA').val()));
            }
        });
        $(document).on('click', '.btn-submit', tada.check);
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    addRows: function(){
                var rows = '<tr class="tadaRow">'+
                      '<td>'+
                        '<input type="text" class="form-control tadaDate" placeholder="date" readonly>'+
                      '</td>'+
                      '<td>'+
                        '<input type="text" class="form-control tadaFrom" placeholder="from">'+
                      '</td>'+
                      '<td>'+
                        '<input type="text" class="form-control tadaTo" placeholder="To">'+
                      '</td>'+
                      '<td>'+
                        '<input type="text" class="form-control tadaKm" placeholder="K.M">'+
                      '</td>'+
                      '<td>'+
                        '<input type="text" class="form-control tadaTA" placeholder="T.A" readonly value="'+tada.taAmount+'">'+
                      '</td>'+
                      '<td>'+
                        '<select class="form-control tadaDA">'+
                        '<option value="'+tada.daAmount1+'">'+tada.daAmount1+'</option>'+
                            '<option value="'+tada.daAmount2+'">'+tada.daAmount2+'</option>'+
                            '<option value="'+tada.daAmount3+'">'+tada.daAmount3+'</option>'+
                        '</select>'+
                      '</td>'+
                      '<td>'+
                        '<input type="text" class="form-control tadaTotal" placeholder="Total">'+
                      '</td>'+
                      '<td>'+
                        '<input type="checkbox" class="tadaLeave">'+
                      '</td>'+
                      '<td>'+
                        '<button type="button" class="btn btn-warning floating-add tada-add glyphicon glyphicon-plus" style="float: left;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus tada-remove glyphicon glyphicon-remove" style="float: left;"></button>'+
                      '</td>'+
                    '</tr>';
            $('.tadaTable tbody').append(rows);
    },
    showDatePicker: function(element) {
      var options = {
          date: new Date(),
          mode: 'date',
          windowTitle: 'Date for TADA'
        };

        datePicker.show(options, function(d){
          var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
          $(element).val(datestring);
        });
    },
    loadData: function() {
        var tokenInformations = tada.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
              tada.globalAccessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };
            tada.addLoading();

          $.ajax({
              url: tada.ip + '?r=tada/load-data',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  tada.removeLoading();
                  // console.log(data);
                  if (data.status === 2) {
                      window.location.href = "../index.html";
                  } else if(data.status === 0) {
                        navigator.notification.alert(
                            data.msgBody,
                            tada.loadData,
                            data.msgTitle,
                            'Try Again'
                        );
                    } else {
                        var options = '';
                        options += '<option value="'+data.da_allowance1+'">'+data.da_allowance1+'</option>'+
                                  '<option value="'+data.da_allowance2+'">'+data.da_allowance2+'</option>'+
                                  '<option value="'+data.da_allowance3+'">'+data.da_allowance3+'</option>';
                        $('.tadaDA').append(options);
                        $('.tadaTA').val(data.ta_allowance);
                        tada.taAmount = data.ta_allowance;
                        tada.daAmount1 = data.da_allowance1;
                        tada.daAmount2 = data.da_allowance2;
                        tada.daAmount3 = data.da_allowance3;
                    }
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  tada.loadData,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    check: function() {
        var count = 1;
        $('table > tbody > tr').each(function() {
            if($(this).find('.tadaDate').val().trim() == '') {
                navigator.notification.alert(
                  'Date field is empty in row'+count,
                  null,
                  'Warrning',
                  'Ok'
              );
                return;
            }
            if(!$(this).find(".tadaLeave").is(':checked')) {
                if($(this).find('.tadaDate').val().trim() == '') {
                    navigator.notification.alert(
                      'Date field is empty in row'+count,
                      null,
                      'Warrning',
                      'Ok'
                  );
                    return;
                }
                if($(this).find('.tadaFrom').val().trim() == '') {
                    navigator.notification.alert(
                      'From field is empty in row'+count,
                      null,
                      'Warrning',
                      'Ok'
                  );
                    return;
                }
                if($(this).find('.tadaTo').val().trim() == '') {
                    navigator.notification.alert(
                      'To field is empty in row'+count,
                      null,
                      'Warrning',
                      'Ok'
                  );
                    return;
                }
                if($(this).find('.tadaKm').val().trim() == '') {
                    navigator.notification.alert(
                      'K.M field is empty in row'+count,
                      null,
                      'Warrning',
                      'Ok'
                  );
                    return;
                }
                if($(this).find('.tadaTotal').val().trim() == '') {
                    navigator.notification.alert(
                      'Total amount field is empty in row'+count,
                      null,
                      'Warrning',
                      'Ok'
                  );
                    return;
                }
            }
            count = count + 1;
        });
        tada.sendToRemote();
    },
    sendToRemote: function() {
        var tadaData = [];
        $('table > tbody > tr').each(function() {
            var leave = 0;
            if($(this).find(".tadaLeave").is(':checked')) {
                leave = 1;
            }
            var data = {
                date: $(this).find('.tadaDate').val(),
                from: $(this).find('.tadaFrom').val(),
                to: $(this).find('.tadaTo').val(),
                km: $(this).find('.tadaKm').val(),
                ta: $(this).find('.tadaTA').val(),
                da: $(this).find('.tadaDA').val(),
                total: $(this).find('.tadaTotal').val(),
                isLeave: leave
            };
            tadaData.push(data);
        });
        var values = {
            accessToken: tada.globalAccessToken,
            datas: tadaData
        };
        var url = tada.ip+'?r=tada/insert';
        tada.addLoading();
        $('.loadingText').val('Submiting data');
        var result = tada.ajaxCall(url, values);
        result.then(function(data) {
            tada.removeLoading();
            if(data.status === 2) {
                window.location.href = "../index.html";
            } else if(data.status === 0) {
                navigator.notification.alert(
                            data.msgBody,
                            null,
                            data.msgTitle,
                            'Ok'
                        );
            } else {
                navigator.notification.alert(
                            data.msgBody,
                            null,
                            data.msgTitle,
                            'Ok'
                        );
                window.location.href = "./tada.html";
            }
        }).catch(function(error) {
            console.error(error);
            tada.removeLoading();
            navigator.notification.alert(
                    'Network error occured',
                    null,
                    'ERROR',
                    'OK'
                );
        });
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    }
}
tada.init();