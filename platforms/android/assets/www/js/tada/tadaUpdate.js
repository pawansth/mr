var tadaUpdate = {
    // defining global variables
    globalAccessToken: null,
    ip               : null,
    globID           : null,
    ctr              : null,
    locSet           : null,
    SQLLObj          : null,
    // initial function for tada
    initialize: function() {
        document.addEventListener('deviceready', function () {
            // unlocking screen orientation
            screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
            // featching ip address of server
            tadaUpdate.ip      = new ip();
            tadaUpdate.ip      = tadaUpdate.ip.remoteIpAddress();
            tadaUpdate.ctr     = new controller();
            tadaUpdate.SQLLObj = new SQLiteLogin();
            tadaUpdate.locSet  = new locationSet();

            // featching data that have locally stored while calling this page
            var passedData = window.localStorage.getItem('dataToPass');
            var dataPassed = JSON.parse(passedData);
            tadaUpdate.globalAccessToken = dataPassed.accessToken;
            tadaUpdate.globID = dataPassed.id;
            if(tadaUpdate.loadData()) {
                tadaUpdate.formInitializer(dataPassed);
                tadaUpdate.addEventListener();
            }
        },false);
    },
    formInitializer: function(dataPassedObj) {
        $('#tadaDate').val(dataPassedObj.date);
        $('#tadaFrom').val(dataPassedObj.from);
        $('#tadaTo').val(dataPassedObj.to);
        $('#tadaKm').val(dataPassedObj.km);
        $('#tadaTAmount').val(dataPassedObj.taAmount);
        $('#tadaDAmount').val(dataPassedObj.daAmount);
        $('#tadaTotal').val(dataPassedObj.totalAmount);
        if(dataPassedObj.isLeave === 1) {
            $('#tadaLeave').attr('checked', true);
        }
    },
    addEventListener: function() {
        $(document).on('focusout', '#tadaKm', function() {
            if(!(($('#tadaKm').val().trim() === '') && ($('#tadaTAmount').val().trim() === '') && ($('#tadaDAmount').val().trim() === ''))) {
                $('#tadaTotal').val((parseInt($('#tadaKm').val())*(parseInt($('#tadaTAmount').val())))+parseInt($('#tadaDAmount').val()));
            }
        });
        $(document).on('change', '#tadaDAmount', function() {
            if(!(($('#tadaKm').val().trim() === '') && ($('#tadaTAmount').val().trim() === '') && ($('#tadaDAmount').val().trim() === ''))) {
                $('#tadaTotal').val((parseInt($('#tadaKm').val())*(parseInt($('#tadaTAmount').val())))+parseInt($('#tadaDAmount').val()));
            }
        });
        $(document).on('click', '#tadaDate', function(e) {
            e.preventDefault();
            tadaUpdate.showDatePicker($(this));
        });
        $(document).on('click','#btn-submit', tadaUpdate.check);
    },
    loadData: function() {

          var values = {
              accessToken: tadaUpdate.globalAccessToken
          };
            tadaUpdate.addLoading();

          $.ajax({
              url: tadaUpdate.ip + '?r=tada/load-data',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  tadaUpdate.removeLoading();
                  // console.log(data);
                  if (data.status === 2) {
                      window.location.href = "../index.html";
                  } else if(data.status === 0) {
                        navigator.notification.alert(
                            data.msgBody,
                            tadaUpdate.loadData,
                            data.msgTitle,
                            'Try Again'
                        );
                    } else {
                        var options = '';
                        options += '<option value="'+data.da_allowance1+'">'+data.da_allowance1+'</option>'+
                                  '<option value="'+data.da_allowance2+'">'+data.da_allowance2+'</option>'+
                                  '<option value="'+data.da_allowance3+'">'+data.da_allowance3+'</option>';
                        $('#tadaDAmount').append(options);
                        return true;
                    }
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  tadaUpdate.loadData,
                  'Error',
                  'Try Again'
              );
          });
          return true;
    },
    showDatePicker: function() {
        var options = {
            date: new Date(),
            mode: 'date',
            windowTitle: 'Date for TADA'
          };
  
          datePicker.show(options, function(d){
            var datestring = d.getFullYear() + "-" + (d.getMonth()+1) + "-"+ d.getDate();
            $('#tadaDate').val(datestring);
          });
      },
    check: function() {
        if($('#tadaDate').val().trim() == '') {
            navigator.notification.alert(
              'Date field is empty in row',
              null,
              'Warrning',
              'Ok'
          );
            return;
        }
        if(!$("#tadaLeave").is(':checked')) {
            if($('#tadaDate').val().trim() == '') {
                navigator.notification.alert(
                  'Date field is empty in row',
                  null,
                  'Warrning',
                  'Ok'
              );
                return;
            }
            if($('#tadaFrom').val().trim() == '') {
                navigator.notification.alert(
                  'From field is empty in row',
                  null,
                  'Warrning',
                  'Ok'
              );
                return;
            }
            if($('#tadaTo').val().trim() == '') {
                navigator.notification.alert(
                  'To field is empty in row',
                  null,
                  'Warrning',
                  'Ok'
              );
                return;
            }
            if($('#tadaKm').val().trim() == '') {
                navigator.notification.alert(
                  'K.M field is empty in row',
                  null,
                  'Warrning',
                  'Ok'
              );
                return;
            }
            if($('#tadaTotal').val().trim() == '') {
                navigator.notification.alert(
                  'Total amount field is empty',
                  null,
                  'Warrning',
                  'Ok'
              );
                return;
            }
        }
        tadaUpdate.submitUpdate();
    },
    submitUpdate: function() {
        var leave = 0;
        if($("#tadaLeave").is(':checked')) {
            leave = 1;
        }
        var data = {
            id: tadaUpdate.globID,
            date: $('#tadaDate').val(),
            from: $('#tadaFrom').val(),
            to: $('#tadaTo').val(),
            km: $('#tadaKm').val(),
            ta: $('#tadaTAmount').val(),
            da: $('#tadaDAmount').val(),
            total: $('#tadaTotal').val(),
            isLeave: leave
        };
        var values = {
            accessToken: tadaUpdate.globalAccessToken,
            tadaUpdateData: data
        };
        var url = tadaUpdate.ip+'?r=tada/make-update';
        tadaUpdate.addLoading();
        $('.loadingText').val('Updating ....');
        var result = tadaUpdate.ajaxCall(url, values);
        result.then(function(data) {
            tadaUpdate.removeLoading();
            if(data.status === 2) {
                window.location.href = "../index.html";
            } else if(data.status === 0) {
                navigator.notification.alert(
                            data.msgBody,
                            null,
                            data.msgTitle,
                            'Ok'
                        );
            } else {
                navigator.notification.alert(
                            data.msgBody,
                            null,
                            data.msgTitle,
                            'Ok'
                        );
                window.location.href = "./tadaView.html";
            }
        }).catch(function(error) {
            console.error(error);
            tadaUpdate.removeLoading();
            navigator.notification.alert(
                    'Network error occured',
                    null,
                    'ERROR',
                    'OK'
                );
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    ajaxCall: function(urls, values) {
        return $.ajax({
                url: urls,
                method: "POST",
                dataType: 'json',
                data: values,
            });
    }
};
tadaUpdate.initialize();