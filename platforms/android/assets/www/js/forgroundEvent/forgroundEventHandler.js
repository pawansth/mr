var forgroundEventHandler = function() {
  var flagHandler = {
    appStartFlagCheck: function() {
      var locBtnFlag = new SQLiteLocBtnFlag();
      console.log('after SQLiteLocBtnFlag object is created');
      informations = locBtnFlag.pullLocBtnFlag();
      informations.done(function(informations) {
          var startFlag;
          var stopFlag;
          // console.log('after sucessfully gain btnFlag informations');
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              startFlag = information.start_btn_flag;
              stopFlag = information.stop_btn_flag;
          }
          // checking previous apps status while starting apps
          if (startFlag === 1) {
              console.log('startFlag is : ' + startFlag);
              $(document).find('#btn-start').addClass('active');
              $(document).find('#btn-start').text('Started Day');
          } else {
              console.log('stopFlag is : ' + stopFlag);
              $(document).find('#btn-start').removeClass('active');
              $(document).find('#btn-start').text('Start Day');
              $(document).find('#btn-stop').addClass('active');
          }
      }).fail(function(error) {
          console.warn(error);
      });
      return;
    }
  }

  return flagHandler;
}
