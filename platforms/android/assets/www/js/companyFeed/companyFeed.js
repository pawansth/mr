var companyFeed = {
    SQLLObj: null,
    ip: null,
    ctr: null,

    initialize: function() {
        screen.orientation.unlock();
        // Lock orientation to portrait mode
        screen.orientation.lock('portrait');
        companyFeed.ip              = new ip();
        companyFeed.ip              = companyFeed.ip.remoteIpAddress();
        companyFeed.ctr             = new controller();
        companyFeed.SQLLObj         = new SQLiteLogin();
        $('#google-map').hide();
        companyFeed.bindEvents();
    },

    bindEvents: function() {
        $(document).on('focusout', '#compEmail', companyFeed.isEmail);
        $(document).on('click', '#btnCompInfoInsert', companyFeed.pushCompToRemote);
        $(document).on('click', '#mapBtn', companyFeed.loadMpa);
    },
    //  Displaying google map in screen
    loadMpa: function() {
        $('#google-map').show();
    },
    // method that show loading module
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    // method that remove loading module
    removeLoading: function() {
        $('.loading').remove();
    },


    pushCompToRemote: function(e) {
        e.preventDefault();
        //  geting data from page
        var compName        = $('#compName').val();
        var compPostalCode  = $('#postalCode').val();
        var compLat         = $('#latOfLoc').attr('lat');
        var compLng         = $('#lngOfLoc').attr('lng');
        var compCountry     = $('#compCountry').val();
        var compCity        = $('#compCity').val();
        var compPhone       = $('#compPhone').val();
        var compEmail       = $('#compEmail').val();
        var compFacebook    = $('#compFacebook').val();
        var compViber       = $('#compViber').val();

        if (compName.trim() === '') {
            navigator.notification.alert(
                            'company name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
            return;
        }
        if (compLat.trim() === '' || compLng.trim() === '') {
            navigator.notification.alert(
                            'please set location prorerly.',
                            null,
                            'warning',
                            'OK'
                        );
            return;
        }
        if (compPostalCode.trim() === '') {
            navigator.notification.alert(
                            'you have to provide postal code.',
                            null,
                            'warning',
                            'OK'
                        );
            return;
        }
        if (compCountry.trim() === '') {
            navigator.notification.alert(
                            'please fill country name where company is located.',
                            null,
                            'warning',
                            'OK'
                        );
            return;
        }
        if (compCity.trim() === '') {
            navigator.notification.alert(
                            'please fill city name where company is located.',
                            null,
                            'warning',
                            'OK'
                        );
            return;
        }
        // Checking email field is empty or not 
        if(!(compEmail.trim() === '')) {
            //  Checking email address is in correct formate or not
            var checkEmail = companyFeed.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
            return;
            }
        }
        // adding loader modul while sending data to server
        companyFeed.addLoading();
        $('.loadingText').text('Sending......');
        
        var tokenInformations = companyFeed.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
        var accessToken = '';
        for (var idx in informations) {
              var information = informations[idx];
            //alert(information.name);
            accessToken = information.token;
        }
        if (accessToken.trim() === '') {
            console.log('there is no accessToken');
            window.location.href = "../index.html";
            return;
        }

        console.log(accessToken);

        var values = {
            accessToken   : accessToken,
            compName      : compName,
            compPostalCode: compPostalCode,
            compLat       : compLat,
            compLng       : compLng,
            compCountry   : compCountry,
            compCity      : compCity,
            compPhone     : compPhone,
            compEmail     : compEmail,
            compFacebook  : compFacebook,
            compViber     : compViber
        };

        $.ajax({
            url: companyFeed.ip+'?r=company-feed/company-feed',
            method: "POST",
            dataType: 'json',
            data: values,

            error: function(jqXHR, textStatus, errorThrown) {
                console.error(jqXHR.status + '\n' + jqXHR.responseText);
            },

            success: function(data) {
                  // console.log(data);
                  companyFeed.removeLoading();
                if (data === 1) {
                    navigator.notification.alert(
                        ' Successfully recorede data',
                        null,
                        'Error',
                        'OK'
                    );
                    window.location.href = './companyFeed.html';
                } else if (data === 0) {
                    navigator.notification.alert(
                        'Failed recorede data.\n Data may already exist.',
                        null,
                        'Error',
                        'OK'
                    );
                } else if(data === 2) {
                    window.location.href = "../index.html";
                }

            }
        }).fail(function(error) {
            companyFeed.removeLoading();
            console.error(error);
            navigator.notification.alert(
                ' Failed to insert data',
                null,
                'Error',
                'OK'
            );
        });

        }).fail(function(error) {
            console.error(error);
        });
    },
    // Method for checking email address is correct or not
    isEmail: function() {
    var email = $('#compEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  }
}

companyFeed.initialize();