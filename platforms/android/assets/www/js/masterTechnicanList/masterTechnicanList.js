var masterTechnicanList = {
    SQLLObj  : null,
    ip       : null,
    ctr      : null,
    locSet   : null,
    distData : null,
    retData  : null,
    distCount: null,
    retCount : null,
    cam      : null,

    initialize: function() {
        $('#google-map').hide();
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        masterTechnicanList.ip      = new ip();
        masterTechnicanList.ip      = masterTechnicanList.ip.remoteIpAddress();
        masterTechnicanList.ctr     = new controller();
        masterTechnicanList.SQLLObj = new SQLiteLogin();
        masterTechnicanList.locSet  = new locationSet();

        masterTechnicanList.addLoading();
        masterTechnicanList.loadDist();

        masterTechnicanList.distCount = 0;
        masterTechnicanList.retCount  = 0;
        //  Creating object of camera class
        masterTechnicanList.cam       = new camera();
        masterTechnicanList.bindEvents();
    },
    bindEvents: function() {
        $(document).on('click', '#camera', function(e) {
            e.preventDefault();
            masterTechnicanList.cam.openDefaultCamera();
        });
        $(document).on('click','#gallary', function(e) {
            e.preventDefault();
            masterTechnicanList.cam.openFilePicker();
        });
        $(document).on('click', '#mapBtn', masterTechnicanList.loadMpa);
        $(document).on('click', '#gpsBtn', masterTechnicanList.setLocation);
        $(document).on('click', '.dist-add', masterTechnicanList.addDist);
        $(document).on('click', '.dist-minus', masterTechnicanList.remove);
        $(document).on('click', '.ret-add', masterTechnicanList.addRet);
        $(document).on('click', '.ret-minus', masterTechnicanList.remove);
        $(document).on('click', '.prodFocus-add' , masterTechnicanList.addProdFocus);
        $(document).on('click', '.prodFocus-minus', masterTechnicanList.remove);
        $(document).on('click', '.send-Master-technican', masterTechnicanList.check);
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        $(document).on('input', '#retNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('retId',$(this).attr('ret_id'));
             }
           });
        });
    },
    setLocation: function() {
        masterTechnicanList.locSet.setPosition();
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
  //  Displaying google map in screen
  loadMpa: function() {
    $('#google-map').show();
  },
  //  method that load all distributor data related to server from database
    loadDist: function() {
      var tokenInformations = masterTechnicanList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterTechnicanList.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterTechnicanList.distData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  masterTechnicanList.loadRet();

              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = masterTechnicanList.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: masterTechnicanList.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  masterTechnicanList.retData = data;
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#retList').append(templete);
                  masterTechnicanList.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  null,
                  'Error',
                  'OK'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addDist: function() {
        var templete = '';
        templete += '<div class="distData row">'+
                        '<div class="col-xs-8" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="distNameSearch" distId="" list="distList'+masterTechnicanList.distCount+'" placeholder="distributor name">'+
                            '<datalist id="distList'+masterTechnicanList.distCount+'"></datalist>'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning floating-add dist-add glyphicon glyphicon-plus col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus dist-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
			        '</div>';
        $('.distList').append(templete);
        var target = '#distList'+masterTechnicanList.distCount;
        var templeteOption = '';
                  
        $.each(masterTechnicanList.distData, function() {
            templeteOption += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
        });
        $(target).append(templeteOption);
        masterTechnicanList.distCount = masterTechnicanList.distCount + 1;
    },
    remove: function() {
        $(this).parent().remove();
    },
    addRet: function() {
        var templete = '';
        templete += '<div class="retData row">'+
					  '<div class="col-xs-8" style="padding-top:5px;">'+
                        '<input type="text" class="form-control" retId="" id="retNameSearch" list="retList'+masterTechnicanList.retCount+'" placeholder="retailer name">'+
                        '<datalist id="retList'+masterTechnicanList.retCount+'"></datalist>'+
                       '</div>'+
                        '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus ret-add col-xs-2" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus ret-minus glyphicon glyphicon-remove col-xs-2" style="margin-top:5px;"></button>'+
					'</div>';
        $('.retList').append(templete);
        var target = '#retList'+masterTechnicanList.retCount;
        var templeteOption = '';
                  
        $.each(masterTechnicanList.retData, function() {
            templeteOption += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
        });
        $(target).append(templeteOption);
        masterTechnicanList.retCount = masterTechnicanList.retCount + 1;
    },
    addProdFocus: function() {
        var templete = '';
        templete += '<div class="prodFocusData row">'+
                        '<div class="col-xs-9" style="padding-top:5px;">'+
                            '<input type="text" class="form-control" id="prodFocus" placeholder="Product Focus">'+
                        '</div>'+
                        '<button type="button" class="btn btn-warning col-xs-1 prodFocus-add floating-add glyphicon glyphicon-plus" style="margin-top:5px;"></button>'+
                        '<button type="button" class="btn btn-warning floating-minus prodFocus-minus glyphicon glyphicon-remove col-xs-1" style="margin-top:5px;"></button>'+
                    '</div>';
        $('.prodFocusList').append(templete);
    },
    isEmail: function() {
    var email = $('#tecEmail').val();
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    var result = pattern.test(email);
    if (! result) {
      return 0;
    } else {
      return 1;
    }
  },
    check: function(e) {
        e.preventDefault();
        if($('#tecName').val().trim() === '') {
            navigator.notification.alert(
                            'name can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#tecNumber').val().trim() === '') {
            navigator.notification.alert(
                            'phone number can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#tecAddress').val().trim() === '') {
            navigator.notification.alert(
                            'address can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if($('#tecWorkingArea').val().trim() === '') {
            navigator.notification.alert(
                            'working area can not be empty.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var dstStatus = 0;
        $.each($('.distData'), function() {
            if(($(this).find('#distNameSearch').val().trim() ==='')){
                $(this).find('#distNameSearch').attr('distId','');
            } else {
                if($(this).find('#distNameSearch').attr('distId').trim() ==='') {
                    dstStatus = 1;
                }
            }
        });
        if(dstStatus === 1) {
                    navigator.notification.alert(
                            'Distributor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        var retStatus = 0;
        $.each($('.retData'), function() {
            if(($(this).find('#retNameSearch').val().trim() ==='')) {
                $(this).find('#retNameSearch').attr('retId','');
            } else {
                if($(this).find('#retNameSearch').attr('retId').trim() === '') {
                    retStatus = 1;
                }
            }
        });
        if(retStatus === 1) {
            navigator.notification.alert(
                            'retailor information is not properly set.',
                            null,
                            'warning',
                            'OK'
                        );
                        return;
        }
        if(!($('#tecEmail').val().trim() === '')) {
            var checkEmail = masterTechnicanList.isEmail();
            if(checkEmail === 0) {
                navigator.notification.alert(
                                'please enter valid email.',
                                null,
                                'warning',
                                'OK'
                            );
                return;
            }
        }
        masterTechnicanList.addLoading();
        $('.loadingText').text('Sending......');
        masterTechnicanList.sendRemote();
    },
    sendRemote: function() {
        var tecInfo = [];
        tecDetail = {
            name: $('#tecName').val(),
            number: $('#tecNumber').val(),
            lat: $('#latOfLoc').attr('lat'),
            lng: $('#lngOfLoc').attr('lng'),
            postalCode: $('#postalCode').val(),
            address: $('#tecAddress').val(),
            workingArea: $('#tecWorkingArea').val(),
            email: $('#tecEmail').val(),
            viber: $('#tecViber').val(),
            facebook: $('#tecFacebook').val()
        }
        tecInfo.push(tecDetail);
        var distList = [];
        $.each($('.distData'), function() {
            if(!($(this).find('#distNameSearch').attr('distId').trim() ==='')){
                var data = {
                dist_id: $(this).find('#distNameSearch').attr('distId')
                }
                distList.push(data);
            }
        });
        tecInfo.push(distList);
        var retList = [];
        $.each($('.retData'), function() {
            if(!($(this).find('#retNameSearch').attr('retId').trim() ==='')) {
                var data = {
                ret_id: $(this).find('#retNameSearch').attr('retId')
                }
                retList.push(data);
            }
        });
        tecInfo.push(retList);
        var prodFocus = [];
        $.each($('.prodFocusData'), function() {
            if(!($(this).find('#prodFocus').val().trim() === '')) {
                var data = {
                prod: $(this).find('#prodFocus').val()
                }
                prodFocus.push(data);
            }
        });
        tecInfo.push(prodFocus);
        console.log(tecInfo);
        // sending data to server using ajax
        var tokenInformations = masterTechnicanList.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            /* First uploading image to server using
            file transfer protocal*/
            var imageData          = $('#photo').attr('src');
            var options            = new FileUploadOptions();
            options.fileKey        = "file";
            options.fileName       = imageData.substr(imageData.lastIndexOf('/') + 1);
            options.mimeType       = "image/jpeg";
            console.log(options.fileName);
            // Creating new object to stor field data
            var params             = new Object();
            params.accessToken     = accessToken;
            params.tecInfo         = tecInfo;
            // stroing params object to options
            options.params         = params;
            options.chunkedMode    = false;

            var ft = new FileTransfer();
            ft.upload(imageData, masterTechnicanList.ip + '?r=master-technican-list/handler', function(result){
                console.log('data is: ',result);
                masterTechnicanList.removeLoading();
                var response = $.parseJSON(result.response);
                if(response.status === 1) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                        window.location.href = './masterTechnicanList.html';
                }
                if(response.status === 0) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                }
                if(response.status === 2) {
                    navigator.notification.alert(
                                response.msgBody,
                                null,
                                response.msgTitle,
                                'OK'
                            );
                        window.location.href = "../index.html";
                }
            }, function(error){
                console.error(error);
                masterTechnicanList.removeLoading();
                navigator.notification.alert(
                    'Faild to send data. There may be proble in internet. Check your internet connection and try again.',
                    null,
                    'ERROR',
                    'OK'
                    );
             }, options);
        }).fail(function(error) {
            console.error(error);
            masterTechnicanList.removeLoading();
            navigator.notification.alert(
                                    'Error occured',
                                    null,
                                    'ERROR',
                                    'OK'
                                );
        });
    }
}

masterTechnicanList.initialize();