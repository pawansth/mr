var majorRetailerPlanning = {
    SQLLObj : null,
    ip      : null,
    ctr     : null,
    prodData: null,

    initialize: function() {
        screen.orientation.unlock();
            // Lock orientation to portrait mode
            screen.orientation.lock('portrait');
        majorRetailerPlanning.ip      = new ip();
        majorRetailerPlanning.ip      = majorRetailerPlanning.ip.remoteIpAddress();
        majorRetailerPlanning.SQLLObj = new SQLiteLogin();
        majorRetailerPlanning.ctr     = new controller();

        majorRetailerPlanning.addLoading();
        majorRetailerPlanning.yearInit();
        majorRetailerPlanning.monthInit();
        majorRetailerPlanning.loadDist();
        majorRetailerPlanning.bindEvents();
        // $(document).on('click', '.test1', function() {
        //     $('#unitList option').remove();
        // });
    },
    yearInit: function() {
        var date = new Date();
        var year = parseInt(date.getFullYear());
        for(var i = 1; i<=16; i++) {
            $('#planYear').append('<option value="'+year+'">'+year+'</option>');
            year = year + 1;
        }
    },
    monthInit: function() {
        var month = ['January','February','March','April','May','June','July','August','September','October','November','December'];
        var j = 0;
        for(var i = 1; i <= 12; i++) {
            $('#planMonth').append('<option value="'+i+'">'+month[j]+'</option>');
            j = j + 1;
        }
    },
    bindEvents: function() {
        $(document).on('click', '.addProd', majorRetailerPlanning.addProdTemplete);
        $(document).on('click', '.minusProd', majorRetailerPlanning.removeProdTemplete);
        $(document).on('click', '.btn-major-dist-to-retailer-plan', majorRetailerPlanning.chick);
        $(document).on('focusout', '#distNameSearch', function() {
            if($('#distNameSearch').val().trim() === '') {
                $('#distNameSearch').attr('distId', '');
            }
        });
        $(document).on('focusout', '#retNameSearch', function() {
            if($('#retNameSearch').val().trim() === '') {
                $('#retNameSearch').attr('retId', '');
            }
        });
        $(document).on('focusout', '#productName', function() {
            if($('#productName').val().trim() === '') {
                $('#productName').attr('prodId', '');
                $('#productName').attr('baseUnitId', '');
                $('#productName').attr('price', '');
            }
            majorRetailerPlanning.calculateAmount($(this).parent().parent());
        });
        $(document).on('focusout', '#productUnit', function() {
            if($('#productUnit').val().trim() === '') {
                $('#productUnit').attr('prodUnitId', '');
            }
            majorRetailerPlanning.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('focusout', '#productQty', function() {
            majorRetailerPlanning.calculateAmount($(this).parent().parent().parent());
        });
        $(document).on('input', '#distNameSearch', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('distId',$(this).attr('dist_id'));
             }
           });
        });
        /* Tracking the selected option from datalist for retailer */
        $("#retNameSearch").on('input', function () {
          var value = $('#retNameSearch').val();

          $('#retList option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $('#retNameSearch').attr('retId',$(this).attr('ret_id'));
             }
           });
        });
        $(document).on('input', '#productName', function () {
          var value = $(this).val();
          var ob    = $(this);
          var parent = $(this).parent().parent();
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodId',$(this).attr('prod_id'));
               $(ob).attr('baseUnitId',$(this).attr('base_unit_id'));
               $(ob).attr('price',$(this).attr('base_price'));
               majorRetailerPlanning.addLoading();
               $('.loadingText').text('Loading units...');
               majorRetailerPlanning.searchCrosProdUnit(parent);
             }
           });
        });
        $(document).on('input', '#productUnit', function () {
          var value = $(this).val();
          var ob    = $(this);
          var optionSet = '#'+$(this).attr('list');

          $(optionSet+' option').each(function() {
              if($(this).val() === value){
               // Your code here with the selected value
               $(ob).attr('prodUnitId',$(this).attr('unit_id'));
               return;
             }
           });
        });
    },
    addLoading: function() {
        var templete = '<div class="loading">'+
                            '<img class="loadImage" src="../img/loading.gif">'+
                            '<h5 class="loadingText">Loading.....</h5>'+
                        '</div>';
        $(templete).insertAfter('.slideBar');
    },
    removeLoading: function() {
        $('.loading').remove();
    },
    loadDist: function() {
      var tokenInformations = majorRetailerPlanning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: majorRetailerPlanning.ip + '?r=order-handler/dest-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option dist_id="'+this.id+'" value="'+this.dist_name+'">';
                  });
                  // templete += '</ul>';
                  $('#distList').append(templete);
                  majorRetailerPlanning.loadRet();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  majorRetailerPlanning.loadDist,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    // method that load all retailer data related to mr from server
    loadRet: function() {
      var tokenInformations = majorRetailerPlanning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: majorRetailerPlanning.ip + '?r=order-handler/ret-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  var templete = '';
                  // templete += '<ul>';

                  // display response  data in list
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option ret_id="'+this.id+'" value="'+this.retail_name+'">';
                  });
                  // templete += '</ul>';
                  $('#retList').append(templete);
                  majorRetailerPlanning.loadProd();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  'Network error.',
                  majorRetailerPlanning.loadRet,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          navigator.notification.alert(
                  ' There is error in network',
                  majorRetailerPlanning.loadRet,
                  'Error',
                  'Try Again'
              );
          console.error(error);
      });
    },
    loadProd: function() {
      var tokenInformations = majorRetailerPlanning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken
          };

          $.ajax({
              url: majorRetailerPlanning.ip + '?r=order-handler/prod-list',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  // console.log(data);
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }
                  majorRetailerPlanning.prodData = data;
                  var templete = '';
                  
                  $.each(data, function() {
                    // templete += '<li dist_id="'+this.id+'">'+this.dist_name+'</li>';
                      templete += '<option prod_id="'+this.id+'" base_unit_id="'+this.bas_unit+'" base_price="'+this.bas_price+'" value="'+this.product_name+'">';
                  });
                  // templete += '</ul>';
                  $('#prodList').append(templete);
                  majorRetailerPlanning.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  majorRetailerPlanning.loadProd,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    /* searching corresponding product unit */
    searchCrosProdUnit: function(currentObj) {
      crossProductId = currentObj.find('#productName').attr('prodId');
    //   alert(crossProductId);
      //  Pulling access token form sqlite database
      var tokenInformations = majorRetailerPlanning.SQLLObj.pullToken();
      tokenInformations.done(function(informations) {
          var accessToken = '';
          for (var idx in informations) {
              var information = informations[idx];
              //alert(information.name);
              accessToken = information.token;
          }
          if (accessToken.trim() === '') {
              console.log('there is no accessToken');
              window.location.href = "../index.html";
              return;
          }

          // console.log(accessToken);

          var values = {
              accessToken: accessToken,
              crossProductId: crossProductId
          };

          $.ajax({
              url: majorRetailerPlanning.ip + '?r=order-handler/search-cross-unit',
              method: "POST",
              dataType: 'json',
              data: values,

              error: function(jqXHR, textStatus, errorThrown) {
                  console.error(jqXHR.status + '\n' + jqXHR.responseText);
              },

              success: function(data) {
                  console.log(data);
                  
                  if (data === 2) {
                      window.location.href = "../index.html";
                  }

                  var templete = '';
                  
                  $.each(data, function() {
                      templete += '<option unit_id="'+this.id+'" value="'+this.name+'">';
                  });
                  console.log(currentObj.find('#unitList'));
                  currentObj.find('#unitList').append(templete);
                  majorRetailerPlanning.removeLoading();
              }
          }).fail(function(error) {
              console.error(error);
              navigator.notification.alert(
                  ' There is error in network',
                  majorRetailerPlanning.searchCrosProdUnit,
                  'Error',
                  'Try Again'
              );
          });

      }).fail(function(error) {
          console.error(error);
      });
    },
    addProdTemplete: function() {
        $(this).parent().parent().css('margin-bottom','5px');
        var templete = '';
        templete += '<div class="planProduct">'+
                        '<div class="">'+
                            '<input type="text" class="form-control" id="productName" prodId="" baseUnitId="" price="" list="prodList" placeholder="product">'+
                            '<datalist id="prodList"></datalist>'+
                        '</div>'+
                        '<div class="row add-top-margin">'+
                            '<div class="col-xs-4 nopadding-right">'+
                                '<input type="text" class="form-control" id="productUnit" prodUnitId="" list="unitList" placeholder="unit">'+
                                '<datalist id="unitList"></datalist>'+
                            '</div>'+
                            '<div class="col-xs-2 nopadding-left nopadding-right">'+
                                '<input type="number" class="form-control" id="productQty" placeholder="qty">'+
                            '</div>'+
                            '<div class="col-xs-3 nopadding-left">'+
                                '<input type="text" class="form-control" id="productAmount" disabled placeholder="NPR">'+
                            '</div>'+
                            '<button type="button" class="btn btn-warning floating-add glyphicon glyphicon-plus addProd col-xs-2"></button>'+
                            '<button type="button" class="btn btn-warning floating-minus minusProd glyphicon glyphicon-remove col-xs-2"></button>'+
                        '</div>'+
                    '</div>';
        $('.planProductList').append(templete);
    },
    removeProdTemplete: function() {
        $(this).parent().parent().remove();
    },
    calculateAmount: function(parentObj) {
        if(!(parentObj.find('#productName').val().trim() === '' || parentObj.find('#productName').attr('prodId').trim() === '')) {
            if(!(parentObj.find('#productUnit').val().trim() === '' || parentObj.find('#productUnit').attr('prodUnitId').trim() === '')) {
                if(!(parentObj.find('#productQty').val().trim() === '')) {
                    var tokenInformations = majorRetailerPlanning.SQLLObj.pullToken();
                    tokenInformations.done(function(informations) {
                        var accessToken = '';
                        for (var idx in informations) {
                            var information = informations[idx];
                            //alert(information.name);
                            accessToken = information.token;
                        }
                        if (accessToken.trim() === '') {
                            console.log('there is no accessToken');
                            window.location.href = "../index.html";
                            return;
                        }

                        // console.log(accessToken);

                        var values = {
                            accessToken: accessToken,
                            productId  : parentObj.find('#productName').attr('prodId'),
                            uid        : parentObj.find('#productUnit').attr('prodUnitId'),
                            base_uid   : parentObj.find('#productName').attr('baseUnitId'),
                            qtyToChange: parentObj.find('#productQty').val()
                        };
                        majorRetailerPlanning.addLoading();
                        $('.loadingText').text('calcuation amount');
                    $.ajax({
                        url: majorRetailerPlanning.ip + '?r=major-dist-to-retailer-plan/unit-to-price',
                        method: "POST",
                        dataType: 'json',
                        data: values,

                        error: function(jqXHR, textStatus, errorThrown) {
                            console.error(jqXHR.status + '\n' + jqXHR.responseText);
                            navigator.notification.alert(
                            'There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                        },

                        success: function(data) {
                            majorRetailerPlanning.removeLoading();
                            if (data === 2) {
                                window.location.href = "../index.html";
                            }
                            parentObj.find('#productAmount').val(data);
                        }
                    }).fail(function(error) {
                        console.error(error);
                        majorRetailerPlanning.removeLoading();
                        navigator.notification.alert(
                            ' There is error in network',
                            null,
                            'Error',
                            'OK'
                        );
                    });
                    }).fail(function(error) {
                        console.error(error);
                    });
                }
            }
        }
        return;
    },
    chick: function() {
        if($('#distNameSearch').val().trim() === '') {
            navigator.notification.alert(
                'Distributor name can not be empty',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        if($('#distNameSearch').attr('distId').trim() === '') {
            navigator.notification.alert(
                'Distributor name is not properly slected form list.'+
                ' Please select distributor name form list.',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        if($('#retNameSearch').val().trim() === '') {
            navigator.notification.alert(
                'Retailer name is empty',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        if($('#retNameSearch').attr('retId').trim() === '') {
            navigator.notification.alert(
                'Retailer name is not properly slected form list.'+
                ' Please select retailer name form list.',
                null,
                'Warrning',
                'OK'
            );
            return;
        }
        var count = 1;
        var eachStatus = false;
        $.each($('.planProduct'), function() {
            if($(this).find('#productName').val().trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productName').attr('prodId').trim() === '') {
                navigator.notification.alert(
                    'Product name in row '+count+
                    ' should be select form list of give product.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').val().trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productUnit').attr('prodUnitId').trim() === '') {
                navigator.notification.alert(
                    'Product unit in row '+count+
                    ' should be select form list of given unit list.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            if($(this).find('#productQty').val().trim() === '') {
                navigator.notification.alert(
                    'Product quantity in row '+count+
                    ' is empty.',
                    null,
                    'Warrning',
                    'OK'
                );
                eachStatus = true;
                return false;
            }
            count = count + 1;
        });
        if(eachStatus === true) {
            eachStatus = false;
            return;
        }
        majorRetailerPlanning.addLoading();
        $('.loadingText').text('Submiting data')
        majorRetailerPlanning.submitPlan();
    },
    submitPlan: function() {
        var productValues = [];
        var distId        = $('#distNameSearch').attr('distId');
        var retId         = $('#retNameSearch').attr('retId');
        var year          = $('#planYear').val();
        var month         = $('#planMonth').val();
        var description   = $('#plan_desc').val();

        var distributorInfo = {
            distId     : distId,
            retId      : retId,
            year       : year,
            month      : month,
            description: description
        };
        var productId    = '';
        var productQty   = '';
        var productUnit  = '';
        var productAmount = '';

        $.each($('.planProduct'), function() {
            productId     = $(this).find('#productName').attr('prodId');
            productQty    = $(this).find('#productQty').val();
            productUnit   = $(this).find('#productUnit').attr('prodUnitId');
            productAmount = $(this).find('#productAmount').val();
            var data = {
                productId    : productId,
                productQty   : productQty,
                productUnit  : productUnit,
                productAmount: productAmount
            };
            // pushing json formate data to array
            productValues.push(data);
        });
        console.log(productValues);
        // sending data to server using ajax
        var tokenInformations = majorRetailerPlanning.SQLLObj.pullToken();
        tokenInformations.done(function(informations) {
            var accessToken = '';
            for (var idx in informations) {
                var information = informations[idx];
                //alert(information.name);
                accessToken = information.token;
            }
            if (accessToken.trim() === '') {
                console.log('there is no accessToken');
                window.location.href = "../index.html";
                return;
            }

            // console.log(accessToken);

            var values = {
                accessToken    : accessToken,
                distributorInfo: distributorInfo,
                productValues  : productValues
            };

            $.ajax({
                url: majorRetailerPlanning.ip + '?r=major-dist-to-retailer-plan/handler',
                method: "POST",
                dataType: 'json',
                data: values,

                error: function(jqXHR, textStatus, errorThrown) {
                    majorRetailerPlanning.removeLoading();
                    // navigator.notification.alert(
                    //     'Network eror',
                    //     null,
                    //     'ERROR',
                    //     'OK'
                    // );
                    console.error(jqXHR.status + '\n' + jqXHR.responseText);
                },

                success: function(data) {
                    // console.log(data);
                    if (data === 1) {
                        majorRetailerPlanning.removeLoading();
                        navigator.notification.alert(
                            'Successfull',
                            null,
                            'Success',
                            'OK'
                        );
                        window.location.href = "./majorRetailerPlanning.html";
                    } else if (data === 0) {
                        majorRetailerPlanning.removeLoading();
                        navigator.notification.alert(
                            'Operation faild',
                            null,
                            'Error',
                            'OK'
                        );
                    } else if (data === 2) {
                        window.location.href = "../index.html";
                    }

                }
            }).fail(function(error) {
                console.error(error);
                majorRetailerPlanning.removeLoading();
                navigator.notification.alert(
                    ' There is error in network',
                    null,
                    'Error',
                    'OK'
                );
            });

        }).fail(function(error) {
            majorRetailerPlanning.removeLoading();
            navigator.notification.alert(
                'Error occured.',
                null,
                'ERROR',
                'OK'
            );
            console.error(error);
        });
    }
}

majorRetailerPlanning.initialize();