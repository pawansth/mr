var touchHandler = function() {
  var touch = {
    xDown: null,
    yDown: null,
    initialize: function() {
      $('#sidebar-toggle').change(function() {
        if(! $('.toggle').hasClass('active')) {
          $('.toggle').addClass('active');
        }  else {
          $('.toggle').removeClass('active');
        }
       });
      document.addEventListener('touchstart', this.handleTouchStart, false);
      document.addEventListener('touchmove', this.handleTouchMove, false);
    },
    handleTouchStart: function(evt) {
      this.xDown = evt.touches[0].clientX;
      this.yDown = evt.touches[0].clientY;
    },
    handleTouchMove: function(evt) {
        if ( ! this.xDown || ! this.yDown ) {
            return;
        }
        var xUp = evt.touches[0].clientX;
        var yUp = evt.touches[0].clientY;

        var xDiff = this.xDown - xUp;
        var yDiff = this.yDown - yUp;

        if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
            if ( xDiff > 0 ) {
              if($('.toggle').hasClass('active')) { /* left swipe */
                  $('.toggle').click();
                  $('.toggle').removeClass('active');
                  this.xDown = null;
                  this.yDown = null;
                }
                return;
            } else {
                /* right swipe */
                if(this.xDown > 12) {
                return;
              } else if(! $('.toggle').hasClass('active')) {
                  $('.toggle').click();
                  $('.toggle').addClass('active');
                  this.xDown = null;
                  this.yDown = null;
                }
                return;
            }
        } else {
            if ( yDiff > 0 ) {
                /* up swipe */
                return 2;
            } else {
                /* down swipe */
                return 3;
            }
        }
        /* reset values */
    }
  }

  touch.initialize();
  return touch;
}
