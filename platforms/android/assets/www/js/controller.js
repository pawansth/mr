var controller = function () {
    var control = {
        self: null,
        geoPos: null,
        setFlag: null,
        touchHandler: null,
        // loginHandlerObj: null,

        initialize: function () {
            self = this;
            this.touchHandler = new touchHandler();
            this.setFlag = new forgroundEventHandler();
            this.setFlag.appStartFlagCheck();

            this.geoPos = new geolocationPosition();  //creating object of geolocationPosition class
            // this.loginHandlerObj = new Login();         //  creating object for login class

            this.bindEvents();
        },

        bindEvents: function () {
            // $(document).on("click", "#btnLogin", this.checkLogin);
            $(document).on("click", ".dashbord-btn", this.onBtnClick);
            // $('#btnLogin').click(this.checkLogin());
        },

        onBtnClick: function (e) {
            e.preventDefault();
            if ($(this).hasClass('active')) {
                return;
            }
            // unlock orentation
            if($('.toggle').hasClass('active')) {
              $('.toggle').click();
              $('.toggle').removeClass('active');
            }

            var tab = $(this).data('tab');
            if (tab === '#btn-start') {
                control.workingTimeStart();
            } else if(tab === '#btn-stop') {
                control.workingTimeStop();
            }
            else if (tab === '#order-form') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./orderForm.html";
                }, 250);
            } else if(tab === '#company-order-form') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./companyOrder.html";
                }, 250);
            } else if (tab === '#pushDistInfo') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./distributorInsertForm.html";
                }, 250);
            } else if (tab === '#pushRetInfo') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./retailerInsertForm.html";
                }, 250);
            } else if (tab === '#pushPlanning') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./planning.html";
                }, 250);
            } else if (tab === '#pushDistributorPlanning') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./majorDistributorPlanning.html";
                }, 250);
            } else if(tab === '#pushRetailerPlanning') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./majorRetailerPlanning.html";
                }, 250);
            } else if(tab === '#pushDistributroStock') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./distributorStock.html";
                }, 250);
            } else if(tab === '#pushMasterFarmer') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./masterFarmerList.html";
                }, 250);
            } else if(tab === '#pushMasterTechnican') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./masterTechnicanList.html";
                }, 250);
            } else if(tab === '#pushMasterCattleFarmer') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./masterCattleFarmerList.html";
                }, 250);
            } else if(tab === '#pushMasterPoultryFarmer') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./masterPoultryFarmerList.html";
                }, 250);
            } else if(tab === '#pushMasterDoctor') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./masterDoctorList.html";
                }, 250);
            } else if(tab === '#pushReach') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./reach.html";
                }, 250);
            } else if(tab === '#sampleForm'){
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./sample.html";
                }, 250);
            } else if(tab === '#pushCompanyFeed') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./companyFeed.html";
                }, 250);
            } else if(tab === '#view-list') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./viewList.html";
                }, 250);
            }  else if(tab === '#tada') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./tada.html";
                }, 250);
            }  else if(tab === '#tadaView') {
                $('.slideBar').css({'left':'-100%'});
                setTimeout(function() {
                    window.location.href = "./tadaView.html";
                }, 250);
            }
        },

        workingTimeStart: function () {
            control.geoPos.getStartPosition()
        },

        workingTimeStop: function () {
          control.geoPos.getStopPosition();
        }

    }

    control.initialize();
    return control;
}
