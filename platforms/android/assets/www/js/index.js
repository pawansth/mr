// var networkConnection;
// var controller;
// var storageService;
// var testof;
var app = {
    progressIndi: null,
    // Application Constructor
    initialize: function() {
        // var tableGen = new SQLiteTableGenerator();
        // tableGen.initialize();
        // setTimeout(function() {
            new splashscreen();
        // }, 250);
        // document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },



    onDeviceReady: function() {
        var tableGen = new SQLiteTableGenerator();
        tableGen.initialize();
        // new splashscreen();
        // // window.location.href = './views/reach.html';
        try {
            window.codePush.sync(app.syncStatus, {installMode: InstallMode.IMMEDIATE}, app.downloadProgress);
        } catch(error) {
            console.error('[code] update error', error);
            navigator.notification.alert(
                'There is network error. Please check your network connection and try again',
                null,
                'ERROR',
                'Try Again'
            );
        }
    },
    syncStatus: function(status) {
            switch (status) {
                case SyncStatus.UP_TO_DATE:
                    app.afterUpdate();
                break;
                case SyncStatus.INSTALLING_UPDATE:
                    $('.progress').text('Installing....');
                    break;
                case SyncStatus.UPDATE_INSTALLED:
                    console.log('update installed');
                break;
                case SyncStatus.UPDATE_IGNORED:
                    app.afterUpdate();
                break;
                case SyncStatus.DOWNLOADING_PACKAGE:
                    console.log("Downloading package.");
                    break;
            }
        },
        downloadProgress: function(downloadProgress) {
            if (downloadProgress) {
                app.progressIndi = ((downloadProgress.receivedBytes/downloadProgress.totalBytes) * 100);
                $('.progress').text('Downloading content');
                $("progress").attr("value", app.progressIndi);
                console.log(app.progressIndi);
    	        // Update "downloading" modal with current download %
                // console.log("Downloading " + downloadProgress.receivedBytes + " of " + downloadProgress.totalBytes);
            }
        },
        afterUpdate: function() {
            // At the starting of apps first create table
            var STG = new SQLiteTableGenerator();
            // console.log('after obj create');
            var result = STG.initialize();
            result.done(function(resultReturn){
              console.log('table is successfully created');
            }
            ).fail(function(error){
              console.error(error);
            });
        
            var networkConnection = new networkInformation();
            document.addEventListener("offline", networkConnection.checkConnection, false);
        
            // $('.container').load('./views/splashscreen.html');
            new splashscreen();
            $(document).on("click", "#btnLogin", this.checkLogin);
        },

    //  function that call loginProcessHandler function inside Login class
    checkLogin: function(e) {
      // console.log('fine');
      e.preventDefault();
      var loginHandlerObj = new Login();         //  creating object for login class
      loginHandlerObj.loginProcessHandler();
    },
};

app.initialize();



/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
/*var app = {
    // Application Constructor
    initialize: function () {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function () {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function () {
        // Migrate data from older versions
        window.codePush.getCurrentPackage(function (currentPackage) {
            // getCurrentPackage returns null if no update was installed (app store version)
            if (currentPackage && currentPackage.isFirstRun) {
                // First run after an update, migrate data
                if (currentPackage.appVersion === "1.0.3") {
                    // migrate data from store version to version 1.0.0
                } else if (currentPackage.appVersion === "2.0.0") {
                    // migrate data to version 2.0.0
                }
            }
            
            // continue application initialization
            // app.receivedEvent('deviceready');
            
            // Wait for 5s after the application started and check for updates.
            setTimeout(app.checkAndInstallUpdates, 5000);
            
            // Notify the plugin that update succeeded.
            window.codePush.notifyApplicationReady();

        }, app.getErrorHandler("Error while retrieving the current package."));
    },
    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
    // Uses the CodePush service configured in config.xml to check for updates, prompt the user and install them.
    checkAndInstallUpdates: function () {
        
        // Check the CodePush server for updates.
        console.log("Checking for updates...");
        window.codePush.checkForUpdate(app.checkSuccess, app.getErrorHandler("Checking for update failed."));
    },
    // Called after the CodePush server responded the checkForUpdate call
    checkSuccess: function (remotePackage) {
        if (!remotePackage) {
            // A null remotePackage means that the server successfully responded, but there is no update available.
            console.log("The application is up to date.");
            new splashscreen();
        }
        else {
            console.log("There is an update available. Remote package:" + JSON.stringify(remotePackage));
                
            // Called after the user confirmed or canceled the update 
            function onConfirm(buttonIndex) {
                switch (buttonIndex) {
                    case 1:
                        /* Install */
/*                        console.log("Downloading package...");
                        remotePackage.download(app.onDownloadSuccess, app.getErrorHandler("Downloading the update package failed."));
                        break;
                    case 2:
                        /* Cancel */
                        /* nothing to do */
/*                        break;
                }
            }

            // Ask the user if they want to download and install the update
            navigator.notification.confirm(
                'An update is available. Would you like to download and install it?',
                onConfirm,
                'Update'
                ['Install', 'Cancel']);
        }
    },
    // Called after an update package was downloaded sucessfully.
    onDownloadSuccess: function (localPackage) {
        console.log("Local package downloaded. Local package: " + localPackage.localPath);

        var installCallback = function () {
            console.log("Install succeeded");
        };

        console.log("Installing package...");
        localPackage.install(installCallback, app.getErrorHandler("Installation failed."), { installMode: InstallMode.IMMEDIATE });
    },
    // Returns an error handler that logs the error to the console and displays a notification containing the error message.
    getErrorHandler: function (message) {
        // Displays a dialog containing a message.
        var displayErrorMessage = function (message) {
            navigator.notification.alert(
                message,
                null,
                'CodePush',
                'OK');
        };

        return function (error) {
            console.log(message + ":" + error.message);
            displayErrorMessage(message + ":" + error.message);
        }
    }
};*/

// app.initialize();